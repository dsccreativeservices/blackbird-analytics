<?php

return [

    //set which connection is default
   'default' => 'meta',

   'connections' => [

        //events database
        'events' => [
            'driver'    => env('DB_CONNECTION'),
            'host'      => env('EVENTS_HOST'),
            'database'  => env('EVENTS_DATABASE'),
            'username'  => env('EVENTS_USERNAME'),
            'password'  => env('EVENTS_PASSWORD'),
            'port'      => env('EVENTS_PORT'),
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
            'strict'    => false,
         ],

         //meta-data database
        'meta' => [
            'driver'    => 'mysql',
            'host'      => env('META_HOST'),
            'database'  => env('META_DATABASE'),
            'username'  => env('META_USERNAME'),
            'password'  => env('META_PASSWORD'),
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
            'strict'    => false,
        ],
    ],
];

?>
