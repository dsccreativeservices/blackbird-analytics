<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

//check if user is authenticated
$app->post('/login', 'AuthController@login');

/* all API calls should be within the middleware below */
$app->group(['middleware' => 'auth'], function() use ( $app ){

    //advertisers
    $app->get('/advertisers', 'AdvertiserController@getAll');
    $app->post('/advertisers/create', 'AdvertiserController@create');
    $app->post('/advertisers/search', 'AdvertiserController@search');
    $app->get('/advertisers/{id}', 'AdvertiserController@getDetails');
    $app->post('/advertisers/{id}/update', 'AdvertiserController@update');
    $app->get('/advertisers/{id}/campaigns', 'AdvertiserController@getCampaigns');

    //campaigns
    $app->post('/campaign/create', 'CampaignController@create');
    $app->post('/campaign/search', 'CampaignController@search');
    $app->get('/campaign/{id}', 'CampaignController@getDetails');
    $app->post('/campaign/{id}/update', 'CampaignController@update');
    $app->get('/campaign/{id}/{startDate}/{endDate}', 'CampaignController@getDetails');
    $app->get('/campaign/findByIO/{id}', 'CampaignController@findByIO');
    $app->get('/campaign/{id}/parents', 'CampaignController@getParents');
    $app->get('/campaign/{id}/siblings', 'CampaignController@getSiblings');


    //projects
    $app->post('/project/create', 'ProjectController@create');
    $app->post('/project/search', 'ProjectController@find');
    $app->get('/project/search/{ooid}', 'ProjectController@getIdByOOID');
    $app->get('/project/{id}/exists', 'ProjectController@checkIfExists');
    $app->get('/project/{id}', 'ProjectController@getDetails');
    $app->post('/project/{id}/update', 'ProjectController@update');
    $app->get('/project/{id}/{startDate}/{endDate}', 'ProjectController@getDetails');
    $app->get('/project/{id}/parents', 'ProjectController@getParents');
    $app->get('/project/{id}/siblings', 'ProjectController@getSiblings');
    $app->get('/project/{id}/tags', 'TagController@getByProject');
    $app->post('/project/{id}/tag/create', 'TagController@create');
    $app->post('/project/{id}/tag/delete', 'TagController@delete');

    //pin/unpin
    $app->post('/pin/{projectId}/{userId}', 'PinController@toggleStatus');

    //search
    $app->post('/search', 'SearchController@search');

    //benchmarks
    $app->get('/benchmarks/{adType}', 'BenchmarksController@getProjects');
    $app->get('/benchmarks/{adType}/{startDate}/{endDate}', 'BenchmarksController@getProjects');

    $app->get('/users', 'AuthController@getAllUsers');
    $app->post('/user/new', 'AuthController@createUser');
    $app->get('/user/{id}', 'AuthController@getUser');
    $app->post('/user/{id}/passwordUpdate', 'AuthController@updatePassword');
    $app->post('/user/{id}/update', 'AuthController@update');
    $app->get('/user/{id}/delete', 'AuthController@delete');
    $app->get('/user/{id}/pins', 'ProjectController@getPins');

});
