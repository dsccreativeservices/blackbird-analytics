<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Campaign extends Model{

    public function advertiser(){
        return $this->belongsTo('App\Models\Advertiser', 'advertiserId');
    }

    public function projects(){
        return $this->hasMany('App\Models\Project', 'campaignId');
    }
}
