<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Data extends Model{

    protected $table = 'aggregate_hour';

    public function tag(){
        return $this->belongsTo('App\Models\Tags', 'id');
    }

}

?>
