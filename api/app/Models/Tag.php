<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Models\Data;

class Tag extends Model{

    //use the events database instead of default
    //protected $connection = 'events';

    //Old DB
//    table 'dat_creative';

    //New Db
//    table 'dat_creative';



    protected $fillable = ['name','creativeId','type','redirect'];

    public function project(){
        return $this->belongsTo('App\Models\Project', 'creativeId');
    }


    /*
        GET ALL TAGS FOR PROJECT
        Return each tag with data for each day, not grouped
    */
    public static function getDataByDay( $creativeId, $start = null, $end = null ){

        //if using date range
        if( $start != null && $end != null ){
//            return DB::connection( 'events' )->table( 'final' )
//                    ->where('creative_id', '=', $creativeId)
//                    ->select('time_stamp', 'tag_name', 'redirect_url', DB::raw('sum(count) as count'))
//                    ->whereRaw('time_stamp >= ? AND time_stamp <= ?', [$start,$end])
//                    ->groupBy('tag_name', 'time_stamp')
//                    ->orderBy('tag_name', 'asc')
//                    ->get();

            return DB::connection('events')
                ->select("SELECT time_stamp, tag_name, redirect_url, sum(count) AS count
FROM discoverydciadhoc.creative.final
WHERE creative_id = $creativeId AND time_stamp >= '$start' AND time_stamp <= '$end'
GROUP BY time_stamp, tag_name, redirect_url
ORDER BY tag_name ASC;");

        }

        //get all data
        else{
//            return DB::connection( 'events' )->table( 'final' )
//                    ->where('creative_id', '=', $creativeId)
//                    ->select('time_stamp', 'tag_name', 'redirect_url', DB::raw('sum(count) as count'))
//                    ->groupBy('tag_name', 'time_stamp')
//                    ->orderBy('tag_name', 'asc')
//                    ->get();

            return DB::connection('events')
                ->select("SELECT time_stamp, tag_name, redirect_url, sum(count) AS count
FROM discoverydciadhoc.creative.final
WHERE creative_id = $creativeId
GROUP BY time_stamp, tag_name, redirect_url
ORDER BY tag_name ASC");
        }
    }



    /*
        GET ALL TAGS FOR PROJECT
        Return totals for each tag, not data by day
        Mostly called from Campaign pages where data should be totaled
    */
    public static function getTotal( $creativeId, $start = null, $end = null ){

        if( $start != null && $end != null ){
//            return DB::connection( 'events' )->table( 'final' )
//                    ->select('time_stamp', 'tag_name', 'redirect_url', DB::raw('count'))
//                    ->where('creative_id', '=', $creativeId)
//                    ->whereRaw('time_stamp >= ? AND time_stamp <= ?', [ $start, $end ] )
//                    ->groupBy('tag_name')
//                    ->orderBy('tag_name', 'asc')
//                    ->get();
            return DB::connection('events')
                ->select("SELECT time_stamp, tag_name, redirect_url,  \"count\"
FROM discoverydciadhoc.creative.final
WHERE creative_id = $creativeId AND time_stamp >= $start AND time_stamp <= $end
GROUP BY tag_name, time_stamp, redirect_url, count
ORDER BY tag_name ASC;");
        }

        //get all data
        else{
//            return DB::connection( 'events' )->table( 'final' )
//                    ->select('tag_name', 'redirect_url', DB::raw('count'))
//                    ->where('creative_id', '=', $creativeId)
//                    ->groupBy('tag_name')
//                    ->orderBy('tag_name', 'asc')
//                    ->get();

            return DB::connection('events')
                ->select("SELECT tag_name, redirect_url, \"count\"
FROM discoverydciadhoc.creative.final
WHERE creative_id = $creativeId
GROUP BY tag_name, redirect_url, count
ORDER BY tag_name ASC;
");
        }
    }



    /*
        GET OVERALL IMPRESSIONS FOR PROJECT
    */
    public static function getImpressions( $creativeId ){
//        return DB::connection( 'events' )->table( 'final' )
//                ->where( 'creative_id', '=', $creativeId )
//                ->whereIn('tag_name', [ "overall", "overall-impression" ] )
//                ->select( DB::raw('sum(count) as count') )
//                ->first();
        return DB::connection('events')
            ->select("SELECT sum(count) AS count
FROM discoverydciadhoc.creative.final
WHERE creative_id = $creativeId AND tag_name LIKE '%overall%';");
    }

    /*
        GET OVERALL IMPRESSIONS FOR PROJECT
    */
    public static function getClicksTotal( $creativeId ){
//        return DB::connection( 'events' )->table( 'final' )
//                ->where( 'creative_id', '=', $creativeId )
//                ->where( 'redirect_url', '<>', '' )
//                ->select( DB::raw('sum(count) as count') )
//                ->first();
        return DB::connection('events')
            ->select("SELECT sum(count) AS count
FROM discoverydciadhoc.creative.final
WHERE creative_id = $creativeId AND redirect_url <> '';");
    }


}

?>
