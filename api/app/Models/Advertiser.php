<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Advertiser extends Model
{

    public function campaigns()
    {
        return $this->hasMany('App\Models\Campaign', 'advertiserId');
    }

}
