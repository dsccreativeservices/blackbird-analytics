<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pin extends Model{

    public function user(){
        return $this->belongsTo('App\User', 'userId');
    }

    public function project(){
        return $this->hasOne('App\Models\Project', 'id', 'projectId');
    }
}

?>
