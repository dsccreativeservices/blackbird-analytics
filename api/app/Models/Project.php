<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Project extends Model{

    protected $table = 'creatives';

    protected $fillable = ['name','category','description','published_on', 'ooid'];

    public function campaign(){
        return $this->belongsTo('App\Models\Campaign', 'campaignId');
    }

    public function tags(){
        return $this->hasMany('App\Models\Tag', 'creativeId');
    }
}

?>
