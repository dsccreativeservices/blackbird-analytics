<?php

/*
    ARCHIVED VERSION
    New events database aggregates data by day so no need for us to do that here
*/

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Models\Data;

class Tag extends Model{

    //use the events database instead of default
    protected $connection = 'events';

    protected $fillable = ['name','creativeId','type','redirect'];

    public function project(){
        return $this->belongsTo('App\Models\Project', 'creativeId');
    }

    /*
        GET COUNT FOR INDIVIDUAL TAG
        Group by day
    */
    public static function dataByDay( $tagId, $start = null, $end = null ){

        //if using date range
        if( $start != null && $end != null ){

            return  DB::table('aggregate_hour as d')
                    ->where('tagId', '=', $tagId )
                    ->whereBetween('on_date', array( $start, $end ) )
                    ->select(
                        DB::raw( 'DATE( DATE_ADD(d.on_date, INTERVAL -4 HOUR) ) AS day' ), //timezone conversion so days stay as the database has them
                        //DB::raw( 'DATE(d.on_date ) AS day' ),
                        DB::raw('sum(d.count) AS total') //sum up the count
                    )
                    ->groupBy('day')
                    ->get();
        }

        //get all data
        else{
            return  DB::table('aggregate_hour as d')
                    ->where('tagId', '=', $tagId )
                    ->select(
                        DB::raw( 'DATE( DATE_ADD(d.on_date, INTERVAL -4 HOUR) ) AS day' ), //timezone conversion so days stay as the database has them
                        DB::raw('sum(d.count) AS total') //sum up the count
                    )
                    ->groupBy('day')
                    ->get();
        }
    }



    /*
        GET COUNT FOR INDIVIDUAL TAG
        Group by day
    */
    public static function getTotal( $tagId, $start = null, $end = null ){

        if( $start != null && $end != null ){

            return  DB::table('aggregate_hour as d')
                    ->where('tagId', '=', $tagId )
                    ->whereBetween('on_date', array( $start, $end ) )
                    ->select(
                        DB::raw( 'DATE( DATE_ADD(d.on_date, INTERVAL -4 HOUR) ) AS day' ), //timezone conversion so days stay as the database has them
                        //DB::raw( 'DATE(d.on_date ) AS day' ),
                        DB::raw('sum(d.count) AS total') //sum up the count
                    )
                    ->count();
        }

        else{
            return  DB::table('aggregate_hour as d')
                    ->where('tagId', '=', $tagId )
                    ->select(
                        DB::raw( 'DATE( DATE_ADD(d.on_date, INTERVAL -4 HOUR) ) AS day' ), //timezone conversion so days stay as the database has them
                        DB::raw('sum(d.count) AS total') //sum up the count
                    )
                    ->sum( 'd.count' );
        }
    }


    //public static function getTags( $projectId, $start, $end ){
    /*public function getCount(){
        return DB::table( 'aggregate_hour' )
                      ->join('tags', 'tags.id', '=', 'aggregate_hour.tagId')
                      ->select( 'aggregate_hour.tagId', 'tags.name', 'tags.type', 'tags.redirect', DB::raw('count(aggregate_hour.id) as count'))
                      ->where('tagId', '=', 10)
                      ->groupBy('tagId' )
                      ->get();
    }
    */
}

?>
