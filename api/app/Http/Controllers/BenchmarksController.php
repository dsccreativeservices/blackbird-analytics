<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Project;
use App\Models\Tag;
use App\Models\Campaign;
use App\Models\Pin;


class BenchmarksController extends Controller{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /*
        GET Projects
        Return the projects details
    */
    public function getProjects($adType, $startDate=null, $endDate=null){

        $projects = Project::where("adtype", "=", $adType)->get();
        $returnData = [];

        function findTag( $tag_name, $array ){
            foreach( $array as $k => $v ){
                if( $v['name'] === $tag_name ){
                    return $k;
                }
            }
            return null;
        }

        foreach( $projects as $p ){
            if( count( $p ) ){

                // +1 days to end date so that data includes end date
                if( $endDate != null ){
                    $date = date_create( $endDate );
                    date_add( $date, date_interval_create_from_date_string( "1 days" ) );
                    $endDate = date_format($date,"Y-m-d");
                }

                //get metadata for this project
                $ret['metaData'] = $p;

                //add operative id to the data
                $ret['metaData']['campaign'] = $p->campaign;

                $data['tags'] = [];

                $tags = Tag::getDataByDay( $p->id, $startDate, $endDate );

                foreach( $tags as $t ){

                    //if tag exists only add data for the day
                    $existingTag = findTag( $t->tag_name, $data['tags'] );
                    if( $existingTag !== null ){
                        array_push(
                            $data['tags'][ $existingTag ][ 'dataByDay' ],
                            [
                                'date' => isset( $t->time_stamp ) ? $t->time_stamp : null,
                                'total' => $t->count
                            ]
                        );
                        $data['tags'][ $existingTag ]['total'] = $data['tags'][ $existingTag ]['total'] + $t->count;
                    }

                    //if tag doesn't exist create a new one and push into the tags array
                    else{
                        $tagInfo = [];
                        $tagInfo['name'] = $t->tag_name;
                        //$tagInfo['type'] = $t->type;

                        $tagInfo['total'] = $t->count;

                        //data as array
                        $tagInfo['dataByDay'] = [];

                        //push this value into data array
                        array_push(
                            $tagInfo['dataByDay'],
                            [
                                'date' => isset( $t->time_stamp ) ? $t->time_stamp : null,
                                'total' => $t->count
                            ]
                        );

                        //redirect
                        if( $t->redirect_url !== "" ){
                            $tagInfo['redirect'] = $t->redirect_url;
                        }

                        //push into tags array
                        array_push( $data['tags'], $tagInfo );
                    }

                }

                $ret['data'] = $data;

                array_push($returnData, $ret);

//                return json_encode( $ret );
            }
            else{
                return 'false';
            }
        }

        return json_encode( $returnData);
//        return json_encode( $projects);
//        return json_encode($p);
    }

}
