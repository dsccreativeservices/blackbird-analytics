<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;


class AuthController extends Controller{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }


    /*
        CHECK AUTH STATUS
    */
    public function login(){

        //find user
        $user = User::where('email', '=', $_POST[ 'email' ] )->first();

        if( count( $user ) > 0 ){
            $ret['user'] = $user->id;

            //check password
            if( Hash::check( $_POST[ 'password' ], $user->password ) ){
                $ret['status'] = 'success';
                $ret['apiKey'] = $user->api_token;
                $ret['userGroup'] = $user->userGroup;
            }else{
                $ret['status'] = 'failed';
            }

        }else{
            $ret['status'] = 'failed';
            $ret['msg'] = 'no user matching email';
        }

        return json_encode( $ret );
   }


   /*
        Get User Data
   */
   public function getUser( $id ){

       $user = User::where('id', '=', $id )->first();

       if( count( $user ) > 0 ){
           $ret['status'] = 'success';
           $ret['user'] = [
               'id' => $user->id,
               'name' => $user->name,
               'email' => $user->email,
               'userGroup' => $user->userGroup,
               'issues' => $user->issues,
               'created' => $user->created_at
           ];
       }else{
           $ret['status'] = 'failed';
           $ret['msg'] = 'no user found';
       }

       return json_encode( $ret );

   }



   /*
        Update Password
   */
   public function updatePassword( $id ){

       $user = User::where('id', '=', $id )->first();

       if( count( $user ) > 0 ){

           $user->password = app('hash')->make( $_POST['password'] );

           try {
               $user->save();
               $ret['status'] = 'success';
           } catch (Exception $e) {
               $ret['status'] = 'failed';
               $ret['error'] = $e;
           }

       }
       else{
           $ret['status'] = 'failed';
           $ret['error'] = 'user does not exist';
       }

       return json_encode( $ret );

   }

    /*
          Get User Data
     */
    public function getAllUsers(  ){

        $users = User::orderBy('id', 'asc')->get();

        return json_encode( $users );

    }

    /*
          Create User
     */
    public function createUser(){

        $new = new User;
        $new->name = $name = $_POST['name'];
        $new->email = $_POST['email'];
        $new->password = app('hash')->make( $_POST['password'] );
        $new->userGroup = $_POST['userGroup'];
        $new->api_token = $_POST['api_token'];

        $preexistingUser = User::where('email', '=', $_POST['email'] )->first();

        if( count( $preexistingUser ) > 0 ){
            $ret['status'] = "failed";
            $ret['error'] = "exists";
            $ret['user']['id'] = $preexistingUser->id;
            $ret['user']['name'] = $preexistingUser->name;
        }
        else{
            try {
                $new->save();
                $ret['status'] = "success";
                $ret['user'] = $new;
            } catch (Exception $e) {
                $ret['status'] = "failed";
                $ret['error'] = $e;
            }
        }


        return json_encode( $ret );

    }

    /*
        UPDATE
        Update Name, email, password, userGroup
    */
    public function update( $id ){

        $user = User::where( "id", "=", $id )->first();

        if( count( $user ) ){
            if ( !empty( $_POST['name'] ) ){
                $user->name = $_POST['name'];
            }
            if ( !empty( $_POST['email'] ) ){
                $user->email = $_POST['email'];
            }
            if ( !empty( $_POST['password'] ) ){
                $user->password = app('hash')->make( $_POST['password'] );
            }
            if ( !empty( $_POST['userGroup'] ) ){
                $user->userGroup = $_POST['userGroup'];
            }
            if ( !empty( $_POST['issues'] ) ){
                $user->issues = $_POST['issues'];
            }


            try {
                $user->save();
                $ret['status'] = "success";
            } catch (Exception $e) {
                $ret['status'] = "failed";
                $ret['error'] = $e;
            }

        }
        else{
            $ret['status'] = "failed";
            $ret['message'] = "user not found";
        }

        return json_encode( $ret );

    }

    /*
       DELETE
    */
    public function delete( $id ){

        $user = User::where( "id", "=", $id )->first();

        try {
            $user->delete();
            $ret['status'] = "success";
            $ret['message'] = "user deleted";
        } catch (Exception $e) {
            $ret['status'] = "failed";
            $ret['error'] = $e;
        }

        return json_encode( $ret );

    }





}
