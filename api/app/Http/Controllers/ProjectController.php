<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Project;
use App\Models\Tag;
use App\Models\Campaign;
use App\Models\Pin;


class ProjectController extends Controller{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }



    /*
        CHECK IF PROJECT EXISTS
        Return the projects details
    */
    public function checkIfExists( $id ){

        $p = Project::where("id", "=", $id)->first();
        if( count( $p ) ){
            $ret['status'] = "exists";
        }else{
            $ret['status'] = "does not exist";
        }

        return json_encode( $ret );
    }

    /*
        GET PROJECT ID BY OOID
        Return the project id by the ooid
    */
    public function getIdByOOID( $ooid ){

        $p = Project::where("ooid", "=", $ooid)->first();
        if( count( $p ) ){
            $ret['id'] = $p->id;
            $ret['status'] = "exists";
        }else{
            $ret['status'] = "does not exist";
        }

        return json_encode( $ret );
    }


    /*
        SEARCH
        Search for a project
    */
    public function find(){

        $name = $_POST[ 'project_name'];
        if( isset( $_POST[ 'ad_type' ] ) ){
            $adtype = $_POST[ 'ad_type' ];
        }else{
            $adtype = null;
        }

        if( $adtype != null ){
            $projects = Project::where("name", 'LIKE', '%' . $name . '%' )
                ->where( "adtype", "=", $adtype )
                ->limit(25)
                ->get();
        }else{
            $projects = Project::where("name", 'LIKE', '%' . $name . '%' )
                ->limit(25)
                ->get();
        }


        //find campaign name
        foreach( $projects as $p ){
            if( !empty( $p->campaign ) ){
                $p->campaignName = $p->campaign->name;
                $p->advertiserName = $p->campaign->advertiser->name;
            }
        }

        return json_encode( $projects );
    }

    /*
        GET Projects
        Return the projects details
    */
    public function getProjects(){

        $name = $_POST[ 'project_name'];
        if( isset( $_POST[ 'ad_type' ] ) ){
            $adtype = $_POST[ 'ad_type' ];
        }else{
            $adtype = null;
        }

        if( $adtype != null ){
            $projects = Project::where("name", 'LIKE', '%' . $name . '%' )
                ->where( "adtype", "=", $adtype )
                ->limit(25)
                ->get();
        }else{
            $projects = Project::where("name", 'LIKE', '%' . $name . '%' )
                ->orderby("adtype")
                ->get();
        }


        //find campaign name
        foreach( $projects as $p ){
            if( !empty( $p->campaign ) ){
                $p->campaignName = $p->campaign->name;
                $p->advertiserName = $p->campaign->advertiser->name;
            }
        }

        return json_encode( $projects );
    }




    /*
        GET DETAILS
        Return the projects details
    */
    public function getDetails( $id, $startDate = null, $endDate = null ){

        $p = Project::where("id", "=", $id)->first();
        if( count( $p ) ){

            // +1 days to end date so that data includes end date
            if( $endDate != null ){
                $date = date_create( $endDate );
                date_add( $date, date_interval_create_from_date_string( "1 days" ) );
                $endDate = date_format($date,"Y-m-d");
            }

            //is in users pins
            $pinStatus = Pin::where( "userId", '=', $_GET[ 'user' ] )
                            ->where( "projectId", '=', $id )
                            ->first();
            if( count( $pinStatus ) ){
                $p->pinStatus = true;
            }else{
                $p->pinStatus = false;
            }

            //get metadata for this project
            $ret['metaData'] = $p;
            $ret['startDate'] = $startDate;
            $ret['endDate'] = $endDate;

            //add operative id to the data
            $ret['metaData']['campaign'] = $p->campaign;

            $data['tags'] = [];

            function findTag( $tag_name, $array ){
                foreach( $array as $k => $v ){
                   if( $v['name'] === $tag_name ){
                       return $k;
                   }
                }
                return null;
            }


            $tags = Tag::getDataByDay( $p->id, $startDate, $endDate );

            foreach( $tags as $t ){

                //if tag exists only add data for the day
                $existingTag = findTag( $t->tag_name, $data['tags'] );
                if( $existingTag !== null ){
                    array_push(
                        $data['tags'][ $existingTag ][ 'dataByDay' ],
                        [
                            'date' => isset( $t->time_stamp ) ? $t->time_stamp : null,
                            'total' => $t->count
                        ]
                    );
                }

                //if tag doesn't exist create a new one and push into the tags array
                else{
                    $tagInfo = [];
                    $tagInfo['name'] = $t->tag_name;
                    //$tagInfo['type'] = $t->type;

                    //data as array
                    $tagInfo['dataByDay'] = [];

                    //push this value into data array
                    array_push(
                        $tagInfo['dataByDay'],
                        [
                            'date' => isset( $t->time_stamp ) ? $t->time_stamp : null,
                            'total' => $t->count
                        ]
                    );

                    //redirect
                    if( $t->redirect_url !== "" ){
                        $tagInfo['redirect'] = $t->redirect_url;
                    }

                    //push into tags array
                    array_push( $data['tags'], $tagInfo );
                }

            }

            $ret['data'] = $data;

            return json_encode( $ret );
        }
        else{
            return 'false';
        }

    }

    /*
        GET PARENTS
        Return the projects campaign and advertiser
    */
    public function getParents( $id ){

        $p = Project::where("id", "=", $id)->first();
        if( count( $p ) ){
            $c = $p->campaign;
            $a = $c->advertiser;
            $data = [
                [
                    'type' => 'advertiser',
                    'id' => $a->id,
                    'name' => $a->name
                ],
                [
                    'type' => 'campaign',
                    'id' => $c->id,
                    'name' => $c->name
                ]
            ];

            return json_encode( $data );
        }
        else{
            return json_encode( 'project does not exist' );
        }
    }


    /*
        GET SIBLINGS
        Return max 3 of the projects siblings in the campaign
    */
    public function getSiblings( $id ){

        $p = Project::where("id", "=", $id)->first();
        if( count( $p ) ){

            $data['siblings'] = Project::where("campaignId", '=', $p->campaignId )
                                ->where('id', '!=', $id)
                                ->take(3)
                                ->get();
            $data['parent'] = $p->campaign->id;

            if( count( $data['siblings'] ) ){
                return json_encode( $data );
            }
            else{
                return json_encode( 'project has no siblings' );
            }
        }
        else{
            return json_encode( 'project does not exist' );
        }
    }


    /*
        CREATE
        Create a new project
    */
    public function create(){

        $new = new Project;
        $new->name = $name = $_POST['name'];
        $new->campaignId = $_POST['campaign_id'];
        $new->adtype = $_POST['adtype'];

        if ( !empty( $_POST['site'] ) ){
            $new->site = $_POST['site'];
        }
        if ( !empty( $_POST['placement'] ) ){
            $new->placement = $_POST['placement'];
        }

        if( !empty( $_POST['createdBy'] ) ){
            $new->created_by = $_POST['createdBy'];
        }

        if ( !empty( $_POST['ooid'] ) ){
            $p->ooid = $_POST['ooid'];
        }

        try {
            $new->save();

            $ret['status'] = "success";
            $ret['id'] = $new->id;
            $ret['name'] = $_POST['name'];

        } catch (Exception $e) {
            $ret['status'] = "failed";
            $ret['error'] = $e;
        }

        return json_encode( $ret );

    }

    /*
        UPDATE
        Update title, adtype etc
    */
    public function update( $project_id ){

        $p = Project::where( "id", "=", $project_id )->first();

        if( count( $p ) ){
            if ( !empty( $_POST['name'] ) ){
                $p->name = $_POST['name'];
            }
            if ( !empty( $_POST['adtype'] ) ){
                $p->adtype = $_POST['adtype'];
            }
            if ( !empty( $_POST['site'] ) ){
                $p->site = $_POST['site'];
            }
            if ( !empty( $_POST['placement'] ) ){
                $p->placement = $_POST['placement'];
            }
            if ( !empty( $_POST['ooid'] ) ){
                $p->ooid = $_POST['ooid'];
            }

            try {
                $p->save();
                $ret['status'] = "success";
            } catch (Exception $e) {
                $ret['status'] = "failed";
                $ret['error'] = $e;
            }

        }
        else{
            $ret['status'] = "failed";
            $ret['message'] = "project not found";
        }

        return json_encode( $ret );

    }


    /*
        GET PINS FOR USER
        Return the saved/pinned projects for a user
    */
    public function getPins( $userId ){

        $pins = Pin::where( "userId", "=", $userId )->get();
        if( count( $pins ) > 0 ){

            $ret['pins'] = [];

            //append project data for each
            foreach( $pins as $p ){

                $project = $p->project;
                $campaign = $project->campaign;
                $advertiser = $campaign->advertiser;

                $pinInfo = [
                    'id' => $p->id,
                    'project' => [
                        'id' => $project->id,
                        'name' => $project->name,
                        'adtype' => $project->adtype,
                        'site' => $project->site,
                        'placement' => $project->placement,
                        'created_at' => $project->created_at->diffForHumans(),
                        'impressions' => Tag::getImpressions( $project->id ),
                        'clicks' => Tag::getClicksTotal( $project->id )
                    ],
                    'campaign' => $campaign->name,
                    'advertiser' => $advertiser->name
                ];

                array_push( $ret['pins'], $pinInfo );

                //$project = $p->project;
                //$p->project = $project;
                //$p->project->impressions = Tag::getImpressions( $p->projectId );
                //$p->project->clicks = Tag::getClicksTotal( $p->projectId );

                //$c = $project->campaign;
                //$p['campaign'] = $c;
                //$p['advertiser'] = "test";
            }

            $ret['status'] = "success";
        }else{
            $ret['status'] = "no pins";
        }

        return json_encode( $ret );
    }


}
