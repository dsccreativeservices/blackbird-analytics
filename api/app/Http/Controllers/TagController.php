<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\GeneratedTag;


class TagController extends Controller{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }



    /*
        CREATE
        Check for existing tag first, if exists update
        Create new tag record after tag is created in tag generator
    */
    public function create( $project_id ){

        //check existing
        $existingTag = GeneratedTag::where( 'project_id', '=', $project_id )
                                    ->where( 'tag', '=', $_POST[ 'fullTag' ] )
                                    ->first();

        if( count( $existingTag ) > 0 ){
            $t = $existingTag;
            $ret['type'] = "existing tag";
        }else{
            $t = new GeneratedTag;
            $t->project_id = $project_id;
            $ret['type'] = "new tag";
        }

        $t->tag = $_POST[ 'fullTag' ];
        $t->type = $_POST[ 'type' ];
        $t->desc = $_POST[ 'desc' ];
        $t->redirect = $_POST[ 'redirect' ];

        try {
            $t->save();
            $ret['status'] = "success";
        } catch (Exception $e) {
            $ret['status'] = "error";
            $ret['error'] = $e;
        }

        return json_encode( $ret );
    }

    /*
        DELETE TAG
    */
    public function delete( $project_id ){

        //check existing
        $existingTag = GeneratedTag::where( 'project_id', '=', $project_id )
                                    ->where( 'desc', '=', $_POST[ 'desc' ] )
                                    ->first();

        try {
            $existingTag->delete();
            $ret['status'] = "success";
        } catch (Exception $e) {
            $ret['status'] = "error";
            $ret['error'] = $e;
        }

        return json_encode( $ret );
    }


    /*
        GET
        Get all tags for a project
    */
    public function getByProject( $id ){
        $tags = GeneratedTag::where("project_id", "=", $id)->get();
        return json_encode( $tags );
    }





}
