<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Campaign;
use App\Models\Tag;

class CampaignController extends Controller{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }



    /*
        FIND BY IO NUMBER
    */
    public function findByIO( $IO ){

        $campaign = Campaign::where( 'operativeOrderId', '=', $IO )->first();

        if( count( $campaign ) > 0 ){
            $ret['status'] = "success";
            $ret['campaign'] = $campaign;
            $ret['advertiser'] = $campaign->advertiser;
        }else{
            $ret['status'] = "failed";
            $ret['campaign'] = null;
        }

        return json_encode( $ret );

    }


    /*
        SEARCH
        Search for a campign
    */
    public function search(){

        $name = $_POST[ 'campaign_name'];

        $campaigns = Campaign::where("name", 'LIKE', '%' . $name . '%' )
            ->limit(25)
            ->get();

        //find advertiser name
        foreach( $campaigns as $c ){
            $c->advertiserName = $c->advertiser->name;
        }

        return json_encode( $campaigns );
    }





    /*
        GET SPECIFIC
        Return specific campaign and associated projects
    */
    public function getDetails( $id, $startDate = null, $endDate = null ){

        $campaign = Campaign::where('id', '=', $id)->first();

        if( count( $campaign ) ){
            $projects = $campaign->projects;

            //only return select info about campaign
            $data['campaign'] = [
                'id' => $campaign->id,
                'name' => $campaign->name,
                'operativeOrderId' => $campaign->operativeOrderId,
                'created_at' => \DateTime::createFromFormat('Y-m-d H:i:s', $campaign->created_at)->format("Y-m-d H:i:s"),
            ];

            //only return select info about the project instead of the full collection
            $data['projects'] = [];
            foreach( $projects as $p ){

                //build tag data for project
                $tags = Tag::getTotal( $p->id, $startDate, $endDate );
                $tagData = [];

                foreach( $tags as $t ) {
                    $tagInfo = [];
                    $tagInfo['name'] = $t->tag_name;
                    if( $t->redirect_url !== "" ){
                        $tagInfo['redirect'] = $t->redirect_url;
                    }
                    $tagInfo['total'] = $t->count;

                    array_push( $tagData, $tagInfo );
                    unset( $t );
                }
                unset( $tags );

                array_push( $data['projects'], [
                    'id' => $p->id,
                    'name' => $p->name,
                    'created_at' => \DateTime::createFromFormat('Y-m-d H:i:s', $p->created_at)->format("Y-m-d H:i:s"),
                    'tags' => $tagData
                ] );

                unset( $p );
            }

            return json_encode( $data );
        }else{
            return "no campaign";
        }



    }




    /*
        GET PARENTS
        Return the projects campaign and advertiser
    */
    public function getParents( $id ){

        $c = Campaign::where("id", "=", $id)->first();
        if( count( $c ) ){
            $a = $c->advertiser;
            $data = [
                [
                    'type' => 'advertiser',
                    'id' => $a->id,
                    'name' => $a->name
                ]
            ];

            return json_encode( $data );
        }
        else{
            return json_encode( 'campaign does not exist' );
        }
    }

    /*
        GET SIBLINGS
        Return max 3 of the campaigns siblings in the advertiser
    */
    public function getSiblings( $id ){

        $c = Campaign::where("id", "=", $id)->first();
        if( count( $c ) ){

            $data['siblings'] = Campaign::where("advertiserId", '=', $c->advertiserId )
                                ->where('id', '!=', $id)
                                ->take(3)
                                ->get();
            $data['parent'] = $c->advertiser->id;

            if( count( $data['siblings'] ) ){
                return json_encode( $data );
            }
            else{
                return json_encode( 'campaign has no siblings' );
            }
        }
        else{
            return json_encode( 'campaign does not exist' );
        }
    }



    /*
        CREATE
        Create a new campaign
    */
    public function create(){

        //if advertiser already exists
        $existingCampaign = Campaign::where( "operativeOrderId", "=", $_POST['operativeId'] )->first();
        if( count( $existingCampaign ) > 0 ){
            $ret['status'] = "failed";

            //if existing campaign belongs to a different advertiser
            if( $existingCampaign->advertiserId != $_POST['advertiserId'] ){
                $ret['error'] = "exists under different advertiser";
            }

            //if belongs to same one then use it
            else{
                $ret['error'] = "exists";
                $ret['campaign']['id'] = $existingCampaign->id;
                $ret['campaign']['name'] = $existingCampaign->name;
            }

        }

        //if no campaign exists make one
        else{
            $new = new Campaign;
            $new->name = $name = $_POST['name'];
            $new->advertiserId = $_POST['advertiserId'];
            $new->operativeOrderId = $_POST['operativeId'];

            try {
                $new->save();

                $ret['status'] = "success";
                $ret['newCampaign']['id'] = $new->id;
                $ret['newCampaign']['name'] = $name;

            } catch (Exception $e) {
                $ret['status'] = "failed";
                $ret['error'] = $e;
            }
        }

        return json_encode( $ret );

    }

    /*
        UPDATE
        Update a campaign
    */
    public function update($id){

        $c = Campaign::where( "id", "=", $id )->first();
        if( count( $c ) > 0 ){
            if ( !empty( $_POST['name'] ) ){
                $c->name = $_POST['name'];
            }
            if ( !empty( $_POST['advertiserId'] )){
                $c->advertiserId = $_POST['advertiserId'];;
            }
            if ( !empty( $_POST['operativeId'] ) ){
                $c->operativeId = $_POST['operativeId'];
            }

            try {
                $c->save();
                $ret['status'] = "success";
            } catch (Exception $e) {
                $ret['status'] = "failed";
                $ret['error'] = $e;
            }

        }

        else{
            $ret['status'] = "failed";
            $ret['message'] = "project not found";
        }

        return json_encode( $ret );

    }





}
