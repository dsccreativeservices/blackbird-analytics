<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Project;
use App\Models\Campaign;
use App\Models\Advertiser;


class SearchController extends Controller{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }



    /*
        Search by keyword
    */
    public function search(){

        $keyword = $_POST[ 'keyword' ];

        $ret['advertisers'] = Advertiser::where("name", 'LIKE', '%' . $keyword . '%' )
            ->limit(25)
            ->get();

        $ret['campaigns'] = Campaign::where("name", 'LIKE', '%' . $keyword . '%' )
            ->limit(25)
            ->get();

        if( $_POST[ 'ad_type' ] != null ){
            $ret['projects'] = Project::where("name", 'LIKE', '%' . $keyword . '%' )
                ->where( "adtype", "=", $_POST[ 'ad_type' ] )
                ->limit(25)
                ->get();
        }
        else{
            $ret['projects'] = Project::where("name", 'LIKE', '%' . $keyword . '%' )
                ->limit(25)
                ->get();
        }

        return json_encode( $ret );
    }

}
