<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Pin;

class PinController extends Controller{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }



    /*
        Toggle Pin Status
    */
    public function toggleStatus( $projectId, $userId ){

        $pin = Pin::where( 'projectId', '=', $projectId )
                  ->where( 'userId', '=', $userId )
                  ->first();

        //if pin exists, remove it
        if( count( $pin ) > 0 ){

            try {
                $pin->delete();
                $ret['status'] = "success";
                $ret['message'] = "pin removed";
            } catch (\Exception $e) {
                $ret['status'] = "failed";
                $ret['error'] = $e;
            }

        }

        //else create it
        else{

            $newPin = new Pin;
            $newPin->userId = $userId;
            $newPin->projectId = $projectId;

            try {
                $newPin->save();
                $ret['status'] = "success";
                $ret['message'] = "pin added";
            } catch (\Exception $e) {
                $ret['status'] = "failed";
                $ret['error'] = $e;
            }

        }

        return json_encode( $ret );

    }




}
