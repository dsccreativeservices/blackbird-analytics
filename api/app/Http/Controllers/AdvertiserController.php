<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Advertiser;

class AdvertiserController extends Controller{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        //$this->middleware('auth');
    }

    /*
        GET ALL
        Return all advertisers
    */
    public function getAll(){

        $advertisers = Advertiser::orderBy('name', 'asc')->get();

        return json_encode( $advertisers );
    }






    /*
        GET SPECIFIC
        Return specifix advertiser and associated campaigns
    */
    public function getDetails( $id ){

        $advertiser = Advertiser::where('id', '=', $id)->first();
        $campaigns = $advertiser->campaigns;

        $data['advertiser'] = [
            'id' => $advertiser->id,
            'name' => $advertiser->name,
            'created_at' => \DateTime::createFromFormat('Y-m-d H:i:s', $advertiser->created_at)->format("Y-m-d H:i:s"),
        ];

        $data['campaigns'] = [];

        foreach( $campaigns as $c ){
            array_push( $data['campaigns'], [
                'id' => $c->id,
                'name' => $c->name,
                'operativeOrderId' => $c->operativeOrderId,
                'created_at' => \DateTime::createFromFormat('Y-m-d H:i:s', $c->created_at)->format("Y-m-d H:i:s"),
                'projectCount' => $c->projects()->count()
            ] );
            //$c->projectCount = $c->projects()->count();
        }

        return json_encode( $data );
    }




    /*
        GET CAMPAIGNS
        Return campaigns for a specific advertiser
    */
    public function getCampaigns( $id ){

        $advertiser = Advertiser::where('id', '=', $id)->first();

        if( $advertiser == null ){
            $data['campaigns'] = null;
        }else{
            $data['campaigns'] = $campaigns = $advertiser->campaigns;
        }

        return json_encode( $data );
    }






    /*
        CREATE
        Create a new advertiser
        Update the static advertiser JS file
    */
    public function create(){

        //if advertiser already exists
        $existingAdvertiser = Advertiser::where( "name", "=", $_POST['name'] )->first();
        if( count( $existingAdvertiser ) > 0 ){
            $ret['status'] = "failed";
            $ret['error'] = "exists";
            $ret['advertiser']['id'] = $existingAdvertiser->id;
            $ret['advertiser']['name'] = $existingAdvertiser->name;
        }

        //if no advertiser exists make one
        else{
            $new = new Advertiser;
            $new->name = $name = $_POST['name'];

            try {
                $new->save();
                $ret['status'] = "success";
                $ret['newAdvertiser']['id'] = $new->id;
                $ret['newAdvertiser']['name'] = $name;
            } catch (Exception $e) {
                $ret['status'] = "failed";
                $ret['error'] = $e;
            }
        }

        return json_encode( $ret );

    }


    /*
        UPDATE
        Update current Adverser
    */
    public function update($id){

        //if advertiser already exists
        $a = Advertiser::where( "id", "=", $id )->first();
        if( count( $a )){
            if ( !empty( $_POST['name'] ) ){
                $a->name = $_POST['name'];
            }

            try {
                $a->save();
                $ret['status'] = "success";
            } catch (Exception $e) {
                $ret['status'] = "failed";
                $ret['error'] = $e;
            }
        }
        else{
            $ret['status'] = "failed";
            $ret['message'] = "project not found";
        }

        return json_encode( $ret );

    }





    /*
        SEARCH
        Search for a advertiser
    */
    public function search(){

        $name = $_POST[ 'advertiser_name'];

        $advertisers = Advertiser::where("name", 'LIKE', '%' . $name . '%' )
            ->limit(25)
            ->get();

        //find advertiser name
        foreach( $advertisers as $a ){
            $a->campaigns = $a->campaigns;
        }

        return json_encode( $advertisers );
    }
}
