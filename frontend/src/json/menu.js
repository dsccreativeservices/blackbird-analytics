export const MenuData = [
    {
        id: "create",
        url: "/project/create",
        text: "Create Project",
        icon: "pencil",
        class: 'writeAccessRequired'
    }, {
        id: "advertisers",
        url: "/advertiser/all",
        text: "Advertisers",
        icon: "layers"
    }, {
        id: "search",
        url: "/search",
        text: "Search",
        icon: "search"
    },
    // {
    //     id: "benchmarks",
    //     url: "/benchmarks",
    //     text: "Benchmarks",
    //     icon: "trending-up"
    // },
    {
        id: "account",
        url: "/account",
        text: "My Account",
        class: "flex-align-right",
        icon: "home"
    }, {
        id: "logout",
        url: "/logout",
        text: "Logout",
        icon: "lock"
    }
];



export const FooterMenuData = [
    {
        category: 'advertisers',
        items: [
            {
                id: "advertisers",
                url: "/advertiser/all",
                text: "Browse All Advertisers"
            }
        ]
    },
    {
        category: 'campaigns',
        items: [
            {
                id: "campaigns",
                url: "/search/campaign",
                text: "Find a Campaign"
            }
        ]
    },
    {
        category: 'projects',
        items: [
            {
                id: "create",
                url: "/project/create",
                text: "Create a Project"
            }, {
                id: "find",
                url: "/search/project",
                text: "Find a Project"
            }
        ]
    },
    {
        category: 'benchmarks',
        items: [
            {
                id: "browse",
                url: "/benchmarks/all",
                text: "Browse All Benchmarks"
            }, {
                id: "create",
                url: "/benchmarks/create",
                text: "Create Custom Benchmark"
            }
        ]
    },
    {
        category: 'account',
        items: [
            {
                id: "profile",
                url: "/account",
                text: "Account"
            }, {
                id: "issueTrackerClick",
                url: "",
                text: "Issue Tracker"
            }, {
                id: "logout",
                url: "/logout",
                text: "Logout"
            }
        ]
    }
];
