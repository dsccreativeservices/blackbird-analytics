export const Global = {
  // apiEndpoint: 'http://localhost/Programs/Blackbird/api/public',
  // apiEndpoint: 'https://api.sni-blackbird.com',
  apiEndpoint: 'http://blackbirdadtracking-env-dev.us-east-1.elasticbeanstalk.com',
  colorArray: [
    '#C73369', // pink/red
    '#2F80ED', // blue
    '#5DC7D1', // blue-green
    '#C8D444', // green
    '#F18933', // orange
    '#223E6B', // dark blue
    '#F9CF46', // yellow
    '#BD4432', // burnt/dark red
    '#0E0D0E', // almost black
  ],
  minTagCount: 10, // min number of events for a tag to be considered active
  // minTagCount: 0, //min number of events for a tag to be considered active
  radix: 10,
  userRoles: [
    { id: 0, name: 'admin' },
    { id: 1, name: 'editor' },
    { id: 3, name: 'viewer' },
  ],

  // possible values for overall-impression
  overallImpressionTagName: 'blackbird-overall',
  overallImpressionThreshold: 20, // less than that considered preview not live
  // overallImpressionThreshold: 1, //less than that considered preview not live
  overallTagNames: [
    'blackbird-overall',
    'overall',
    'overalls',
    'overall-impression',
    'overall-impressions',
    'Overall_Impression',
    'Total_Impressions',
  ],
  overallImpressionPlaceholder: '---,---',

  viewTagOptions: [
    'engagedUser',
    // time spent
    'time05',
    'time10',
    'time15',
    'time20',
    'time30',
    'time1min',
    'time2min',
    'time3min',
    'time4min',
    'time5min',
    // video
    '_start',
    '25',
    '50',
    '75',
    '100',
    '0percent',
    '25percent',
    '50percent',
    '75percent',
    '100percent',
    'zero',
    'quarter',
    'half',
    'threequarter',
    'threeQuarter',
    'full',
    'video-25',
    'video-50',
    'video-75',
    'video-100',
    'video-play',
    // 'video1_',
    'Video_',
    '-autoplay',
    '_autoplay',
    // carousel slides - not back/next/pagination
    '_slide',
    'carousel_',
  ],

  // videoTagThreshold: 30, //less than that considered preview not live
  videoTagThreshold: 0, // less than that considered preview not live
  videoTagOptions: [
    'zero',
    'quarter',
    'half',
    'threeQuarter',
    'full',
    '_start',
    '25',
    '50',
    '75',
    '100',
    'video',
    'Video_',
    '0percent',
    '25percent',
    '50percent',
    '75percent',
    '100percent',
    'threequarter',
    '_autoplay',
    '-autoplay',
  ],

  carouselTagOptions: [
    'carousel_',
    '_slide',
    'slide1',
    'slide2',
    'slide3',
    'slide4',
    'slide5',
    'slide6',
  ],

  // data capture API endpoints
  // used for building tags
  baseTags: {
    click: 'https://ruv80zbas1.execute-api.us-east-1.amazonaws.com/prod/jump?redirect_url={{ redirect }}&creative_id={{ creativeId }}&tag_name={{ tagName }}&operative_id={{ operativeId }}',
    imp: 'https://ruv80zbas1.execute-api.us-east-1.amazonaws.com/prod/view?creative_id={{ creativeId }}&operative_id={{ operativeId }}&tag_name={{ tagName }}',
  },

  // adTypes
  // matches mockingbird
  adTypes: [
    {
      id: 'adtype_1',
      text: 'Leaderboard',
    }, {
      id: 'adtype_2',
      text: 'Pushdown',
    }, {
      id: 'adtype_3',
      text: 'Brandscape',
    }, {
      id: 'adtype_4',
      text: 'Bigbox 300x250',
    }, {
      id: 'adtype_5',
      text: 'RSI Module',
    }, {
      id: 'adtype_15',
      text: 'Bigbox 300x150',
    }, /* {
            id: 'adtype_24',
            text: "Tablet"
        }, */{
      id: 'adtype_26',
      text: 'Half Page',
    }, {
      id: 'adtype_27',
      text: 'Portrait',
    }, /* {
            id: 'adtype_28',
            text: "Videoscape"
        }, */{
      id: 'adtype_29',
      text: 'Video Overlay',
    }, {
      id: 'adtype_30',
      text: 'Photo Gallery Interstitial',
    }, {
      id: 'adtype_31',
      text: 'Filmstrip',
    }, {
      id: 'adtype_32',
      text: 'Sidekick',
    }, /* {
            id: 'adtype_33',
            text: "Baseboard"
        }, */{
      id: 'adtype_34',
      text: 'Sidescape',
    }, {
      id: 'adtype_35',
      text: 'Sweepstakes',
    }, {
      id: 'adtype_36',
      text: 'Microsite - Hub',
    }, /* {
            id: 'adtype_38',
            text: "Hover"
        }, */{
      id: 'adtype_39',
      text: 'Interactive Pre-Roll',
    }, {
      id: 'adtype_40',
      text: 'Newsletter',
    }, {
      id: 'adtype_41',
      text: 'Other',
    }, {
      id: 'adtype_42',
      text: 'Billboard Video',
    }, {
      id: 'adtype_43',
      text: 'Pushdown Showcase',
    }, {
      id: 'adtype_44',
      text: 'Pre-Roll Sync - 300x60',
    }, {
      id: 'adtype_45',
      text: 'Mobile - Tap to Expand',
    }, {
      id: 'adtype_46',
      text: 'Mobile - Pushdown',
    }, {
      id: 'adtype_47',
      text: 'Mobile - Standard',
    }, {
      id: 'adtype_48',
      text: 'Mobile - Interstitial',
    }, {
      id: 'adtype_49',
      text: 'Native - 3 Across',
    }, {
      id: 'adtype_51',
      text: 'Native - 1 Across',
    }, {
      id: 'adtype_52',
      text: 'Snapchat',
    }, {
      id: 'adtype_53',
      text: 'Native Ingredient',
    }, {
      id: 'adtype_54',
      text: 'Native - 2 Across',
    }, {
      id: 'adtype_55',
      text: 'Infographic',
    }, {
      id: 'adtype_56',
      text: 'Ratio[ 2x1 ]',
    }, {
      id: 'adtype_57',
      text: 'Ratio[ 3x1 ]',
    }, {
      id: 'adtype_58',
      text: 'Ratio[ 4x1 ]',
    }, {
      id: 'adtype_59',
      text: 'Ratio[ 6x1 ]',
    }, {
      id: 'adtype_60',
      text: 'Ratio[ 8x1 ]',
    }, {
      id: 'adtype_61',
      text: 'Ratio[ 10x1 ]',
    }, {
      id: 'adtype_62',
      text: 'Ratio[ 1x1 ]',
    }, {
      id: 'adtype_63',
      text: 'Mobile - Parallax',
    }, {
      id: 'adtype_64',
      text: 'Product Record',
    }, {
      id: 'adtype_65',
      text: 'Photo Gallery Hotspots',
    }, {
      id: 'adtype_66',
      text: 'Interactive Article',
    },
  ],

  // sites
  // matches mockingbird
  // older sites are left out
  sites: [
    {
      id: 'site_13',
      text: 'Run Of Food',
    }, {
      id: 'site_1',
      text: 'Food Network',
    }, {
      id: 'site_9',
      text: 'Genius Kitchen',
    }, {
      id: 'site_12',
      text: 'Cooking Channel',
    }, {
      id: 'site_14',
      text: 'Run Of Home',
    }, {
      id: 'site_2',
      text: 'HGTV',
    }, {
      id: 'site_3',
      text: 'DIY Network',
    }, {
      id: 'site_6',
      text: 'Travel Channel',
    }, {
      id: 'site_11',
      text: 'GAC',
    }, {
      id: 'site_15',
      text: 'Run Of Scripps',
    }, {
      id: 'site_17',
      text: 'Animal Planet',
    }, {
      id: 'site_13',
      text: 'Discovery Channel',
    }, {
      id: 'site_18',
      text: 'Investigation Discovery',
    }, {
      id: 'site_19',
      text: 'Motor Trend',
    }, {
      id: 'site_20',
      text: 'Opera Winfery Network',
    }, {
      id: 'site_21',
      text: 'Science Channel',
    }, {
      id: 'site_22',
      text: 'TLC',
    }, {
      id: 'site_23',
      text: 'TLCme',
    },

  ],

  // placements
  placements: [
    {
      id: 'placement_1',
      text: 'PT',
    }, {
      id: 'placement_2',
      text: 'AT',
    },
  ],

  // used when exporting data to XLS
  // for RBG start with FF for 100% opacity, then normal RGB after
  exportStyles: {
    header: {
      alignment: {
        vertical: 'top',
      },
      /* fill: {
                fgColor: {
                    rgb: 'FF242C3A'
                }
            }, */
      font: {
        color: {
          rgb: 'FF5A6C8E',
        },
        sz: 16,
        bold: true,
        underline: false,
      },
      border: {
        bottom: {
          style: 'medium',
          color: {
            rgb: 'FF2F80ED',
          },
        },
      },
    },

    // name cells
    text: {
      font: {
        sz: 14,
        bold: false,
        color: {
          rgb: 'FF515151',
        },
      },
    },

    url: {
      font: {
        sz: 12,
        bold: false,
        color: {
          rgb: 'FF515151',
        },
      },
    },

    number: {
      alignment: {
        horizontal: 'left',
      },
      font: {
        sz: 14,
        bold: true,
        color: {
          rgb: 'FF515151',
        },
      },
    },

  },

};

// merge overall tag options with viewTag options
Global.viewTagOptions = Global.viewTagOptions.concat(Global.overallTagNames);
