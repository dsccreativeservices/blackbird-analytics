import React from 'react';
import { PageTitle } from './Typography';
import { Icon } from './Icons';
import {GetData, GetOverallImpressions, SortAlphabetically, formatNumber} from "./Helpers";
import {Global} from "../json/global";
import {Dropdown, FormField} from "./Inputs";
import {Button} from "./Buttons";

/*
    CREATE NEW PROJECT
*/
export class Benchmark extends React.Component {

    constructor(props){
        super( props );

        this.state = {
            projectOverall:[],
            adtype: null,
            loading: false,
            filterVideo: false,
            filterNoVideo: false,
            filterCarousel: false,
            filterSite: 0,
            sortValue: "",
            averageImpressions: 0,
            averageCTR: 0,
            averageIR: 0,
            averageER: 0,
            selectedID: null
        };

        this.processData = this.processData.bind( this );
        this.search = this.search.bind( this );
        this.filter = this.filter.bind( this );
        this.setSortValue = this.setSortValue.bind(this);
        this.convertToNumber = this.convertToNumber.bind(this);
        this.setAverage = this.setAverage.bind(this);
    }

    getData( adType, startDate, endDate ){
        let dataURL = `${ Global.apiEndpoint }/benchmarks/${ adType}`;
        if( typeof startDate !== "undefined" && typeof endDate !== "undefined" ){
            dataURL = `${ Global.apiEndpoint }/benchmarks/${ adType }/${ startDate }/${ endDate }`;
        }
        console.log("dataURL",dataURL);
        fetch( dataURL, {
            headers: {
                'apiToken': GetData( 'apiKey', 'session' )
            }
        } )
            .then( ( response ) => {
                return response.text();
            } )
            .then( ( data ) => {
                //console.log( data );
                var dataObj = JSON.parse( data );
                console.log( dataObj );
                console.log( dataObj.length );
                if( dataObj.length === 0 ){
                    console.log( 'no data found' );
                }else{
                    console.log("recived data")
                    this.processData( dataObj, startDate, endDate );
                }
            } );
    }

    /*
        Sort non-click tags into appropriate buckets
    */
    viewTagSort( tag, data ){

        let isViewTag = Global.viewTagOptions.find( function( option ){
            if( tag.name.includes( option ) ){
                return true;
            }
            else{
            }
        } );

        if( typeof isViewTag !== "undefined" ){
            //video tag
            if( tag.name.indexOf("video_") >= 0 || Global.videoTagOptions.some( function( option ){return tag.name.indexOf( option ) >= 0; } ) ){
                data.videoTags.push( tag );
            }
            //carousel tag
            else if(tag.name.indexOf("carousel_") >= 0 || Global.carouselTagOptions.some( function( option ){ return tag.name.indexOf( option ) >= 0; } ) ){
                data.carouselTags.push( tag );
            }
            //time tag
            else if( tag.name.includes( "time" ) ){
                data.timeTags.push( tag );
            }
            //other non-interaction view tag
            else{
                data.viewTags.push( tag );
            }
        }else{
            data.interactionTags.push( tag );
        }
    }

    processData(dataObj, startDate, endDate){
        console.log("processing data");


        dataObj.forEach(d => {
            let newProjectOverallArray = this.state.projectOverall;
            const tags = d.data.tags;
            const data = d.data;

            const overallImpressions = GetOverallImpressions(tags, true);
            console.log(overallImpressions);

            data.overallImpressionsByDay = overallImpressions.dataByDay;
            data.overallImpressions = overallImpressions.total > 0 ? overallImpressions.total : Global.overallImpressionPlaceholder;


            //CHECK THE
            const clickTags = tags.filter( ( tag ) => {
                return typeof tag.redirect !== "undefined";
            } );
            //data.clickTags = clickTags.sort( this.sortArray( 'name', 'asc' ) );
            data.clickTags = clickTags.sort( SortAlphabetically( 'name' ) );

            //for each tag, count up total clicks for all days
            for( var i = 0; i < clickTags.length; i++ ){
                clickTags[i].total = clickTags[i].dataByDay.reduce( ( total, day ) => {
                    return total += parseInt( day.total, Global.radix );
                }, 0 );
            }

            //count up totals
            data.totalClicks = clickTags.reduce( ( total, tag ) => {
                return total += tag.total;
            }, 0 );

            if( data.totalClicks === 0 ){
                data.totalClicks = "--";
            }


            let interactionTags = tags.filter( ( tag ) => {
                return typeof tag.redirect === "undefined"; //temp until type works
            } );
            //interactionTags = interactionTags.sort( this.sortArray( 'name', 'asc' ) );
            interactionTags = interactionTags.sort( SortAlphabetically( 'name' ) );

            console.log(interactionTags);

            //temp until TYPE works
            data.interactionTags = [];
            data.viewTags = [];
            data.carouselTags = [];
            data.videoTags = [];
            data.timeTags = [];

            for( var j = 0; j < interactionTags.length; j++ ){

                let tag = interactionTags[j];

                //temp until TYPE works, split data
                /*if( Global.viewTagOptions.indexOf( tag.name ) > -1 ){
                    data.viewTags.push( tag );
                }else{
                    data.interactionTags.push( tag );
                }*/
                this.viewTagSort( tag, data );
                /*let isViewTag = Global.viewTagOptions.find( function( option ){
                    if( tag.name.includes( option ) ){
                        return true;
                    }
                } );
                if( typeof isViewTag !== "undefined" ){
                    data.viewTags.push( tag );
                }else{
                    data.interactionTags.push( tag );
                }*/


                //for each tag, count up total interactions for all days
                tag.total = tag.dataByDay.reduce( ( total, day ) => {
                    return total += parseInt( day.total, Global.radix );
                }, 0 );

            }

            console.log(data.videoTags);

            //count up totals
            data.totalInteractions = data.interactionTags.reduce( ( total, tag ) => {
                return total += tag.total;
            }, 0 );

            if (data.overallImpressions !== Global.overallImpressionPlaceholder) {
                console.log("Overall inpressions greater than placeholder");
                console.log(data);
                /*data.overallImpressionsByDay = overallTag.dataByDay;
                data.overallImpressions = data.overallImpressionsByDay.reduce( ( total, day ) => {
                                              return total += parseInt( day.total, Global.radix );
                                          }, 0 );*/

                if (data.totalClicks !== "--") {
                    /*
                        CLICK EVENTS
                        Calculate CTR
                    */
                    data.CTR = ((data.totalClicks / data.overallImpressions) * 100).toFixed(2) + "%";


                    /*
                        INTERACTION EVENTS
                        Calculate ER and IR
                    */
                    data.ER = ((data.engagedUsersTotal / data.overallImpressions) * 100).toFixed(2) + "%";
                    data.IR = ((data.totalInteractions / d.data.overallImpressions) * 100).toFixed(2) + "%";
                }
                else {
                    /*
                       CLICK EVENTS
                       Calculate CTR
                   */
                    data.CTR = "NaN%";


                    /*
                        INTERACTION EVENTS
                        Calculate ER and IR
                    */
                    data.ER = "NaN%";
                    data.IR = "NaN%";
                }

                /*
                  DATA START & END
                  find last day of data set
                  Use overall impressions to determine the last time the ad served
                */
                //if start date is from user selected date-range remove the erroneous previous day that shouldn't be part of dataset
                if (startDate !== null) {
                    data.overallImpressionsByDay.shift();
                }
                if(data.overallImpressionsByDay.length>0){
                    data.firstDay = data.overallImpressionsByDay[0].date;
                    data.lastDay = data.overallImpressionsByDay[data.overallImpressionsByDay.length - 1].date;
                }


                // console.log(this.state.projectOverall);


                if( data.overallImpressions > 5000){
                    if(data.CTR === "NaN%"){
                        newProjectOverallArray.push({metaData: d.metaData, data: d.data});
                    }
                    else if(parseInt(data.CTR) < 50){
                        newProjectOverallArray.push({metaData: d.metaData, data: d.data});
                    }

                }


            }
            // else{
            //     data.totalClicks = "--";
            //     data.CTR = "NaN%";
            //     data.ER = "NaN%";
            //     data.IR = "NaN%";
            //
            //     newProjectOverallArray.push({metaData: d.metaData, data: d.data});
            // }

            console.log(newProjectOverallArray);

            this.setState( {
                projectOverall: newProjectOverallArray,
                loading: false,
                selectedID: null
            } );

        });

        console.log("Prefilter state",this.state);

        this.filter("");

        console.log("Processssed data");
        console.log("Processed state",this.state);
    }

    search(){
        this.setState( {
            projectOverall: [],
            loading: true,
            sortValue: ""
        } );
        console.log(this.state);
        const adtype_input = document.querySelector( '.form-item[data-content="adtype"]' );
        let adtype = adtype_input.querySelector( 'select' ).value;

        adtype = adtype.replace( "adtype_", "" );

        console.log("adtype",adtype);

        this.getData(adtype)
    }

    filter(filterType){
        console.log((filterType));

        let data= {
            filterSite: this.state.filterSite,
            filterVideo: this.state.filterVideo,
            filterNoVideo: this.state.filterNoVideo,
            filterCarousel: this.state.filterCarousel,
            averageImpressions: 0,
            averageCTR: 0,
            averageIR: 0,
            averageER: 0,
            average100: 0,
            average75: 0,
            average50: 0,
            average25: 0,
            averageStart: 0
        },
            avgImp=[], avgCTR=[], avgER=[], avgIR=[], avg100=[], avg75=[], avg50=[], avgStart=[];

        console.log(data);

        if(filterType.includes("site_")){
            let siteNumber = filterType.replace( "site_", "" );
            data.filterSite=parseInt(siteNumber);
        }
        else{
            switch(filterType) {
                case "video":
                    data.filterVideo = !this.state.filterVideo;
                    data.filterNoVideo = false;
                    break;
                case "noVideo":
                    data.filterNoVideo = !this.state.filterNoVideo;
                    data.filterVideo = false;
                    break;
                case "carousel":
                    data.filterCarousel = !this.state.filterCarousel;
                    break;
                default:
                    data.filterSite = 0;
                    data.filterVideo = false;
                    data.filterNoVideo = false;
                    data.filterCarousel = false;
            }
        }

        console.log(data);

        let projects = this.state.projectOverall.map(p=> {
            p.show = false;

            if(!data.filterVideo && !data.filterNoVideo && !data.filterCarousel && data.filterSite === 0){p.show = true; console.log("Showing because: No filter", p)}

            else if(parseInt(p.metaData.site) === parseInt(data.filterSite)){
                if(data.filterVideo || data.filterCarousel || data.filterNoVideo){
                    //If filtering video and tags length > 0
                    if(data.filterVideo && p.data.videoTags.length > 0){p.show = true; console.log("Showing because:" +
                        " Site selected & Video filter", p)}
                    else if(data.filterNoVideo && p.data.videoTags.length === 0){p.show = true; console.log("Showing" +
                        " because: Site selected & No Video filter", p)}
                    else if(data.filterCarousel && p.data.carouselTags.length > 0){p.show = true; console.log("Showing" +
                        " because: Site selected & Carousel filter", p)}
                    else{p.show = false}
                }
                else{p.show = true; console.log("Showing because:" +
                    " Site selected & No filter", p)}
            }
            else if(data.filterSite === 0){
                if(data.filterVideo || data.filterCarousel || data.filterNoVideo){
                    if(data.filterVideo && p.data.videoTags.length > 0){p.show = true; console.log("Showing because:" +
                        " No Selected site & Video filter", p)}
                    else if(data.filterNoVideo && p.data.videoTags.length === 0){p.show = true; console.log("Showing" +
                        " because: No Selected site & No Video filter", p)}
                    else if(data.filterCarousel && p.data.carouselTags.length > 0){p.show = true; console.log("Showing because:" +
                        " No Selected site & Video filter", p)}
                    else{p.show = false; console.log("Showing because:" +
                        " No Selected site & Not filtered", p)}
                }
                else{p.show = false; console.log("Showing because:" +
                    " Not Selected site", p)}
            }

            if(p.show){
                avgImp.push(p.data.overallImpressions);
                if(this.convertToNumber(p.data.CTR) > 0){
                    avgCTR.push(this.convertToNumber(p.data.CTR));
                }
                if(this.convertToNumber(p.data.ER) > 0){
                    avgER.push(this.convertToNumber(p.data.ER));
                }
                if(this.convertToNumber(p.data.IR) > 0){
                    avgIR.push(this.convertToNumber(p.data.IR));
                }
                if(data.filterVideo && p.data.videoTags.length > 0){

                }
            }

            return p;
        });

        function sum(total, num) {
            return total + num;
        }

        console.log(projects);

        console.log(avgImp);
        if(avgImp.length > 0){
            data.averageImpressions = avgImp.reduce(sum);
            data.averageImpressions = data.averageImpressions/ avgImp.length;
        }

        console.log("avgCTR",avgCTR);
        if(avgCTR.length > 0) {
            data.averageCTR = avgCTR.reduce(sum);
            data.averageCTR = data.averageCTR / avgCTR.length;
        }


        console.log("avgER",avgER);
        if(avgER.length > 0){
            data.averageER = avgER.reduce(sum);
            data.averageER = data.averageER/avgER.length;
        }


        console.log("avgIR",avgIR);
        if(avgIR.length > 0){
            data.averageIR = avgIR.reduce(sum);
            data.averageIR = data.averageIR/avgIR.length;
        }

        this.setState({
            projectOverall: projects,
            averageImpressions: data.averageImpressions.toFixed(0),
            averageCTR: data.averageCTR.toFixed(2),
            averageIR: data.averageIR.toFixed(2),
            averageER: data.averageER.toFixed(2),
            filterSite: data.filterSite,
            filterVideo: data.filterVideo,
            filterNoVideo: data.filterNoVideo,
            filterCarousel: data.filterCarousel
        });

    }

    setSortValue(sortID){
        if(sortID === this.state.sortValue){
            this.setState({sortValue: this.state.sortValue += "-1"})
        }
        else{
            this.setState({sortValue: sortID})
        }

    }

    setAverage(impression, ir, er, ctr){
        let projectLength = this.state.projectsLength + 1;
        let thisImpressions = this.state.overallImpressions + impression;

        this.setState({
            overallImpressions: thisImpressions,
            averageImpressions: thisImpressions / projectLength,
            projectsLength: projectLength
        });
    }

    convertToNumber(string){
        // console.group(`Convert to string ${string}`);
        if(typeof string === "string"){
            // console.log("Starting String", string);
            string = string.toLowerCase();
            // console.log("Lowercase", string);
            string = string.replace("%", "");
            // console.log("Minus %", string);
            string = string.replace("nan", "-1");
            // console.log("Removed nan", string);
            string = parseFloat(string);
            // console.log('Ending string',string);
        }
        // console.groupEnd();
        return string
    }

    render() {
        return (
            <section className="benchmarks">

                <header className="headerSlantedEdge background-backgroundDark color-onDarkBackground padding-y-xlarge text-align-center">
                    <Icon src='trending-up' />
                    <PageTitle classes="color-white">Benchmarks</PageTitle>
                </header>

                <div className="mainContent">
                    <div className="innerWrap medium">
                        <div className="innerWrap-inner background-background padding-x-xlarge padding-y-xlarge">
                            <div className="form-row flex">
                                <Dropdown
                                    wrapClass="sixth-width flex-grow"
                                    inputClass="bold"
                                    name="adtype"
                                    label="Adtype"
                                    placeholder="Select Adtype"
                                    required="true"
                                    options={
                                        Global.adTypes.filter(a => {
                                            let hideList =["adtype_42","adtype_3","adtype_39","adtype_48","adtype_46","adtype_45","adtype_65","adtype_30","adtype_44","adtype_2","adtype_43","adtype_64","adtype_62","adtype_61","adtype_60","adtype_59","adtype_58","adtype_56","adtype_52","adtype_29","adtype_5"];

                                            if(hideList.indexOf(a.id) < 0){
                                                return a
                                            }
                                        }).sort( SortAlphabetically( 'text' ) )
                                    }
                                    //onChange={ this.selectCampaign }
                                />
                                <Button
                                    loader="false"
                                    classes="inline"
                                    text="Search"
                                    onClick={()=>{this.search()}}
                                />
                            </div>

                            <div className="form-row flex margin-top-small">
                                <Button
                                    loader="false"
                                    classes={`${this.state.filterVideo ? "color-green" : "color-gray"} inline`}
                                    text="Has Video"
                                    onClick={()=>{this.filter("video")}}
                                />
                                <Button
                                    loader="false"
                                    classes={`${this.state.filterNoVideo ? "color-green" : "color-gray"} inline`}
                                    text="Has No Video"
                                    onClick={()=>{this.filter("noVideo")}}
                                />
                                <Button
                                    loader="false"
                                    classes={`${this.state.filterCarousel ? "color-green" : "color-gray"} inline`}
                                    text="Has Carousel"
                                    onClick={()=>{this.filter("carousel")}}
                                />
                                <Dropdown
                                    wrapClass="sixth-width flex-grow"
                                    inputClass="bold hideSearch showReset"
                                    name="site"
                                    label="Site"
                                    placeholder="Select Site"
                                    options={
                                        Global.sites.sort( SortAlphabetically( 'text' ) )
                                    }
                                    onChange={(e)=>{this.filter(e.target.value)} }
                                />
                                <Button
                                    loader="false"
                                    classes={`inline`}
                                    text="Reset"
                                    onClick={()=>{this.filter("reset")}}
                                />
                                {/*<h1>Selected Site ID: {this.state.filterSite}</h1>*/}
                            </div>
                            {
                                this.state.loading  ? "" :
                                    this.state.projectOverall.length > 0 && <div className={"form-row flex averages"}>
                                        <span>Average Impressions: {formatNumber(this.state.averageImpressions)}</span>
                                        <span>Average CTR: {formatNumber(this.state.averageCTR)}%</span>
                                        {/*<span>Average ER: {formatNumber(this.state.averageER)}%</p>*/}
                                        <span>Average IR: {formatNumber(this.state.averageIR)}%</span>
                                    </div>
                            }

                            {
                                this.state.loading  ? "Loading" :
                                this.state.projectOverall.length > 0 && <table>
                                        <thead>
                                            <td className={"bold"}>Project Name</td>
                                            <td className={"bold"}>Campaign</td>
                                            <td className={"click bold"} onClick={()=>this.setSortValue("overallImpressions")}>Overall Impressions</td>
                                            <td className={"click bold"} onClick={()=>this.setSortValue("CTR")}>CTR</td>
                                            {/*<td onClick={()=>this.setState({sortValue: "ER"})}>ER</th>*/}
                                            <td className={"click bold"} onClick={()=>this.setSortValue("IR")}>IR</td>
                                            {/*<td>Has Video</td>*/}
                                            {/*<td>Has Carousel</td>*/}
                                            {/*<td>Site ID</td>*/}
                                            <td className={"bold"}>Info</td>
                                        </thead>
                                        <tbody>
                                        {
                                            this.state.projectOverall.filter(p=>{

                                                if(p.show){return p}

                                                // let allow=false;
                                                //
                                                // //If not filtering return
                                                // if(!this.state.filterVideo && !this.state.filterCarousel && this.state.filterSite === 0){allow = true}
                                                // //If the site is the filtered site continue
                                                // else if(parseInt(p.metaData.site) === parseInt(this.state.filterSite)){
                                                //
                                                //     if(this.state.filterVideo || this.state.filterCarousel){
                                                //         //If filtering video and tags length > 0
                                                //         if(this.state.filterVideo && p.data.videoTags.length > 0){allow=true}
                                                //
                                                //         //If filtering carusel  and carousel tags greater than 0
                                                //         if(this.state.filterCarousel && p.data.carouselTags.length > 0){allow=true}
                                                //     }
                                                //     else{allow=true}
                                                // }
                                                // if(allow){return p}
                                            }).sort((a,b)=> {
                                                if(this.state.sortValue !== ""){
                                                    if(this.state.sortValue.includes("-1")){
                                                        let newSortValue = this.state.sortValue.replace("-1","");
                                                        return this.convertToNumber(a.data[newSortValue])>this.convertToNumber(b.data[newSortValue]) ? 1 : -1;
                                                    }
                                                    else{
                                                        return this.convertToNumber((a.data[this.state.sortValue]))>this.convertToNumber((b.data[this.state.sortValue])) ? -1 : 1;
                                                    }
                                                }
                                            }).map(p => {
                                                // this.setAverage(p.data.overallImpressions, p.data.IR, p.data.ER, p.data.CTR);
                                                return <tr>
                                                    <td>{p.metaData.name}</td>
                                                    <td>{p.metaData.campaign !== null && p.metaData.campaign.name}</td>
                                                    <td>{
                                                        typeof p.data.overallImpressions === "number" ?
                                                            formatNumber(p.data.overallImpressions) :
                                                            p.data.overallImpressions
                                                    }</td>
                                                    <td>{p.data.CTR}</td>
                                                    {/*<td>{p.data.ER}</td>*/}
                                                    <td>{p.data.IR}</td>
                                                    {/*<td>{p.data.videoTags.length > 0 ? "true" : "false"}</td>*/}
                                                    {/*<td>{p.data.carouselTags.length > 0 ? "true" : "false"}</td>*/}
                                                    {/*<td>{p.metaData.site}</td>*/}
                                                    <td>{p.data.videoTags.length > 0 ? <button onClick={()=>this.setState({selectedID: p.metaData.id})}>Video Analytics</button> : ""}</td>
                                                </tr>
                                            })
                                        }
                                        </tbody>
                                    </table>
                            }

                        </div>
                    </div>
                </div>

                {
                    this.state.selectedID === null ? "" : this.state.projectOverall.filter((p) => {
                        if(p.metaData.id === this.state.selectedID){
                            p.data.videoAnalyitcs = {
                                play_100: 0,
                                play_75: 0,
                                play_50: 0,
                                play_25: 0,
                                play_start: 0,
                                play_startAudio: 0,
                                play_replay: 0,
                            };

                            let videoAnalyitics = p.data.videoTags.filter((vt)=>{
                                if(vt.name.toLowerCase().indexOf('autoplay') === -1){
                                    return vt
                                }
                            });

                            let clickAnalyitics = p.data.interactionTags.filter((vt)=>{
                                if(vt.name.toLowerCase().indexOf('play') !== -1){
                                    return vt
                                }
                            });


                            videoAnalyitics.map(va => {
                                console.group(va.name);
                                // Set 100%;
                                if(va.name.toLowerCase().indexOf("100") !== -1){
                                    p.data.videoAnalyitcs.play_100 += va.total;
                                }

                                // Set 75%;
                                else if(va.name.toLowerCase().indexOf("75") !== -1){
                                    p.data.videoAnalyitcs.play_75 += va.total;
                                }
                                else if(va.name.toLowerCase().indexOf("3qt") !== -1){
                                    p.data.videoAnalyitcs.play_75 += va.total;
                                }

                                // Set 50%;
                                else if(va.name.toLowerCase().indexOf("50") !== -1){
                                    p.data.videoAnalyitcs.play_50 += va.total;
                                }
                                else if(va.name.toLowerCase().indexOf("2qt") !== -1){
                                    p.data.videoAnalyitcs.play_50 += va.total;
                                }
                                else if(va.name.toLowerCase().indexOf("half") !== -1){
                                    p.data.videoAnalyitcs.play_50 += va.total;
                                }

                                // Set 25%;
                                else if(va.name.toLowerCase().indexOf("25") !== -1){
                                    p.data.videoAnalyitcs.play_25 += va.total;
                                }
                                else if(va.name.toLowerCase().indexOf("1qt") !== -1){
                                    p.data.videoAnalyitcs.play_25 += va.total;
                                }
                                else if(va.name.toLowerCase().indexOf("qt") !== -1){
                                    p.data.videoAnalyitcs.play_25 += va.total;
                                }

                                // Set replay;
                                else if(va.name.toLowerCase().indexOf("replay") !== -1){
                                    p.data.videoAnalyitcs.play_replay += va.total;
                                }

                                // Set audio;
                                else if(va.name.toLowerCase().indexOf("audio") !== -1){
                                    p.data.videoAnalyitcs.play_startAudio += va.total;
                                }

                                // Set play;
                                else if(va.name.toLowerCase().indexOf("0") !== -1){
                                    p.data.videoAnalyitcs.play_start += va.total;
                                }
                                else if(va.name.toLowerCase().indexOf("play") !== -1){
                                    p.data.videoAnalyitcs.play_start += va.total;
                                }
                                else if(va.name.toLowerCase().indexOf("start") !== -1){
                                    p.data.videoAnalyitcs.play_start += va.total;
                                }


                                //100% clean up
                                else if(va.name.toLowerCase().indexOf("complete") !== -1){
                                    p.data.videoAnalyitcs.play_100 += va.total;
                                }
                                console.log(p.data.videoAnalyitcs);
                                console.groupEnd();
                            });

                            clickAnalyitics.map(va => {
                                console.group(va.name);

                                // Set replay;
                                if(va.name.toLowerCase().indexOf("replay") !== -1){
                                    p.data.videoAnalyitcs.play_replay += va.total;
                                }
                                else if(va.name.toLowerCase().indexOf("play") !== -1){
                                    p.data.videoAnalyitcs.play_start += va.total;
                                }
                                else if(va.name.toLowerCase().indexOf("start") !== -1){
                                    p.data.videoAnalyitcs.play_start += va.total;
                                }
                                console.log(p.data.videoAnalyitcs);
                                console.groupEnd();
                            });

                            console.log(videoAnalyitics);


                            console.log(p);
                            return p
                        }
                    }).map(p=>{
                        return <div className={"modal"}>
                            <div className={"modal-inner"}>
                            <button className={"close"} onClick={()=>this.setState({selectedID: null})}>
                                <Icon src='close'/>
                            </button>
                            <h1 className={"text-align-center"}>{p.metaData.name}</h1>
                            <table>
                                <thead>
                                    <td></td>
                                    <td>100%</td>
                                    <td>75%</td>
                                    <td>50%</td>
                                    <td>25%</td>
                                    <td>Started</td>
                                    <td>Play Audio</td>
                                    <td>Replay</td>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Impressions</td>
                                        <td>{(p.data.videoAnalyitcs.play_100 / p.data.overallImpressions).toFixed(2)}%</td>
                                        <td>{(p.data.videoAnalyitcs.play_75 / p.data.overallImpressions).toFixed(2)}%</td>
                                        <td>{(p.data.videoAnalyitcs.play_50 / p.data.overallImpressions).toFixed(2)}%</td>
                                        <td>{(p.data.videoAnalyitcs.play_25 / p.data.overallImpressions).toFixed(2)}%</td>
                                        {
                                            p.data.videoAnalyitcs.play_start > 0 ? <td>{(p.data.videoAnalyitcs.play_start / p.data.overallImpressions).toFixed(2)}%</td> : <td>NaN</td>
                                        }
                                        {
                                            p.data.videoAnalyitcs.play_startAudio > 0 ? <td>{(p.data.videoAnalyitcs.play_startAudio / p.data.overallImpressions).toFixed(2)}%</td> : <td>NaN</td>
                                        }
                                        {
                                            p.data.videoAnalyitcs.play_replay > 0 ? <td>{(p.data.videoAnalyitcs.play_replay / p.data.overallImpressions).toFixed(2)}%</td> : <td>NaN</td>
                                        }
                                    </tr>
                                    {
                                        p.data.videoAnalyitcs.play_start > 0 ? <tr>
                                            <td>Started</td>
                                            <td>{(p.data.videoAnalyitcs.play_100/ p.data.videoAnalyitcs.play_start).toFixed(2)}%</td>
                                            <td>{(p.data.videoAnalyitcs.play_75/ p.data.videoAnalyitcs.play_start).toFixed(2)}%</td>
                                            <td>{(p.data.videoAnalyitcs.play_50/ p.data.videoAnalyitcs.play_start).toFixed(2)}%</td>
                                            <td>{(p.data.videoAnalyitcs.play_25/ p.data.videoAnalyitcs.play_start).toFixed(2)}%</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr> : ""
                                    }
                                </tbody>
                            </table>
                            </div>
                        </div>
                    })
                }
            </section>
        );
    }
}