import React from 'react';
import { Button } from  './Buttons';
import { Filter } from  './Filters';
import { TagGenerator } from  './Generators';
import { Loader } from  './Loader';
import { GetData } from './Helpers';

/*
    PAGE HEADER
    Returns meta data and header info for page
*/
export class MetaHeader extends React.Component {
    /*constructor( props ){
        super( props );
    }*/

    handleFilter() {
        console.log( 'show filter' );
        document.getElementsByClassName( 'filterWrap' )[0].classList.toggle( "show" );
        document.getElementsByClassName( 'headerSlantedEdge' )[0].classList.add( "filterOpen" );

        //close tags if necessary
        if( typeof document.getElementsByClassName( 'tagGeneratorWrap' )[0] !== "undefined" ){
            document.getElementsByClassName( 'tagGeneratorWrap' )[0].classList.remove( "show" );
        }
    }
    buildTags() {
        console.log( 'open tag builder' );
        document.getElementsByClassName( 'tagGeneratorWrap' )[0].classList.toggle( "show" );
        document.getElementsByClassName( 'headerSlantedEdge' )[0].classList.add( "filterOpen" );

        //close filter if necessary
        if( typeof document.getElementsByClassName( 'filterWrap' )[0] !== "undefined" ){
            document.getElementsByClassName( 'filterWrap' )[0].classList.remove( "show" );
        }
    }

    render() {

        //start date. add 4 hours to get to EST
        let startDate = new Date( this.props.start );
            startDate = new Date( startDate.setHours( startDate.getHours() + 4 ) ).toDateString();

        let endDate = null;
        if( this.props.end !== null ){
            endDate = new Date( this.props.end );
            endDate = new Date( endDate.setHours( endDate.getHours() + 4 ) ).toDateString();
        }

        let pinState = "";
        if( this.props.pinStatus === true ){
            pinState = "active";
        }

        const userGroup = GetData( 'userGroup', 'session' );

        return (
            <div className="pageMetaHeader">
                <div className="flex vertical-center">
                    { typeof this.props.handlePin !== 'undefined' &&
                        <div className="pinItem">
                            <Button
                                classes={`onDark iconOnly margin-right-medium fillIcon ${ pinState }`}
                                icon="heart"
                                onClick={ this.props.handlePin.bind( this ) }
                            />
                        </div>
                    }
                    { this.props.showActions === 'true' &&
                        <div className="metaData created">
                            <p className="tiny">Created: { startDate }</p>
                        </div>
                    }
                    { this.props.showActions === 'true' &&
                        <div className="flex-align-right actions flex flex-column">
                            { this.props.hideFilter !== true &&
                                <Button
                                    classes="secondary onDark hasIcon filterTags"
                                    icon="filter"
                                    text="Filter"
                                    onClick={ this.handleFilter }
                                />
                            }
                            <Button
                                classes="secondary onDark hasIcon"
                                icon="download"
                                text="Download Data"
                                onClick={ this.props.handleDownload }
                            />
                            { userGroup !== "viewer" &&
                                <Button
                                    classes="secondary onDark hasIcon buildTags"
                                    icon="pencil"
                                    text="Create Tags"
                                    onClick={ this.buildTags }
                                />
                            }

                        </div>
                    }
                    { this.props.noData === 'true' &&
                        <div className="flex-align-right actions">
                            <Button
                                classes="secondary onDark hasIcon buildTags"
                                icon="pencil"
                                text="Create Tags"
                                onClick={ this.buildTags }
                            />
                        </div>
                    }
                </div>
                <div className="pageTitle text-align-center margin-top-medium">
                    { this.props.showActions === 'true' &&
                        <div>
                            <p className="tiny color-onDarkBackground capitalize">{ this.props.type }</p>
                            <h3 className="sectionTitle color-white">
                                { this.props.name }
                                { GetData( 'userGroup', 'session' ) !== "viewer" &&
                                    <Button
                                        classes="onDark iconOnly margin-left-small"
                                        icon="pencil"
                                        onClick={ this.props.handleEdit }
                                    />
                                }
                            </h3>
                            <div className="dates">
                                { endDate !== null &&
                                    <p className="tiny">( { startDate } - { endDate } )</p>
                                }
                            </div>
                        </div>
                    }
                    { this.props.showActions === 'false' &&
                        <div>
                            <Loader />
                            <h3 className="sectionTitle color-white">{ this.props.name }</h3>
                        </div>
                    }
                    { typeof this.props.showActions === "undefined" &&
                        <div>
                            <h3 className="sectionTitle color-white">
                                { this.props.name }
                                { GetData( 'userGroup', 'session' ) !== "viewer" &&
                                    <Button
                                        classes="onDark iconOnly margin-left-small"
                                        icon="pencil"
                                        onClick={ this.props.handleEdit }
                                    />
                                }
                            </h3>
                        </div>
                    }
                </div>

                { this.props.showActions === 'true' &&
                    <Filter startDate={ startDate } endDate={ endDate } type={ this.props.type }  />
                }
                { typeof this.props.projectId !== "undefined" &&
                    <TagGenerator projectId={ this.props.projectId } operativeId={ this.props.operativeId } />
                }
            </div>
        );
    }
}
