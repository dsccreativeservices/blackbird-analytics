import React from 'react';
import { Button } from './Buttons';
import {CardTitle} from "./Typography";
import {Dropdown, Form, FormField} from "./Inputs";
import { Global } from '../json/global';
import {GetData} from "./Helpers";

export class IssueTracker extends React.Component {
    constructor( props ){
        super( props );

        this.state = {
            open: null,
            options: [
                {id: "bug", text: "Bug"},
                {id: "feature", text: "Feature"},
            ],
            data: {
                title: "",
                description: ""
            },
            issueType: "bug",
        };

        this.createIssue = this.createIssue.bind( this );
        this.saveToIssueTracker = this.saveToIssueTracker.bind( this );
        this.setIssueType = this.setIssueType.bind(this);
    }

    componentDidMount(){
        document.getElementById("issueTrackerClick").addEventListener("click", function () {
            document.querySelector(".issueTrackerContainer").classList.add("active")
        });
    }

    setIssueType(e){
        console.log(e.target.value);
        this.setState({
            issueType: e.target.value
        })
    }

    createIssue(){
        let apiKey = "f85c5e434d8709e1f11d146fb8ffd315&token=322d6ffea0ffbe61758aa0e0908776c84269dc7960582fd2aa968fa2442595c4",
            token = "",
            desc = document.getElementById("issueDescription").value,
            userID,
            title = document.getElementById("issueTitle").value,
            type = this.state.issueType,
            listID = "5c05a87258a1d279d4b87a8b",
            labels = "5c068b7b65b7925246e20e72";

        if(type === "feature"){
            listID = "5c05a87258a1d279d4b87a8b";
            labels = "5c068b7b65b7925246e20e72,5c069aaedc27077476c5f967"
        }
        else {
            listID = "5c05a8709901902416012e06"
        }

        const userId = GetData( 'userId', 'session' );

        fetch( `${ Global.apiEndpoint }/user/${ userId }`, {
            headers: {
                'apiToken': GetData( 'apiKey', 'session' )
            }
        })
            .then( ( response ) => {
                return response.text();
            } )
            .then( ( data ) => {
                console.log( data );
                var dataObj = JSON.parse( data );
                desc =  desc + ` \n (USER: ${dataObj.user.name} CURRENT PAGE: ${window.location.href})`;
                this.saveToIssueTracker(dataObj.user, apiKey, listID, desc, title, labels);
                document.querySelector(".issueTrackerContainer").classList.remove("active");
                this.setState({
                    data: {
                        title: "",
                        description: ""
                    },
                })
            } );


    }

    saveToIssueTracker(userData, apiKey, listID, desc, title, labels){
        let url = `https://api.trello.com/1/cards/?key=${apiKey}&idList=${listID}&desc=${desc}&pos=top&name=${title}&idLabels=${labels}`;

        console.log({
           "Title": title,
           "Description": desc
        });


        fetch(url, {
            method: 'POST', // or 'PUT'
        }).then(res => res.json())
            .then(response => {
                this.saveToUserIssues(userData, response.id);
                console.log('Success:', response.id)
            })
            .catch(error => console.error('Error:', error));
    }


    saveToUserIssues(userData, issueID){
        const userId = GetData( 'userId', 'session' );
        let data = new FormData();
        let issues;

        if(userData.issues){
            issues = JSON.parse(userData.issues);
        }
        else {
            issues = [];
        }

        issues.push(issueID);

        console.log(issues);

        data.append( 'issues', JSON.stringify(issues));


        fetch( `${ Global.apiEndpoint }/user/${userId}/update`, {
            headers: {
                'apiToken': GetData( 'apiKey', 'session' )
            },
            method: "POST",
            body: data
        })
            .then( ( response ) => {
                return response.text();
            } )
            .then( ( data ) => {
                console.log( data );
            } );
    }

    close(){
        document.querySelector(".issueTrackerContainer").classList.remove("active");
        this.setState({
            data: {
                title: "",
                description: ""
            },
        })
    }

    render() {
        return (
            <div className={"issueTrackerContainer"}>
               <div className="issueTrackerBox">
                   <CardTitle>Report Issue</CardTitle>
                   <Form>
                       <FormField
                           wrapClass="full-width"
                           inputClass="outlineInput moveLabelOnFocus forceLabelAbove bold color-blue"
                           name="issueTitle"
                           type="text"
                           label="Title"
                           value={ this.state.data.title }
                       />
                       <FormField
                           wrapClass="full-width"
                           inputClass="outlineInput moveLabelOnFocus forceLabelAbove bold color-blue"
                           name="issueDescription"
                           type="text"
                           label="description"
                           value={ this.state.data.description }
                       />
                       <Dropdown
                           wrapClass="flex-grow margin-right-medium"
                           inputClass="bold"
                           name="advertiser"
                           label="Advertiser"
                           options={ this.state.options }
                           placeholder={"Bug"}
                           onChange={(e) => this.setIssueType(e)}
                       />
                   </Form>


                   <div className="buttonContainer">
                       <Button
                           classes="primary"
                           text="Submit"
                           onClick={ (evt) => this.createIssue(evt) }
                       />
                       <Button
                           classes="primary color-red"
                           text="Cancel"
                           onClick={ (evt) => this.close(evt) }
                       />
                   </div>
               </div>
            </div>
        );
    }

}