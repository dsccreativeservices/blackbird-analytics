import React from 'react';
import * as moment from 'moment';
import DayPicker, { DateUtils } from 'react-day-picker';
import { Button } from './Buttons';
import {FormatDate, FormatDateToPHPFormat} from './Helpers';
import 'react-day-picker/lib/style.css';


/*
    CURRENT YEAR
*/
export class Year extends React.Component {
    /*constructor( props ){
        super( props );
    }*/
    render() {

        const date = new Date();
        const year = date.getFullYear();

        return (
            <span className="currentYear">{ year }</span>
        );
    }
}


/*
    DATE PICKER
*/
export class DatePicker extends React.Component {

    constructor( props ){
        super( props );
        this.state = {
            from: null,
            to: null
        };
        this.setDates = this.setDates.bind( this );
    }

    setDates(){
        console.log( "set dates" );
        const from = FormatDateToPHPFormat( this.state.from );
        const to = FormatDateToPHPFormat( this.state.to );

        document.getElementById( 'startDate' ).value = from;
        document.getElementById( 'endDate' ).value = to;

        //pass up to parent
        this.props.updateData( from, to );

        //this produces an error but still works, not sure how to do it any other way.
        this.setState( {
            from: null,
            to: null
        } );
    }

    handleDayClick = day => {
       const range = DateUtils.addDayToRange( day, this.state );
       this.setState( range );
    };

    handleResetClick = e => {
        e.preventDefault();
        this.setState( {
          from: null,
          to: null
        } );
    };

    closeOverlay(){
        document.getElementById( 'datepicker' ).classList.remove( "active" );
    }

    render() {

        const { from, to } = this.state;
        const lastDay = new Date( this.props.end );
        const firstDay = new Date( this.props.start );

        if( from !== null && to !== null ){
            this.closeOverlay();
            this.setDates();
        }

        return (
            <div className="DatePicker">
                {!from && !to &&
                    <p className="text-align-center">Please select the <strong>first day</strong>.</p>
                }
                {from && !to &&
                    <p className="text-align-center">Please select the <strong>last day</strong>.</p>
                }
                {from &&
                    to &&
                        <p className="text-align-center">
                        You chose from
                        <span className="padding-x-small">
                        { moment( from ).format( 'MM-DD-YYYY' ) }
                        </span>
                        to
                        <span className="padding-x-small">
                        { moment( to ).format( 'MM-DD-YYYY' ) }
                        </span>
                        <Button
                            classes="small resetDates"
                            text="Reset Dates"
                            onClick={ this.handleResetClick }
                        />
                    </p>
                }
                <DayPicker
                    initialMonth={ firstDay }
                    fromMonth={ firstDay }
                    toMonth={ lastDay }
                    numberOfMonths={2}
                    selectedDays={[from, { from, to }]}
                    onDayClick={ this.handleDayClick }
                    fixedWeeks
                    disabledDays={{ after: lastDay, before: firstDay }}
                />
            </div>
        );

    }
}
