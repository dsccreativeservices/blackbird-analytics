import React from 'react';
import { FetchSelect as Select } from 'react-select3';
import '../css/libraries/select3/styles.css';

export class Form extends React.Component {
    render(){

        let classList = "formWrapper";
        if( typeof this.props.classes !== "undefined" ){
            classList += " " + this.props.classes;
        }

        return (
            <form className={ classList }>
                { this.props.children }
            </form>
        );
    }
}


/*
    CREATE INPUT ELEMENT
*/
export class FormField extends React.Component {
    constructor( props ){
        super( props );
        this.state ={
            value: props.value,
        }
    }

    componentWillReceiveProps(nextProps) {
        this.setState({ value: nextProps.value });
    }

    handleChange(e){
        this.setState({
            value: e.target.value
        })
    }

    render() {
        return (
            <div className={ "form-item " + this.props.wrapClass }
                data-content={ this.props.name }
                data-count={ this.props.count }
            >
                <div className="form-input">
                    <input
                        id={ this.props.name }
                        className={ this.props.inputClass }
                        name={ this.props.name }
                        type={ this.props.type }
                        value={ this.state.value }
                        placeholder={ this.props.placeholder }
                        min={ this.props.min }
                        max={ this.props.max }
                        onFocus={ this.props.onFocus }
                        onBlur={ this.props.onBlur }
                        onChange={(evt) => this.handleChange(evt)}
                        required={ this.props.required }
                        disabled={ this.props.disabled }
                        autoComplete={this.props.autoComplete}
                    />
                    <label>{ this.props.label }</label>
                    { this.props.count &&
                        <div className="count number">{ this.props.count }</div>
                    }
                    { this.props.instructions &&
                        <p className="instruct">{ this.props.instructions }</p>
                    }
                    <p className="inputMessage"></p>
                </div>
            </div>

        );
    }
}



/*
    CREATE DROPDOWN
*/
export class Dropdown extends React.Component {

    _selectRef = ( node ) => {
        this.select = node;
    };

    _resetSelect = () => {
        this.select.clear();
    };


    render() {

        //function that formats fetched objects into options
        //object containing id and text
        const responseDataFormatter = item => ( {
            //required keys
            id: 'item_' + item.id,
            text: item.name
        } );

        let dropdown = null;

        //use fetch to load data by specifying api endpoint
        if( typeof this.props.api !== 'undefined' ){
            dropdown = <Select
                ref={ this._selectRef }
                fetch={{
                    endpoint: this.props.api,
                    termQuery: 'filter',
                    responseDataFormatter,
                    once: true
                }}
                placeholder={ this.props.placeholder }
                search={{
                    minimumResults: 10
                }}
                onSelect={ this.props.onChange }
                disabled={ this.props.disabled }
            />;
        }

        //use currated options by specifying options array of objects
        else{
            dropdown = <Select
                ref={ this._selectRef }
                options={ this.props.options }
                value={ this.props.value }
                defaultValue={ this.props.defaultValue }
                placeholder={ this.props.placeholder }
                search={{
                    minimumResults: 10
                }}
                onSelect={ this.props.onChange }
                disabled={ this.props.disabled }
            />;
        }


        return (
            <div
                className={ "form-item " + this.props.wrapClass }
                data-content={ this.props.name }
            >
                <div className={ "form-input dropdown " + this.props.inputClass }>
                    <label>{ this.props.label }</label>

                    { dropdown }

                    { this.props.showReset &&
                        <span className="clearSelect color-light uppercase" onClick={ this._resetSelect }>Clear</span>
                    }

                    { this.props.instructions &&
                        <p className="instruct">{ this.props.instructions }</p>
                    }
                    <p className="inputMessage"></p>
                </div>
            </div>
        );
    }
}
