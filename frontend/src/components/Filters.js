import React from 'react';
import { Icon } from './Icons';
import { Button } from './Buttons';
import { FormField } from './Inputs';
import { FormatDate } from './Helpers';

/*
    GRAPH BUILDER
    Gets data and returns a graph
*/
export class Filter extends React.Component {
    /*constructor( props ){
        super( props );
        this.passDateRangeUpToParent = this.passDateRangeUpToParent.bind( this );
    }*/

    selectDate(){
        console.log( "show datepicker" );
        document.getElementById( 'datepicker' ).classList.toggle( "active" );
    }

    /*
        Close the Filter
    */
    closeFilter(){
        document.getElementsByClassName( 'filterWrap' )[0].classList.toggle( "show" );
        setTimeout( function(){
            document.getElementsByClassName( 'headerSlantedEdge' )[0].classList.remove( "filterOpen" );
        }, 600 );

    }

    /*
        Reset date range by reloading the page
    */
    resetDateRange(){
        window.location.reload();
    }

    toggleFilterItem( event ){
        console.log("Toggle Filter Item");
        const btn = event.target,
              type = event.target.getAttribute( "data-function" ),
              dataSection = document.querySelectorAll( '[data-type="' + type + '"]' )[0];

        console.log( type, dataSection );

        //toggle button
        btn.classList.toggle( "active" );

        //toggle section if it exists
        if( typeof dataSection !== "undefined" ){
            dataSection.classList.toggle( "hide" );
        }

    }

    render() {

        return (
            <div className="metaHeader-expandable filterWrap">
                <Button
                    classes="filterBtn secondary onDark hasIcon active"
                    icon="filter"
                    text="Filter"
                    onClick={ this.closeFilter }
                />
                <DateRange>
                    <FormField
                        wrapClass="half-width"
                        inputClass="outlineInput moveLabelOnFocus bold color-blue text-align-center"
                        name="startDate"
                        type="text"
                        //count="null"
                        value={ FormatDate( this.props.startDate ) }
                        label="Start Date"
                        //instructions="test instructions"
                        //min="1"
                        //max=""
                        onFocus={ this.selectDate }
                    />
                    <FormField
                        wrapClass="half-width"
                        inputClass="outlineInput moveLabelOnFocus bold color-blue text-align-center"
                        name="endDate"
                        type="text"
                        //count="null"
                        value={ FormatDate( this.props.endDate ) }
                        label="End Date"
                        //instructions="test instructions"
                        //min="1"
                        //max=""
                        onFocus={ this.selectDate }
                    />
                    <Button
                        classes="small resetData margin-left-large"
                        text="Reset Dates"
                        onClick={ this.resetDateRange }
                    />
                </DateRange>
                { this.props.type === 'project' &&
                    <ReportItems>
                        <Button
                            classes="reportItem active"
                            text="Clicks"
                            role="clicks"
                            onClick={ this.toggleFilterItem }
                        />
                        <Button
                            classes="reportItem active"
                            text="Interactions"
                            role="interactions"
                            onClick={ this.toggleFilterItem }
                        />
                        <Button
                            classes="reportItem active"
                            text="Engaged Time"
                            role="time"
                            onClick={ this.toggleFilterItem }
                        />
                        <Button
                            classes="reportItem active"
                            text="Carousel Views"
                            role="carousel"
                            onClick={ this.toggleFilterItem }
                        />
                        <Button
                            classes="reportItem active"
                            text="Video Quartiles"
                            role="video"
                            onClick={ this.toggleFilterItem }
                        />
                    </ReportItems>
                }
            </div>
        );
    }
}

class DateRange extends React.Component {

    render() {
        return (
            <div className="filterDateRange">
                <div className="filterTitle">
                    <Icon src="calendar" />
                    <label className="largeLabel">Date Range</label>
                </div>
                <div className="flex margin-top-medium">
                    { this.props.children }
                </div>
            </div>
        );
    }
}

class ReportItems extends React.Component {

    render() {
        return (
            <div className="reportItems margin-top-large">
                <div className="filterTitle">
                    <Icon src="suitcase" />
                    <label className="largeLabel">Show In Report</label>
                    <p className="margin-top-small tiny color-dark">Click an item to show/hide in the report below</p>
                </div>
                <div className="flex flex-wrap justify-between margin-top-medium">
                    { this.props.children }
                </div>
            </div>
        );
    }
}

/*
class ReportItem extends React.Component {

    render() {
        return (
            <div className="reportItem bold" data-type={ this.props.type }>
                { this.props.type }
            </div>
        );
    }
}
*/
