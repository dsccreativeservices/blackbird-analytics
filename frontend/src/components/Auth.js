import React, { useEffect } from 'react';
import { Route, Redirect } from 'react-router-dom';
import { LoginCallback, useOktaAuth } from '@okta/okta-react';
import { Global } from '../json/global';
import { PageTitle } from './Typography';
import { Icon } from './Icons';
import { GetData, StoreData } from './Helpers';
import { Form, FormField } from './Inputs';
import { Button } from './Buttons';

/*
    Login Page
*/
// export class Login extends React.Component {
//     state = {
//         redirectToReferrer: false
//     };

//     login = ( e ) => {
//         e.preventDefault();
//         console.log(e);
//         authSwitch.authenticate( () => {
//             this.setState( { redirectToReferrer: true } );
//         } );
//     };

//     /*
//         called when component first loads, not called for changes
//     */
//     componentDidMount() {
//         if( authSwitch.isAuthenticated === true ){
//             this.setState( { redirectToReferrer: true } );
//         }
//     }

//     render() {

//         console.log( this.props.location.state );

//         let { from } = this.props.location.state || { from: { pathname: '/' } };
//         if( typeof this.props.location.state === "undefined" ){
//             from = "/dashboard";
//         }
//         const { redirectToReferrer } = this.state;

//         if ( redirectToReferrer ) {
//             return (
//                 <Redirect to={from}/>
//             );
//         }

//         return (
//             <section className="loginPage">

//                 <header className="headerSlantedEdge background-backgroundDark color-onDarkBackground padding-y-xlarge text-align-center">
//                     <Icon src='lock' />
//                     <PageTitle classes="color-white">Login</PageTitle>
//                 </header>

//                 <div className="mainContent">
//                     <div className="innerWrap small">
//                         <div className="innerWrap-inner background-background padding-x-xlarge padding-y-xlarge">

//                             <div className="sectionIntro">
//                                 <h3 className="sectionTitle">You must login to view this page.</h3>
//                                 <h4 className="cardTitle error noData">Could Not Log In</h4>

//                                 <Form>
//                                     <div className="form-row flex margin-top-large">
//                                         <FormField
//                                             wrapClass="flex-grow"
//                                             inputClass="outlineInput moveLabelOnFocus bold color-blue"
//                                             name="email"
//                                             type="email"
//                                             label="Email Address"
//                                             //placeholder="Enter Advertiser Name"
//                                             required="true"
//                                             //instructions="Enter a name for the new advertiser"
//                                         />
//                                         <FormField
//                                             wrapClass="flex-grow margin-left-large"
//                                             inputClass="outlineInput moveLabelOnFocus bold color-blue"
//                                             name="password"
//                                             type="password"
//                                             label="Password"
//                                             //placeholder="Enter Advertiser Name"
//                                             required="true"
//                                             //instructions="Enter a name for the new advertiser"
//                                         />
//                                     </div>
//                                     <FormField
//                                         wrapClass="flex-grow margin-left-large checkbox"
//                                         inputClass="outlineInput moveLabelOnFocus bold color-blue"
//                                         name="rememberMe"
//                                         type="checkbox"
//                                         label="Remember Me"
//                                     />
//                                     <div className="form-actions flex justify-end margin-top-xlarge">
//                                         <Button
//                                             classes="primary"
//                                             icon="pencil"
//                                             text="Log In"
//                                             onClick={ this.login }
//                                         />
//                                     </div>
//                                     {/*<div className="form-actions flex justify-end">*/}
//                                         {/*<FormField*/}
//                                             {/*wrapClass="flex-grow margin-left-large"*/}
//                                             {/*inputClass="outlineInput moveLabelOnFocus bold color-blue"*/}
//                                             {/*name="loggedIN"*/}
//                                             {/*type="checkbox"*/}
//                                             {/*label="Keep Me Logged In"*/}
//                                             {/*//placeholder="Enter Advertiser Name"*/}
//                                             {/*required="false"*/}
//                                             {/*//instructions="Enter a name for the new advertiser"*/}
//                                         {/*/>*/}
//                                     {/*</div>*/}
//                                 </Form>

//                             </div>

//                         </div>
//                     </div>
//                 </div>

//             </section>

//         );
//     }
// }

const Login = () => {
  const data = useOktaAuth();

  useEffect(() => {
    console.log(data);

    async function fetchUser() {
      const user = await data.oktaAuth.token.getUserInfo();
      console.log(user);
    }
    fetchUser();
  }, [data]);

  return (
    <LoginCallback />
  );
};

const Logout = () => {
  const { oktaAuth } = useOktaAuth();

  console.log('typeof oktaAuth', typeof oktaAuth);

  useEffect(() => {
    oktaAuth.signOut({
      postLogoutRedirectUri: '/',
    });
  }, [oktaAuth]);

  return (
    <p>Redirecting...</p>
  );
};

function authStatus() {
  const authStatus = GetData('authStatus', 'session');
  if (authStatus === true) {
    console.log('logged in');
    // set user group
    document.getElementById('root').setAttribute('data-usergroup', GetData('userGroup', 'session'));
    return true;
  }
  console.log('not logged in');
  return false;
}

/*
    Authentication manager
*/
const authSwitch = {
  showErrorMessage() {
    if (document.querySelector('.cardTitle.error.noData')) {
      document.querySelector('.cardTitle.error.noData').classList.remove('noData');
    }
  },

  isAuthenticated: authStatus(),

  authenticate(callback) {
    const email = document.getElementById('email').value;
    if (email === '') {
      document.querySelector('.form-item[data-content="email"] .inputMessage').innerHTML = 'Please enter your email address.';
      document.querySelector('.cardTitle.error').innerHTML = 'Please enter your email address.';
      this.showErrorMessage();
      return false;
    }
    document.querySelector('.form-item[data-content="email"] .inputMessage').innerHTML = '';

    const password = document.getElementById('password').value;
    if (password === '') {
      document.querySelector('.cardTitle.error').innerHTML = 'Please enter your email password.';
      document.querySelector('.form-item[data-content="password"] .inputMessage').innerHTML = 'Please enter your password.';
      this.showErrorMessage();
      return false;
    }
    document.querySelector('.form-item[data-content="password"] .inputMessage').innerHTML = '';

    const rememeberMe = document.getElementById('rememberMe').checked;

    const credentials = new FormData();
    credentials.append('email', email);
    credentials.append('password', password);

    fetch(`${Global.apiEndpoint}/login`, {
      method: 'POST',
      body: credentials,
    })
      .then((response) => response.text())
      .then((data) => {
        data = JSON.parse(data);
        console.log(data);
        if (data.status === 'success') {
          console.log(rememeberMe);
          if (rememeberMe) {
            localStorage.rememberMe = 'local';
          } else {
            localStorage.rememberMe = 'session';
          }
          StoreData('authStatus', true, 'session');
          StoreData('userId', data.user, 'session');
          StoreData('apiKey', data.apiKey, 'session');
          StoreData('userGroup', data.userGroup, 'session');
          this.isAuthenticated = true;
          document.getElementById('root').setAttribute('data-usergroup', data.userGroup);
          callback(data);
        }
        if (data.status === 'failed') {
          document.querySelector('.cardTitle.error').innerHTML = 'Could Not Log In';
          this.showErrorMessage();
        }
      });
  },

  signout(callback) {
    this.isAuthenticated = false;
    StoreData('authStatus', false, 'session');
    StoreData('userId', null, 'session');
    StoreData('apiKey', null, 'session');
    StoreData('userGroup', null, 'session');
  },
};

export const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={(props) => (
      authSwitch.isAuthenticated ? (
        <Component {...props} />
      ) : (
        <Redirect to={{
          pathname: '/home',
          state: { from: props.location },
        }}
        />
      )
    )}
  />
);

export { Login, Logout };
