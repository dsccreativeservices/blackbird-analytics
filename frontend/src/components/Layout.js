import React from 'react';

/*
    CARD LAYOUT
*/
export class Card extends React.Component {
    render(){
        let classes = "layout-card";
        if( this.props.classes ){
            classes += " " + this.props.classes;
        }

        return (
            <div id={ this.props.id } className={ classes }>
                <CardTitle>{ this.props.title }</CardTitle>
                <CardContent>
                    { this.props.children }
                </CardContent>
            </div>
        );
    }
}

export class CardTitle extends React.Component {
    render(){
        let classes = "cardHeadline";
        if( this.props.classes ){
            classes += " " + this.props.classes;
        }

        return (
            <div className={ classes }>
                <h4 className="cardTitle">{ this.props.children }</h4>
            </div>
        );
    }
}

export class CardContent extends React.Component {
    render(){
        let classes = "cardContent padding-xlarge background-lightest";
        if( this.props.classes ){
            classes += " " + this.props.classes;
        }

        return (
            <div className={ classes }>
                { this.props.children }
            </div>
        );
    }
}
