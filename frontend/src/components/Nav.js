import React from 'react';
import { Link } from 'react-router-dom';
import { useOktaAuth } from '@okta/okta-react';
import { MenuData, FooterMenuData } from '../json/menu';
import { Icon } from './Icons';
import { GetData } from './Helpers';

export function GlobalNav() {
  const { authState } = useOktaAuth();
  const makeClasses = (...classes) => classes.filter((i) => Boolean(i)).join(' ');

  return (
    <nav className="globalNav">
      <ul className="flex">
        { (authState !== 'undefined' && !authState.isAuthenticated) ? (
          <li className="menu-item flex-align-right">
            <Icon src="lock" />
            <Link to="/login">Login</Link>
          </li>
        ) : MenuData.map((item) => (
          <li
            key={item.id}
            className={
            makeClasses('menu-item',
              item.class ? item.class : '')
        }
          >
            <Icon src={item.icon} />
            <Link to={item.url}>{ item.text }</Link>
          </li>
        )) }
      </ul>
    </nav>
  );
}

export class FooterNav extends React.Component {
  /* constructor( props ){
        super( props );
    } */
  render() {
    const menuGroups = FooterMenuData.map((group) => (
      <FooterNavColumn
        key={group.category}
        title={group.category}
        data={group.items}
      />
    ));

    return (
      <nav className="footerNav margin-top-large flex justify-center">
        { menuGroups }
      </nav>
    );
  }
}

class FooterNavColumn extends React.Component {
  render() {
    const makeClasses = (...classes) => classes.filter((i) => Boolean(i)).join(' ');
    const menuJSX = this.props.data.map((item) => {
      if (item.url) {
        return (
          <li
            key={item.id}
            id={item.id}
            className={
                        makeClasses('menu-item',
                          item.class ? item.class : '', // if class exists
                        )
                    }
          >
            {
                            item.url.includes('http') ? <a href={item.url}>{item.text}</a>
                              : <Link to={item.url}>{item.text}</Link>
                        }
          </li>
        );
      }

      return (
        <li
          key={item.id}
          id={item.id}
          className={
                        makeClasses('menu-item',
                          item.class ? item.class : '', // if class exists
                        )
                    }
          onClick={item.onClick}
        >
          <a>{item.text}</a>
        </li>
      );
    });

    return (
      <div className="footerNav-column text-align-center">
        <h5 className="largeLabel footerNav-title dash-below dash-center dash-blue dash-large">{ this.props.title }</h5>
        <ul className="footerNav-list margin-top-medium vert-list-spacing-small">
          { menuJSX }
        </ul>
      </div>
    );
  }
}
