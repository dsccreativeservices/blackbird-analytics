import React from 'react';
import { Global } from '../json/global';
import { Parents } from './Parents';
import { Siblings } from './Siblings';
import { MetaHeader } from './MetaHeader';
import { Link } from 'react-router-dom';
import { Button, ButtonState } from './Buttons';
import { LargeDataLabel } from './DataLabels';
import {Dropdown, Form, FormField} from './Inputs';
import {CardTitle, SectionTitle} from './Typography';
import { SortAlphabetically, ExportData, GetData, FormatDateToPHPFormat, GetOverallImpressions } from './Helpers';
import { Overlay } from './Overlay';
import { DatePicker } from './Calendar';
import * as moment from 'moment';

/*
    GET CAMPAIGN
    For the specified campaign return a list of all the projects
*/
export class GetCampaign extends React.Component {
    constructor( props ){
        super( props );
        this.params = this.props.match.params; //gets the variables in the URL String and make a shorter way to access them

        this.state = {
            edit: false,
            metaData: null,
            projects: null
        };

        this.downloadData = this.downloadData.bind( this );
        this.updateDateRange = this.updateDateRange.bind( this );
        this.editCampaign = this.editCampaign.bind(this);
        this.updateCampaign = this.updateCampaign.bind(this);
    }


    /*
        custom function to fetch data
    */
    getProjects( id, startDate, endDate ){
        console.log( 'getting data for campaign', id, startDate, endDate );

        let dataURL = `${ Global.apiEndpoint }/campaign/${ id }`;
        if( typeof startDate !== "undefined" && typeof endDate !== "undefined" ){
            dataURL = `${ Global.apiEndpoint }/campaign/${ id }/${ startDate }/${ endDate }`;
        }

        fetch( dataURL, {
            headers: {
                'apiToken': GetData( 'apiKey', 'session' )
            }
        } )
            .then( ( response ) => {
                return response.text();
            } )
            .then( ( data ) => {
                //console.log( data );
                if( data === "no campaign" ){
                    alert( 'this campaign does not exist' );
                    return false;
                }
                var dataObj = JSON.parse( data );
                //console.log( dataObj );

                this.setState( {
                    metaData: dataObj.campaign,
                    projects: dataObj.projects
                } );

            } );
    }


    updateDateRange( from, to ){
        console.log( 'update date range', from, to );

        //convert dates to YYYY-MM-DD
        from = moment( new Date( from ) ).format( 'YYYY-MM-DD' );
        to = moment( new Date( to ) ).format( 'YYYY-MM-DD' );

        this.getProjects( this.state.metaData.id, from, to );

        //close filter
        document.getElementsByClassName( 'filterWrap' )[0].classList.toggle( "show" );
    }


    /*
        Download all data for this project
    */
    downloadData(){
        console.log( 'download data', this.state.projects );

        //add title to data
        function addTitle( array, title ){
            //blank row above
            array.push( {
                name: "",
                total: "",
                redirect: ""
            } );
            //title row
            array.push( {
                name: "title_" + title,
                total: "",
                redirect: ""
            } );
        }
        function addSubtitle( array, text ){
            //blank row above
            array.push( {
                name: "",
                total: "",
                redirect: ""
            } );
            //title row
            array.push( {
                name: "sub_" + text,
                total: "",
                redirect: ""
            } );
        }

        const viewTagsList = [
            'engagedUser',
            'overall',
            'overall-impression',
            //time spent
            'time05',
            'time10',
            'time15',
            'time20',
            'time30',
            'time1min',
            'time2min',
            'time3min',
            'time4min',
            'time5min',
            //video
            '25percent',
            '50percent',
            '75percent',
            '100percent'
        ];

        /*
            Format the tags for this project
        */
        function setupProjectTags( tags ){

            let formattedTags = [];
            for( let t of tags ){
                let obj = {
                    type: t.type,
                    name: t.name,
                    total: t.total,
                    redirect: typeof t.redirect === "undefined" ? "" : t.redirect
                };

                //modify specific tags
                if( t.name === "overall" || t.name === "engagedUser" ){
                    obj.type = 'Global Data';
                }

                //skip if total is undefined for this item and not overall or engagedUser
                else{
                    if( typeof t.total === "undefined" ){
                        console.log( t );
                        continue;
                    }
                }

                //push into array
                formattedTags.push( obj );
            }

            //global data first
            addSubtitle( finalData, "Overall Data" );
            let globalData = formattedTags.filter( ( t ) => {
                return t.type === "Global Data";
            } );
            for( let g of globalData ){
                finalData.push( {
                    name: g.name,
                    total: g.total,
                    redirect: ""
                } );
            }

            //clicks
            addSubtitle( finalData, "Clicks" );
            /*let clickData = formattedTags.filter( ( t ) => {
                return t.type === "jump";
            } )
            .sort( SortAlphabetically( 'name' ) );*/
            let clickData = formattedTags.filter( ( t ) => {
                if( t.redirect !== "" && typeof t.redirect !== "undefined" ){
                    return t;
                }
                return false;
            } )
            .sort( SortAlphabetically( 'name' ) );
            for( let c of clickData ){
                finalData.push( {
                    name: c.name,
                    total: c.total,
                    redirect: c.redirect
                } );
            }

            //temp until tag types are implemented
            let interactionTags = [];
            let viewTags = [];
            let nonClickData = formattedTags.filter( ( t ) => {
                if( typeof t.redirect === "undefined" || t.redirect === "" ){
                    return t;
                }
                return false;
            } );
            for( let d of nonClickData ){
                if( viewTagsList.indexOf( d.name ) > -1 ){
                    viewTags.push( d );
                }else{
                    interactionTags.push( d );
                }
            }

            //console.log( interactionTags, viewTags );

            //engagements
            addSubtitle( finalData, "Interactions" );
            /*let engageData = formattedTags.filter( ( t ) => {
                return t.type === "enga";
            } )
            .sort( SortAlphabetically( 'name' ) );*/
            let engageData = interactionTags.sort( SortAlphabetically( 'name' ) );
            for( let e of engageData ){
                finalData.push( {
                    name: e.name,
                    total: e.total,
                    redirect: ""
                } );
            }

            //other data
            addSubtitle( finalData, "Other Data" );
            /*let viewData = formattedTags.filter( ( t ) => {
                return t.type === "view";
            } )
            .sort( SortAlphabetically( 'name' ) );*/
            let viewData = viewTags.sort( SortAlphabetically( 'name' ) );
            for( let v of viewData ){
                finalData.push( {
                    name: v.name,
                    total: v.total,
                    redirect: ""
                } );
            }

        }

        //build data ( array of objects )
        let finalData = [];
        for( let p of this.state.projects ){

            //add title for project first
            addTitle( finalData, p.name );

            //push data for project into array
            setupProjectTags( p.tags );
        }

        //console.log( "data", finalData );

        const exportStatus = ExportData(
            finalData,
            [ 'name', 'total', 'redirect'  ],
            [
                { wch: 40 },
                { wch: 15 },
                { wch: 70 }
            ],
            `campaign_${ this.state.metaData.id }_all_data.xlsx`,
            function( data ){
                for( let d in data ){
                    let el = data[d];
                    if( typeof el.v === "string" ){
                        if( el.v.includes( 'title_' ) > 0 ){
                            el.v = el.v.replace( 'title_', '' );
                            el.s = {
                                font: {
                                    sz: 20,
                                    bold: true,
                                    color: {
                                        rgb: "FF5A6C8E"
                                    }
                                }
                            };
                        }
                        if( el.v.includes( 'sub_' ) > 0 ){
                            el.v = el.v.replace( 'sub_', '' );
                            el.s = {
                                font: {
                                    sz: 16,
                                    bold: true,
                                    color: {
                                        rgb: "FF0E0D0E"
                                    }
                                }
                            };
                        }
                    }

                }

                return data;
            }
        );
        console.log( exportStatus );
    }


    /*
        EDIT CAMPAIGN
        Edit the name or other details for the campaign
    */
    editCampaign(){
        console.log( 'edit campaign' );
        this.editOverlay.show();

            // `${ Global.apiEndpoint }/campaign/{id}/update`
    }

    updateCampaign(e){
        e.preventDefault();

        const button = e.target;
        button.classList.remove( "state-error" );
        button.classList.add( "state-loading" );

        const name = document.querySelector( '.form-item[data-content="campaign-name"] input' ).value;

        let data = new FormData();

        if( name !== this.state.metaData.name ){
            data.append( 'name', name );
        }

        fetch( `${ Global.apiEndpoint }/campaign/${ this.state.metaData.id }/update`, {
            headers: {
                'apiToken': GetData( 'apiKey', 'session' )
            },
            method: "POST",
            body: data
        } ).then( ( response ) => {
                return response.text();
            } )
            .then( ( data ) => {
                console.log( data );
                const dataObj = JSON.parse( data );

                if( dataObj.status === "success" ){
                    button.classList.remove( "state-loading" );
                    button.classList.add( "state-success" );

                    setTimeout( function(){
                        button.classList.remove( "state-success" );
                    }, 2000 );

                }

            } );
    }



    /*
        called when new params are passed to the component, like changing between projects
    */
    componentWillReceiveProps( nextProps ){
        console.log( 'receive props', nextProps.match.params.id );

        if( this.state.metaData.id !== nextProps.match.params.id ){
            this.getProjects( nextProps.match.params.id );
        }
    }


    /*
        called when component first loads, not called for changes
    */
    componentDidMount() {
        console.log( 'first' );
        this.getProjects( this.params.id );
    }


    render() {

        let projects = [];
        let name = "Loading Campaign";
        let nameAndIO = name;
        let createdAt = "";
        let currentDate = "";
        let showMetaHeaderActions = "false";

        if( this.state.metaData !== null ){

            //setup metaData
            name = this.state.metaData.name;
            nameAndIO = `${ name } (#${ this.state.metaData.operativeOrderId })`;
            createdAt = this.state.metaData.created_at;
            currentDate = FormatDateToPHPFormat( new Date() );
            showMetaHeaderActions = "true";

            //build projects
            if( this.state.projects !== null ){
                projects = this.state.projects.map( ( p ) =>
                    (
                        <Project key={ p.id } data={ p } />
                    )
                );
            }
        }


        return (
            <section className="campaignPage">
                <div className="subnav related">
                    { this.state.metaData !== null &&
                        <div className="innerWrap medium flex">
                            <Parents type="campaign" id={ this.state.metaData.id } currentItem={ name } />
                            <Siblings type="campaign" id={ this.state.metaData.id } />
                        </div>
                    }
                </div>

                <header className="headerSlantedEdge background-backgroundDark color-onDarkBackground padding-y-xlarge text-align-center">
                <div className="innerWrap medium">
                    <MetaHeader
                        name={ nameAndIO }
                        start={ createdAt }
                        end={ `${ currentDate } 12:00:00` }
                        handleDownload={ this.downloadData }
                        handleEdit={ this.editCampaign }
                        showActions={ showMetaHeaderActions }
                        //hideFilter={ true }
                        type="campaign"
                    />
                </div>
                </header>

                <div className="mainContent">
                    <div className="innerWrap medium">
                        <div className="grid spacing-y-xlarge innerWrap-inner background-background padding-x-xlarge padding-y-xlarge">

                            { projects }

                        </div>

                        <div className="annotations flex justify-center">
                            <p className="tiny color-light padding-x-medium">1 - CTR: Click Through Rate</p>
                            <p className="tiny color-light padding-x-medium">2 - IR: Interaction Rate</p>
                            <p className="tiny color-light padding-x-medium">3 - ER: Engagement Rate</p>
                        </div>
                    </div>
                </div>
                { this.state.metaData !== null &&
                    <Overlay name="datepicker">
                        <DatePicker start={ createdAt } end={ currentDate } updateData={ this.updateDateRange } />
                    </Overlay>
                }
                { this.state.metaData !== null &&
                    <Overlay name="editWrap" ref={ ( overlay ) => { this.editOverlay = overlay; }}>
                        <div className='editProjectWrap innerWrap xsmall-min'>
                            <CardTitle classes="margin-bottom-large">Edit Campaign</CardTitle>
                            <Form>
                                <div className="form-row flex">
                                    {/* TO DO - add campaign option
                                            <Dropdown
                                                wrapClass="half-width flex-grow"
                                                inputClass="bold"
                                                name="campaign"
                                                label="Campaign"
                                                placeholder={ this.state.metaData.campaign.name }
                                                //use placeholder since value doesn't seem to work
                                                options={
                                                    Global.adTypes.sort( SortAlphabetically( 'text' ) )
                                                }
                                                //onChange={ this.selectCampaign }
                                            />
                                            */}
                                    <FormField
                                        wrapClass="half-width flex-grow"
                                        inputClass="outlineInput moveLabelOnFocus forceLabelAbove bold color-blue"
                                        name="campaign-name"
                                        type="text"
                                        label="Campaign Name"
                                        value={ this.state.metaData.name }
                                        required="true"
                                    />
                                    <FormField
                                        wrapClass="half-width flex-grow"
                                        inputClass="outlineInput moveLabelOnFocus forceLabelAbove bold color-blue"
                                        name="operativeOrderId"
                                        type="text"
                                        label="Operative Order Id"
                                        value={ this.state.metaData.operativeOrderId }
                                        required="true"
                                    />
                                </div>
                                <div className="form-actions flex justify-end margin-top-xlarge">
                                    <Button
                                        loader="true"
                                        classes="primary"
                                        icon="pencil"
                                        text="Update Campaign"
                                        onClick={ this.updateCampaign }
                                    />
                                </div>
                            </Form>
                        </div>
                    </Overlay>
                }
            </section>
        );
    }
}

/*
    NEW CAMPAIGN
    Called from create project form
*/
export class NewCampaign extends React.Component {

    constructor( props ){
        super( props );
        this.createNew = this.createNew.bind( this );
    }


    createNew( e ){
        console.log( 'create new campaign' );

        e.preventDefault();

        const button = e.target;
        ButtonState( button, 'error' );

        let data = new FormData();
        data.append( 'name', document.getElementById( 'newCampaignName' ).value );
        data.append( 'advertiserId', this.props.advertiser );
        data.append( 'operativeId', document.getElementById( 'newCampaignIO' ).value );

        fetch( `${ Global.apiEndpoint }/campaign/create`, {
            headers: {
                'apiToken': GetData( 'apiKey', 'session' )
            },
            method: "POST",
            body: data
        } )
            .then( ( response ) => {
                return response.text();
            } )
            .then( ( data ) => {
                console.log( data );
                const dataObj = JSON.parse( data );
                if( dataObj.status === "success" ){
                    this.props.onClose();
                    this.props.oncomplete( dataObj.newCampaign );
                    ButtonState( button, 'success' );
                }else{

                    //if campaign exists return similar data to new campaign but for existing one
                    if( dataObj.error === "exists" ){
                        this.props.onClose();
                        this.props.oncomplete( dataObj.campaign );
                        ButtonState( button, 'success' );
                    }

                    //if campaign exists under a different advertiser
                    if( dataObj.error === "exists under different advertiser" ){
                        ButtonState( button, 'error' );
                        alert( 'The Operative ID number provided matches a campaign associated with a different advertiser. Please update and try again.' );
                    }

                    //if doesn't exist then another error occured
                    else{
                        ButtonState( button, 'error' );
                    }
                }
            } );
    }

    render() {
        return (
            <div className="overlayItem newCampaign">
                <SectionTitle>Create new campaign</SectionTitle>
                <Form>
                    <div className="form-row flex margin-top-large viewport-min-width-thirty">
                        <FormField
                            wrapClass="flex-grow half-width"
                            inputClass="outlineInput moveLabelOnFocus bold color-blue"
                            name="newCampaignName"
                            type="text"
                            label="Campaign Name"
                            //placeholder="Enter Advertiser Name"
                            required="true"
                            //instructions="Enter a name for the new advertiser"
                        />
                        <FormField
                            wrapClass="flex-grow half-width"
                            inputClass="outlineInput moveLabelOnFocus bold color-blue"
                            name="newCampaignIO"
                            type="number"
                            label="Operative Id"
                            //placeholder="Enter Advertiser Name"
                            required="true"
                            instructions="Enter Operative Id or IO Number"
                        />
                    </div>
                    <div className="form-actions flex justify-end margin-top-xlarge">
                        <Button
                            loader="true"
                            classes="primary"
                            icon="pencil"
                            text="Create Campaign"
                            onClick={ this.createNew }
                        />
                    </div>
                </Form>
            </div>
        );
    }

}


/*
    Project Card
*/
class Project extends React.Component{

    render(){

        const p = this.props.data;

        let createdAt = new Date( p.created_at );
        createdAt = new Date( createdAt.setHours( createdAt.getHours() + 4 ) ).toDateString();


        /*
            OVERALL IMPRESSIONS
        */
        const overallImpressionsData = GetOverallImpressions( p.tags, false );
        let overallImpressions = overallImpressionsData.total > 0 ? parseInt( overallImpressionsData.total, Global.radix ) : Global.overallImpressionPlaceholder;

        /*
            GET CLICK EVENTS
            Save tags
            Count up total click on each tag by summing dataByDay
            Count up total clicks for all tags
        */

        //ultimately use type, for now check if redirect is not empty
        /*const clickTags = tags.filter( ( tag ) => {
            return tag.type === "jump";
        } );*/

        const clickTags = p.tags.filter( ( tag ) => {
            return typeof tag.redirect !== "undefined";
        } );

        //total up all clicks
        let clicks = clickTags.reduce( ( total, tag ) => {
            return total += parseInt( tag.total, Global.radix );
        }, 0 );
        if( clicks === 0 ){
            clicks = "--";
        }




        /*
            GET INTERACTION EVENTS
            Save tags
            Count up total interactions for all tags
        */
        const interactionTags = p.tags.filter( ( tag ) => {
            //return tag.type === "enga";
            return typeof tag.redirect === "undefined";
        } );

        //temp until TYPE works
        const filteredInteractionTags = [];
        const filteredViewTags = [];
        for( var j = 0; j < interactionTags.length; j++ ){

            let tag = interactionTags[j];

            //if tag name contains one of the viewTag options, even if it's part of the string
            let isViewTag = Global.viewTagOptions.find( function( option ){
                if( tag.name.includes( option ) ){
                    return true;
                }
            } );
            if( typeof isViewTag !== "undefined" ){
                filteredViewTags.push( tag );
            }else{
                filteredInteractionTags.push( tag );
            }
        }

        //total up all interactions
        let interactions = filteredInteractionTags.reduce( ( total, tag ) => {
            return total += parseInt( tag.total, Global.radix );
        }, 0 );


        //get engaged users
        let engagedUsers = 0;
        if( typeof p.tags.find( x => x.name === 'engagedUser' ) !== "undefined" ){
            engagedUsers = p.tags.find( x => x.name === 'engagedUser' ).total;
        }



        /*
            CALCULATIONS
            If overall impressions exist make calculations to determine ER IR CTR
        */

        //defaults in case data prevents calculations
        let CTR = "--";
        let ER = "--";
        let IR = "--";


        //if impressions exist
        if( overallImpressions !== Global.overallImpressionPlaceholder ){


            //if less than threshold
            if( overallImpressions < Global.overallImpressionThreshold ){
                overallImpressions = Global.overallImpressionPlaceholder;
            }

            //greater than threshold so is Live Creative
            //if no clicks then don't make the calculations
            else if( clicks !== "--" ){
                /*
                    Calculate CTR
                */
                CTR = ( ( clicks / overallImpressions ) * 100 ).toFixed( 2 ) + "%";


                /*
                    Calculate Interaction Rate and Engagement Rate
                */
                IR = ( ( interactions / overallImpressions ) * 100 ).toFixed( 2 ) + "%";
                ER = ( ( engagedUsers / overallImpressions ) * 100 ).toFixed( 2 ) + "%";
            }

        }

        return (
            <div className="projectWrap">
                <div className="project-title">
                    <p className="project-number advertiser-group-letter"></p>
                    <h4 className="cardTitle">{ p.name }</h4>
                </div>
                <div className="project-data padding-xlarge flex justify-between">
                    <div className='dataGroup padding-x-large padding-y-medium'>
                        <div className="dataGroup-labels">
                            <LargeDataLabel key="impressions" label='Impressions' value={ overallImpressions } textAlign="center" />
                        </div>
                    </div>
                    <div className="dataGroup-divider"></div>
                    <div className='dataGroup padding-x-large padding-y-medium'>
                        <div className="dataGroup-labels flex justify-between">
                            <LargeDataLabel key="clicks" label='Clicks' value={ clicks } textAlign="center" />
                            <LargeDataLabel key="ctr" label={[<span key='1'>CTR<sup>1</sup></span>]} value={ CTR } textAlign="center" />
                        </div>
                    </div>
                    <div className="dataGroup-divider"></div>
                    <div className='dataGroup padding-x-large padding-y-medium'>
                        <div className="dataGroup-labels flex justify-between">
                            <LargeDataLabel key="ir" label={[<span key='2'>IR<sup>2</sup></span>]} value={ IR } textAlign="center" />
                            <LargeDataLabel key="er" label={[<span key='3'>ER<sup>3</sup></span>]} value={ ER } textAlign="center" />
                        </div>
                    </div>
                </div>
                <div className="project-actions">
                    <div className="flex vertical-center">
                        <Button
                            classes="small grayDark arrowRight"
                            text='Download CSV'
                            onClick={ this.handleClick }
                        />
                        <Link to={ `/project/${ p.id }` }>
                            <p className="tiny color-darker margin-left-large"><b>Created</b>: { createdAt }</p>
                        </Link>
                        <Link to={ `/project/${ p.id }` } className="flex-align-right">
                            <Button
                                classes="secondary color-blue arrowRight"
                                text='Go To Project'
                            />
                        </Link>
                    </div>
                </div>
            </div>
        );
    }

}
