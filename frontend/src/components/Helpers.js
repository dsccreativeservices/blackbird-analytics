import * as XLSX from 'xlsx';
import * as FileSaver from 'file-saver';
import { Global } from '../json/global'; // global settings

/*
    FORMAT DATE
    Format to MM-DD-YYYY
*/
export function FormatDate(date) {
  const formattedDate = new Date(date);
  function pad(s) {
    return (s < 10) ? `0${s}` : s;
  }
  const d = new Date(formattedDate);

  return [
    pad(d.getMonth() + 1),
    pad(d.getDate()),
    d.getFullYear(),
  ].join('-');
}

/*
    FORMAT DATE
    Format to YYYY-MM-DD
*/
export function FormatDateToPHPFormat(date) {
  const formattedDate = new Date(date);
  function pad(s) {
    return (s < 10) ? `0${s}` : s;
  }
  const d = new Date(formattedDate);

  return [
    d.getFullYear(),
    pad(d.getMonth() + 1),
    pad(d.getDate()),
  ].join('-');
}

/*
    CONVERT DATA TO ARRAYS
    Given an array of objects turn it into an array of arrays
    Keys parameter is an array of keys to use when building new arrays, other data from object will be dumped
*/
export function DataToArrayOfArrays(data, keys = null) {
  const arrayData = [];
  const headers = [];

  for (let i = 0; i < data.length; i++) {
    const obj = data[i];
    const newArray = [];

    for (const [key, value] of Object.entries(obj)) {
      // if first loop also push keys in
      if (keys !== null) {
        if (keys.includes(key) === true) {
          if (i === 0) {
            headers.push(key);
          }
          newArray.push(value);
        }
      } else {
        if (i === 0) {
          headers.push(key);
        }
        newArray.push(value);
      }
    }

    if (i === 0) {
      arrayData.push(headers);
    }
    arrayData.push(newArray);
  }

  return arrayData;
}

/*
    CONVERT DATA TO EXCEL SHEET
    convert array of arrays into rows/columns of a sheet
*/
export function BuildWorksheet(data, opts) {
  const worksheet = {};
  const range = { s: { c: 10000000, r: 10000000 }, e: { c: 0, r: 0 } };
  for (let R = 0; R !== data.length; ++R) {
    for (let C = 0; C !== data[R].length; ++C) {
      if (range.s.r > R) { range.s.r = R; }
      if (range.s.c > C) { range.s.c = C; }
      if (range.e.r < R) { range.e.r = R; }
      if (range.e.c < C) { range.e.c = C; }

      // set cell value
      const cell = { v: data[R][C] };
      if (cell.v == null) { continue; }
      const cell_ref = XLSX.utils.encode_cell({ c: C, r: R });

      // set type of cell
      if (typeof cell.v === 'number') {
        cell.t = 'n';
      } else if (typeof cell.v === 'boolean') {
        cell.t = 'b';
      }
      /* else if(cell.v instanceof Date) {
                cell.t = 'n'; cell.z = XLSX.SSF._table[14];
                cell.v = datenum(cell.v);
            } */
      else {
        cell.t = 's';
      }

      worksheet[cell_ref] = cell;
    }
  }
  if (range.s.c < 10000000) { worksheet['!ref'] = XLSX.utils.encode_range(range); }
  return worksheet;
}

/*
    SAVE TO EXCEL
    key is array of columns to keep in data when converting it
    columnWidth is arrow of objects defining width of columns, like { wch:20 }
*/
export function ExportData(data, keys = null, columnWidths, fileName, dataModifier = null) {
  // modify data to array of arrays
  const modifiedData = DataToArrayOfArrays(data, keys);

  // build new workbook
  function Workbook() {
    if (!(this instanceof Workbook)) { return new Workbook(); }
    this.SheetNames = [];
    this.Sheets = {};
  }

  const workbook = new Workbook();
  let sheet = BuildWorksheet(modifiedData, { name: Global.cellGreen });

  // modify data
  for (const [key, value] of Object.entries(sheet)) {
    const row = key.replace(/\D/g, '');
    const col = key.replace(/[0-9]/g, '');

    // for header
    if (row === '1') {
      value.s = Global.exportStyles.header;
    }

    // for non-header rows
    else if (col === 'A') {
      value.s = Global.exportStyles.text;
    } else if (col === 'B') {
      value.s = Global.exportStyles.url;
    } else if (col === 'C') {
      value.s = Global.exportStyles.number;
    }
  }

  // add sheet to workbook
  workbook.SheetNames.push('Data');
  workbook.Sheets.Data = sheet;

  sheet['!cols'] = columnWidths;

  // this doesn't seem to work in xlsx-style - may need to try js-xlsx again
  sheet['!rows'] = [
    { hpt: 50 },
  ];

  console.log(sheet);

  // if function exists to modify data for this instance
  if (typeof dataModifier === 'function') {
    sheet = dataModifier(sheet);

    console.log('modified data', sheet);
  }

  const wbout = XLSX.write(workbook, { bookType: 'xlsx', bookSST: true, type: 'binary' });

  // create buffer with data
  function s2ab(s) {
    const buf = new ArrayBuffer(s.length);
    const view = new Uint8Array(buf);
    for (let i = 0; i !== s.length; ++i) {
      view[i] = s.charCodeAt(i) & 0xFF;
    }
    return buf;
  }

  FileSaver.saveAs(
    new Blob(
      [s2ab(wbout)],
      { type: 'application/octet-stream' },
    ),
    fileName,
  );

  return 'success';
}

/*
    sort to alphabetical order
*/
export function SortAlphabetically(key, order = 'asc') {
  return function (a, b) {
    if (!a.hasOwnProperty(key) || !b.hasOwnProperty(key)) {
      // property doesn't exist on either object
      return 0;
    }

    const varA = (typeof a[key] === 'string') ? a[key].toUpperCase() : a[key];
    const varB = (typeof b[key] === 'string') ? b[key].toUpperCase() : b[key];

    let comparison = 0;
    if (varA > varB) {
      comparison = 1;
    } else if (varA < varB) {
      comparison = -1;
    }
    return (
      (order === 'desc') ? (comparison * -1) : comparison
    );
  };
}

/*
    Group data by letter to create alpha groups
    key will default to name, but can be overridden
*/
export function GroupByLetter(data, key = 'name') {
  const result = {};

  // loop through data
  for (const d of data) {
    // if id is 0 it's probably a helper field ( like Create New ) and not real data, so skip
    if (d.id === 0) {
      continue;
    }

    // get first letter
    const firstLetter = d[key].charAt(0).toUpperCase();

    if (typeof result[firstLetter] === 'undefined') {
      result[firstLetter] = [d];
    } else {
      result[firstLetter].push(d);
    }
  }

  return result;
}

/*
    Get Siblings of node
*/
export function GetSiblings(node) {
  const siblings = [];
  let sibling = node.parentNode.firstChild;
  for (; sibling; sibling = sibling.nextSibling) {
    // if not a node (like text string), or if node we're trying to find siblings for - then skip
    if (sibling.nodeType !== 1 || sibling === node) {
      continue;
    }
    siblings.push(sibling);
  }
  return siblings;
}

/*
    Find Parent node with class
*/
export function FindParent(el, selector) {
  // while( ( el = el.parentNode ) && el.className.indexOf( selector ) < 0 );
  // return el;

  // polyfill matches
  if (!Element.prototype.matches) {
	    Element.prototype.matches = Element.prototype.matchesSelector
	        || Element.prototype.mozMatchesSelector
	        || Element.prototype.msMatchesSelector
	        || Element.prototype.oMatchesSelector
	        || Element.prototype.webkitMatchesSelector
	        || function (s) {
	            const matches = (this.document || this.ownerDocument).querySelectorAll(s);
	                let i = matches.length;
	            while (--i >= 0 && matches.item(i) !== this) {}
	            return i > -1;
	        };
  }

  // Get the closest matching element
  for (; el && el !== document; el = el.parentNode) {
    if (el.matches(selector)) return el;
  }
  return null;
}

/*
    CHECK AUTH STATUS
    Check session storage variable to determine if login has been completed during this session already
    If not, redirect to login form
    If yes, continue
*/
export function CheckAuth() {
  console.log('check if user is authenticated');

  const authStatus = GetData('authStatus', 'session');
  if (authStatus === null) {
    console.log('not logged in');
    if (window.location.pathname !== '/login') {
      window.location.href = '/login';
    }
  } else {
    console.log('logged in');
  }
}

/*
    STORE DATA
    Save data into browser local storage
*/
export function StoreData(key, value, type) {
  console.log('store data', key);
  const rememeberMeKey = localStorage.rememberMe;

  if (rememeberMeKey === 'local') {
    localStorage[key] = JSON.stringify(value);
  }
  if (rememeberMeKey === 'session') {
    sessionStorage[key] = JSON.stringify(value);
  }
}

/*
    GET DATA
    Get data from browser local storage
*/
export function GetData(key, type) {
  const rememeberMeKey = localStorage.rememberMe;
  if (rememeberMeKey === 'local') {
    return JSON.parse(localStorage.getItem(key));
  }
  if (rememeberMeKey === 'session') {
    return JSON.parse(sessionStorage.getItem(key));
  }
  localStorage.rememberMe = 'session';
}

/*
    OVERALL IMPRESSIONS
    get all tags matching the overall tag option.
    count up all the dates to get the total
*/
export function GetOverallImpressions(tags, showByDay) {
  const overallTags = tags.filter((tag) => {
    if (Global.overallTagNames.indexOf(tag.name) > -1 && typeof tag.redirect === 'undefined') {
      return tag.name;
    }
  });
  console.log('found these overall impression tags:', overallTags);

  // create combined dataByDay from all tags
  const dataByDay = [];
  if (showByDay === true) {
    for (const t of overallTags) {
      for (const d of t.dataByDay) {
        d.total = parseInt(d.total, Global.radix);
        const existingDay = dataByDay.find((day) => day.date === d.date);
        if (typeof existingDay === 'undefined') {
          dataByDay.push(d);
        } else {
          existingDay.total += d.total;
        }
      }
    }
    return {
      dataByDay: dataByDay.sort(SortAlphabetically('date')),
      total: dataByDay.reduce((total, day) => total += parseInt(day.total, Global.radix), 0),
    };
  }
  return {
    total: overallTags.reduce((total, tag) => total += parseInt(tag.total, Global.radix), 0),
  };
}

export function genRandID(length) {
  const chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZ';
  let randomstring = '';
  for (let i = 0; i < length; i++) {
    const rnum = Math.floor(Math.random() * chars.length);
    randomstring += chars.substring(rnum, rnum + 1).toLowerCase();
  }

  return randomstring;
}

/*
    FORMAT NUMBER
    formats the number with commas
* */

export function formatNumber(num) {
  return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
}

/**
 * ISSUE TRACKER
 */
// Save issue to trello
export function saveToIssueTracker(type, title, desc) {
  const apiKey = 'f85c5e434d8709e1f11d146fb8ffd315&token=322d6ffea0ffbe61758aa0e0908776c84269dc7960582fd2aa968fa2442595c4';
  const token = '';
  let listID = '5c05a87258a1d279d4b87a8b';
  let labels = '5c068b7b65b7925246e20e72';

  if (type === 'feature') {
    listID = '5c05a87258a1d279d4b87a8b';
    labels = '5c068b7b65b7925246e20e72,5c069aaedc27077476c5f967';
  } else {
    listID = '5c05a8709901902416012e06';
  }

  const url = `https://api.trello.com/1/cards/?key=${apiKey}&idList=${listID}&desc=${desc}&pos=top&name=${title}&idLabels=${labels}`;
}

// Get issue from trello
export function getIssueCard(cardID) {
  const apiKey = 'f85c5e434d8709e1f11d146fb8ffd315&token=322d6ffea0ffbe61758aa0e0908776c84269dc7960582fd2aa968fa2442595c4';

  const url = `https://api.trello.com/1/batch?urls=/cards/${cardID}/actions?filter=commentCard,/cards/${cardID}&key=${apiKey}`;
}
