import React from 'react';
import { Link } from 'react-router-dom';

export class SubmenuItem extends React.Component {
    render(){
        return (
            <li className={ this.props.className } data-type={ this.props.type }>
                <Link to={ this.props.url }>{ this.props.name }</Link>
            </li>
        );
    }
}
