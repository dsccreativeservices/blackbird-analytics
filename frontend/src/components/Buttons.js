import React from 'react';
import { Icon } from './Icons';

/*
    CREATE BUTTON ELEMENT
*/
export class Button extends React.Component {
    render() {

        let button = "";

        if( this.props.loader === "true" ){
            button = (
                <button
                    id={ this.props.id }
                    className={'button progressLoader ' + this.props.classes }
                    data-function={ this.props.role }
                    onClick={ this.props.onClick }
                    type={ this.props.type }
                >
                    <span className="buttonInner">
                        { this.props.icon &&
                            <Icon src={ this.props.icon } />
                        }
                        { this.props.text }
                    </span>
                    <span className="progress">
                        <span className="progress-inner notransition"></span>
                    </span>
                </button>
            );
        } else{
            button = (
                <button
                    id={ this.props.id }
                    className={'button ' + this.props.classes }
                    data-function={ this.props.role }
                    onClick={ this.props.onClick }
                    type={ this.props.type }
                >
                    <span className="buttonInner">
                        { this.props.icon &&
                            <Icon src={ this.props.icon } />
                        }
                        { this.props.text }
                    </span>
                </button>
            );
        }

        return button;

    }
}



/*
    Button events from fetch requests
*/
export function ButtonState( btn, state ){
    btn.classList.remove( "state-loading", "state-error", "state-success" );
    btn.classList.add( `state-${ state }` );

    setTimeout( function(){
        btn.classList.remove( `state-${ state }` );
    }, 2000 );
}
