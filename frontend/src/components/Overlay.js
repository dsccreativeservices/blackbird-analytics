import React from 'react';
import { Button } from './Buttons';

/*
    Overlay/popup lightbox
*/
export class Overlay extends React.Component {
    constructor( props ){
        super( props );
        this.closeOverlay = this.closeOverlay.bind( this );
        this.show = this.show.bind( this );
    }

    closeOverlay(){
        this.lightbox.classList.remove( "active" );
        const items = document.getElementsByClassName( 'overlayItem' );
        for( let i of items ){
            i.classList.remove( "active" );
        }
    }

    show(){
        this.lightbox.classList.add( "active" );
    }

    render() {

        //insert the onclose method into each child so it can be called from within the child
        const { children } = this.props;
        let childrenWithProps = React.Children.map( children, child =>
            React.cloneElement( child, { onClose: this.closeOverlay } )
        );

        let id = "lightbox";
        if( typeof this.props.name !== "undefined" ){
            id = this.props.name;
        }

        return (
            <div id={ id } className="overlay" ref={ ( el ) => { this.lightbox = el; }}>
                <div className="overlayInner padding-large">
                    <Button
                        classes="close"
                        text="+"
                        onClick={ this.closeOverlay }
                    />
                    { childrenWithProps }
                </div>
            </div>
        );

    }

}
