import React from 'react';
import { Global } from '../json/global'; //global settings
import { Button } from  './Buttons';
import { Graph, Table } from './Graph';
import { FormatNumber } from './Formatting';
import { ExportData } from './Helpers';
import { CardTitle } from './Typography';


function filterData( data, min ){
    return data.filter( ( d ) => {
        return d.total > min;
    } );
}


/*
    CLICK SECTION
    Returns click detailed info
*/
export class ClicksSection extends React.Component {
    constructor( props ){
        super( props );

        this.state = {
            clickTags: this.props.tags,
            totalClicks: this.props.total,
            CTR: this.props.CTR,
            overallImpressions: this.props.overallImpressions
        };

        this.downloadData = this.downloadData.bind( this );
    }

    /*
        called when new params are passed to the component, like changing between projects
    */
    componentWillReceiveProps( nextProps ){

        if( this.state.totalClicks !== nextProps.total ){
            this.setState( {
                clickTags: nextProps.tags,
                totalClicks: nextProps.total,
                CTR: nextProps.CTR,
                overallImpressions: nextProps.overallImpressions
            } );
        }
    }

    /*
        Download Click Data
    */
    downloadData( data ){
        const exportStatus = ExportData(
            data,
            [ 'name', 'redirect', 'total' ],
            [
                { wch: 30 },
                { wch: 50 }
            ],
            `project_${ this.props.projectId }_click_data.xlsx`
        );
        console.log( exportStatus );
    }


    render() {
        console.log(this.state);

        if( typeof this.state.totalClicks !== 'undefined'){

            const filteredData = filterData( this.state.clickTags, Global.minTagCount );
            //const filteredData = this.state.clickTags;
            // console.log(this.state.clickTags);
            console.log(filteredData);

            if(filteredData.length > 0){
                return (
                    <div className="dataSection" data-type='clicks'>
                        <div className="dataHeader flex vertical-center">
                            <h4 className="cardTitle">Clicks</h4>
                            <Button
                                classes="small gray arrowRight margin-left-medium"
                                text="Download Data"
                                onClick={ this.downloadData.bind( this, filteredData ) }
                            />
                            <div className="flex flex-align-right">
                                <h3 className="sectionTitle">
                                    <FormatNumber num={ this.state.totalClicks } />
                                    <span className="xsmall margin-left-xsmall">Clicks</span>
                                </h3>
                                <h3 className="sectionTitle margin-left-medium">
                                    { this.state.CTR }
                                    <span className="xsmall margin-left-xsmall">CTR</span>
                                </h3>
                            </div>
                        </div>
                        <Graph id="clicksGraph" data={ filteredData } type="clicks" showHeader="true" graphStyle="lines"  />
                        <Table data={ filteredData } type="clicks" />
                    </div>
                );
            }
            else{
                return(
                    <div className="dataSection empty">
                        <div>No Data</div>
                    </div>
                )
            }
        }

        //return
        else{
            return (
                <div className="dataSection empty">
                    <div className='noData'>No Data</div>
                </div>
            );
        }
    }
}



/*
    INTERACTION SECTION
    Returns detailed interaction info
*/
export class InteractionsSection extends React.Component {
    constructor( props ){
        super( props );
        this.buildData = this.buildData.bind( this );
        this.downloadData = this.downloadData.bind( this );
    }


    /*
        called when new params are passed to the component, like changing between projects
    */
    componentWillReceiveProps( nextProps ){
        this.setState( {
            interactionTags: nextProps.interactions,
            //clickTags: nextProps.clicks,
            totalInteractions: nextProps.total,
            ER: nextProps.ER,
            IR: nextProps.IR,
            //engagedUsers: nextProps.engagedUsers,
            //engagedUsersTags: nextProps.engagedUsersByDay,
            //overallImpressions: nextProps.overallImpressions,
            graphData: this.buildData(
                nextProps.interactions,
                nextProps.clicks,
                nextProps.engagedUsersByDay
            )
        } );
    }

    /*
        called when component first loads, not called for changes
    */
    //componentDidMount() {
    componentWillMount() {
        this.setState( {
            interactionTags: this.props.interactions,
            //clickTags: this.props.clicks,
            totalInteractions: this.props.total,
            ER: this.props.ER,
            IR: this.props.IR,
            //engagedUsers: this.props.engagedUsers,
            //engagedUsersTags: this.props.engagedUsersByDay,
            //overallImpressions: this.props.overallImpressions,
            graphData: this.buildData(
                this.props.interactions,
                this.props.clicks,
                this.props.engagedUsersByDay
            )
        } );
    }


    /*
        Build data for graph
        For interaction graph is total engagements vs engaged users - not individual tags
        return array of 2 objects, with name and dataByDay
    */
    buildDayData( data, array ){
        for( var i = 0; i < data.length; i++ ){
            let dataSet = data[i].dataByDay;

            for ( var j = 0; j < dataSet.length; j++ ) {
                let day = dataSet[j];

                //first see if data exists, if not create
                if( typeof array.find( item => item.date === day.date ) === "undefined" ){
                    let dataObj = {
                        date: day.date,
                        total: parseInt( day.total, Global.radix )
                    };
                    array.push( dataObj );
                }

                //if it does exist add to it
                else{
                    var item = array.find( item => item.date === day.date );
                    item.total += parseInt( day.total, Global.radix );
                }
            }
        }
        return array;
    }
    buildData( interactions, clicks, engagedUsers ){

        //merge click and interactions data into array by days
        const intsOnly = this.buildDayData( interactions, [] );
        const clickAndInts = this.buildDayData( clicks, intsOnly );

        //console.log( engagedUsers );

        return [
            {
                name: 'total_engagements',
                dataByDay: clickAndInts
            },
            {
                name: 'engaged_users',
                dataByDay: engagedUsers
            }
        ];
    }

    /*
        Download Imp Data
    */
    downloadData( data ){
        const exportStatus = ExportData(
            data,
            [ 'name', 'total' ],
            [
                { wch: 30 },
                { wch: 10 }
            ],
            `project_${ this.props.projectId }_interaction_tags.xlsx`
        );
        console.log( exportStatus );
    }



    render() {

        const filteredData = filterData( this.state.interactionTags, Global.minTagCount );
        console.log(this.state);

        if( typeof this.state.graphData !== 'undefined' || this.state.interactionTags.length > 1){

            return (
                <div className="dataSection" data-type='interactions'>
                    <div className="dataHeader flex vertical-center">
                        <h4 className="cardTitle">Interactions</h4>
                        <Button
                            classes="small gray arrowRight margin-left-medium"
                            text="Download Data"
                            onClick={ this.downloadData.bind( this, this.state.interactionTags )  }
                        />
                        <div className="flex flex-align-right">
                            <h3 className="sectionTitle">
                                <FormatNumber num={ this.state.totalInteractions } />
                                <span className="xsmall margin-left-xsmall">Interactions</span>
                            </h3>
                            <h3 className="sectionTitle margin-left-medium">
                                { this.state.ER }
                                <span className="xsmall margin-left-xsmall">ER</span>
                            </h3>
                            <h3 className="sectionTitle margin-left-medium">
                                { this.state.IR }
                                <span className="xsmall margin-left-xsmall">IR</span>
                            </h3>
                        </div>
                    </div>
                    <Graph id="interactionGraph" data={ this.state.graphData } type="interactions" showHeader="true" graphStyle="filledLines" />
                    <Table data={ filteredData } type="interactions" />
                </div>
            );
        }

        //return
        else{
            return (
                <div>test</div>
            );
        }
    }
}


/*
    Carousel SECTION
    Returns carousel detailed info
*/
export class CarouselSection extends React.Component {
    constructor( props ){
        super( props );

        this.state = {
            tags: this.props.tags
        };

        console.log("Inside Carousel SEction")
    }

    handleDownload() {
        console.log( 'download data' );
    }

    /*
        called when new params are passed to the component, like changing between projects
    */
    componentWillReceiveProps( nextProps ){
        this.setState( {
            tags: nextProps.tags
        } );
    }

    /*
        Filter Data
        filter out any tags with low counts ( less than 10 )
    */
    filterData( data ){
        let filteredData = [];
        for( var i = 0; i < data.length; i++ ){
            let dataLength = data[i].dataByDay.length;

            if( dataLength > Global.minTagCount ){
                filteredData.push( data[i] );
            }
        }
        return filteredData;
    }


    render() {

        let JSX = "";

        if( typeof this.state.tags !== 'undefined' ){

            //const filteredData = this.filterData( this.state.tags );
            const filteredData = filterData( this.state.tags, Global.minTagCount, true );
            console.log( filteredData );

            if( filteredData.length > 0 ){
                JSX = (
                    <div className="dataSection" data-type='carousel'>
                        <div className="dataHeader flex vertical-center">
                            <h4 className="cardTitle">Carousel Views <span>( by slide )</span></h4>
                            <Button
                                classes="small gray arrowRight margin-left-medium"
                                text="Download Data"
                                onClick={ this.handleDownload }
                            />
                        </div>
                        <div className="flex">
                            <Graph id="carouselGraph" data={ filteredData } type="interactions" showHeader="true" graphStyle="lines"  />
                            <Table data={ filteredData } type="interactions" />
                        </div>
                    </div>
                );
            }else{
                JSX = (
                    <div className='noData'>No Data</div>
                );
            }

        }

        //return
        else{
            JSX = (
                <div className='noData'>No Data</div>
            );
        }

        return JSX;
    }
}







/*
    VIDEO SECTION
    Returns video detailed info
*/
export class VideoSection extends React.Component {
    constructor( props ){
        super( props );

        this.state = {
            tags: this.props.tags
        };
    }

    handleDownload() {
        console.log( 'download data' );
    }

    /*
        called when new params are passed to the component, like changing between projects
    */
    componentWillReceiveProps( nextProps ){
        console.log(nextProps);
        this.setState( {
            tags: nextProps.tags
        } );
    }

    /*
        Filter Data
        split into autoplay vs user engaged
        DO NOT filter out low count data for video
    */
    filterData( data ){
        console.log(data);
        let filteredData = {
            autoplay: [],
            user: []
        };

        //Find Video items with autoplay
        filteredData.autoplay = data.filter(item => item.name.includes("autoplay"));

        //Find Video Items without autoplay
        filteredData.user = data.filter(item => item.name.toLowerCase().indexOf("autoplay") === -1);

        return filteredData;
    }


    render() {

        let JSX = "";

        if( typeof this.state.tags !== 'undefined' ){

            const filteredData = this.filterData( this.state.tags );
            console.log( filteredData );

            if( typeof filteredData.autoplay !== "undefined" || typeof filteredData.user !== "undefined" ){
                JSX = (
                    <div className="dataSection" data-type='video'>
                        <div className="dataHeader flex vertical-center">
                            <h4 className="cardTitle">Video Quartiles</h4>
                            <Button
                                classes="small gray arrowRight margin-left-medium"
                                text="Download Data"
                                onClick={ this.handleDownload }
                            />
                        </div>
                        <div className="flex">
                            { /* <Graph id="videoGraph" data={ filteredData } type="interactions" showHeader="true" graphStyle="lines"  /> */ }
                            <div className="videoMetrics flex-grow">
                                <CardTitle classes="margin-bottom-small">Autoplay</CardTitle>
                                <Table id="autoplay" data={ filteredData.autoplay } type="interactions" />
                            </div>
                            <div className="videoMetrics flex-grow">
                                <CardTitle classes="margin-bottom-small">Engaged</CardTitle>
                                <Table id="engaged" data={ filteredData.user } type="interactions" />
                            </div>
                        </div>
                    </div>
                );
            }else{
                JSX = (
                    <div className='noData'>No Data</div>
                );
            }

        }

        //return
        else{
            JSX = (
                <div className='noData'>No Data</div>
            );
        }

        return JSX;
    }
}
