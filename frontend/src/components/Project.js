import React from 'react';
import { Link, Redirect } from 'react-router-dom';
import { Global } from '../json/global'; //global settings
import { SortAlphabetically, ExportData, GetData, GetOverallImpressions } from './Helpers'; //global settings
import { Parents } from './Parents';
import { Siblings } from './Siblings';
import { MetaHeader } from './MetaHeader';
import { Button } from './Buttons';
import { Icon } from './Icons';
import { Overlay } from './Overlay';
import { Form, FormField, Dropdown } from './Inputs';
import { DatePicker } from './Calendar';
import { LargeDataLabel } from './DataLabels';
import { ClicksSection, InteractionsSection, CarouselSection, VideoSection } from './DataVis';
import { FormatNumber } from './Formatting';
import { GetAdvertisers, NewAdvertiser } from './Advertiser';
import { NewCampaign } from './Campaign';
import { PageTitle, CardTitle } from './Typography';
import { Graph} from "./Graph";

//npm modules
import * as moment from 'moment';

//styles
import '../css/advertiser.css';

/*
    CREATE NEW PROJECT
*/
export class CreateProject extends React.Component {
    constructor( props ){
        super( props );

        this.state = {
            advertisers: null,
            selectedAdvertiser: null,
            campaigns: null,
            selectedCampaign: null,
            foundByIO: false
        };

        this.findIO = this.findIO.bind( this );
        this.newAdvertiserComplete = this.newAdvertiserComplete.bind( this );
        this.selectAdvertiser = this.selectAdvertiser.bind( this );
        this.selectCampaign = this.selectCampaign.bind( this );
        this.newCampaignComplete = this.newCampaignComplete.bind( this );
        this.create = this.create.bind( this );
        this.clear = this.clear.bind( this );
    }



    /*
        FIND IO
        Given the IO, test if a campaign exists
        Return advertiser / campaign
    */
    findIO( data ){
        console.log( "find advertiser and campaign by IO number:", data.target.value );

        if( data.target.value === "" ){
            return false;
        }

        fetch( `${ Global.apiEndpoint }/campaign/findByIO/${ data.target.value }`, {
            headers: {
                'apiToken': GetData( 'apiKey', 'session' )
            }
        } )
            .then( ( response ) => {
                return response.text();
            } )
            .then( ( data ) => {
                let dataObj = JSON.parse( data );
                console.log( dataObj );

                const formField = document.querySelector( '.form-item[data-content="operative_id"]' );

                //exists
                if( dataObj.campaign !== null ){

                    formField.querySelector( '.instruct' ).style.display = 'block';
                    formField.querySelector( '.inputMessage' ).innerHTML = "";

                    this.setState( {
                        foundByIO: true,
                        selectedAdvertiser: dataObj.advertiser.id,
                        selectedAdvertiserName: dataObj.advertiser.name,
                        campaigns: [{
                            id: dataObj.campaign.id,
                            text: dataObj.campaign.name
                        }],
                        selectedCampaign: dataObj.campaign.id,
                        selectedCampaignName: dataObj.campaign.name
                    } );
                }

                //does not exist
                else{
                    formField.querySelector( '.instruct' ).style.display = 'none';
                    formField.querySelector( '.inputMessage' ).innerHTML = "No campaign was found with this Operative ID.";
                    this.setState( {
                        foundByIO: false,
                        selectedAdvertiser: null,
                        selectedAdvertiserName: null,
                        selectedCampaign: null,
                        selectedCampaignName: null
                    } );
                }

            } );
    }



    /*
        NEW ADVERTISER
        Show the lightbox to create a new advertiser
    */
    newAdvertiser(){
        document.getElementById( 'lightbox' ).classList.toggle( "active" );
        document.querySelector( '.overlayItem.newAdvertiser' ).classList.toggle( "active" );
    }

    /*
        NEW ADVERTISER SUCCESS
        Callback for successfully creating new advertiser
        Update form field
    */
    newAdvertiserComplete( advertiserData, exists = false ){
        console.log( 'new advertiser created' );

        //if selected an existing advertiser load campaigns instead
        if( exists === true ){

        }

        this.setState( {
            advertisers: [{
                id: advertiserData.id,
                text: advertiserData.name
            }],
            selectedAdvertiser: advertiserData.id,
            selectedAdvertiserName: advertiserData.name,
            campaigns: [{
                id: 0,
                text: "Create New Campaign"
            }]
        } );

    }


    /*
        SELECT ADVERTISER
        Triggered by selection on advertiser Dropdown
        If selected item is "Create New", show create new dialog instead
    */
    selectAdvertiser( data ){

        const selected = data.target.option;

        //create new
        if( selected.id === "0" ){
            this.newAdvertiser();
        }

        //retrieve campaigns
        else{
            const advertiser_id = parseInt( selected.id.replace( "item_", "" ), Global.radix );

            /*const component = this;
            this.getCampaigns( advertiser_id, function( campaigns ){
                component.setState( {
                    selectedAdvertiser: advertiser_id,
                    campaigns: campaigns
                } );
            } );*/
            this.getCampaigns( advertiser_id );

        }
    }



    /*
        GET CAMPAIGNS
        Set state to update campaign dropdown
    */
    getCampaigns( advertiser_id ){
        console.log( advertiser_id );
        fetch( `${ Global.apiEndpoint }/advertisers/${ advertiser_id }/campaigns`, {
            headers: {
                'apiToken': GetData( 'apiKey', 'session' )
            }
        } )
            .then( ( response ) => {
                return response.text();
            } )
            .then( ( data ) => {
                let dataObj = JSON.parse( data );

                //format campaigns
                const campaigns = [];
                if( dataObj.campaigns !== null ){
                    for( let c of dataObj.campaigns ){
                        campaigns.push( {
                            id: c.id,
                            text: c.name
                        } );
                    }
                }

                //add create new option to the bottom of the campaign list
                campaigns.push( {
                    id: 0,
                    text: "Create New Campaign"
                } );

                this.setState( {
                    selectedAdvertiser: advertiser_id,
                    campaigns: campaigns
                } );

            } );
    }



    /*
        SELECT CAMPAIGN
        Callback for when the campaign is selected in the dropdown
    */
    selectCampaign( data ){
        const selected = data.target.option;

        //create new
        if( selected.id === "0" ){
            console.log( 'create new' );
            this.newCampaign();
        }

        //existing one
        else{
            this.setState( {
                selectedCampaign: parseInt( selected.id, Global.radix )
            } );
        }
    }


    /*
        NEW CAMPAIGN
        Show the lightbox to create a new campaign
    */
    newCampaign(){
        document.getElementById( 'lightbox' ).classList.toggle( "active" );
        document.querySelector( '.overlayItem.newCampaign' ).classList.toggle( "active" );
    }

    /*
        NEW CAMPAIGN SUCCESS
        Callback for successfully creating new campaign
        Update form field
    */
    newCampaignComplete( campaignData ){
        console.log( 'new campaign created' );

        this.setState( {
            campaigns: [{
                id: campaignData.id,
                text: campaignData.name
            }],
            selectedCampaign: campaignData.id,
            selectedCampaignName: campaignData.name
        } );
    }


    /*
        CREATE PROJECT
    */
    create( e ){
        console.log( 'create project', this.state );
        e.preventDefault();

        let error = false;

        const button = e.target;
        button.classList.remove( "state-error" );
        button.classList.add( "state-loading" );

        function inputError( input ){
            input.classList.add( "error" );
            button.classList.remove( "state-loading" );
            button.classList.add( "state-error" );
        }
        function inputClear( input ){
            input.classList.remove( "error" );
            button.classList.remove( "state-error" );
        }

        //build data
        const c_input = document.querySelector( '.form-item[data-content="campaign"]' );
        //const c_id = c_input.querySelector('select').value;
        const name_input = document.querySelector( '.form-item[data-content="project_name"] input' );
        const name = name_input.value;
        const adtype_input = document.querySelector( '.form-item[data-content="adtype"]' );
        let adtype = adtype_input.querySelector( 'select' ).value;
        const site = document.querySelector( '.form-item[data-content="site"] select' ).value;
        const placement = document.querySelector( '.form-item[data-content="placement"] select' ).value;
        const ooid = document.querySelector('.form-item[data-content="project_ooid"] input').value;

        //if campaign is selected
        if( this.state.selectedCampaign === null ){
            inputError( c_input.querySelector( '.rs3__container' ) );
            error = true;
            return false;
        }else{
            inputClear( c_input.querySelector( '.rs3__container' ) );
        }

        //if name is filled out
        if( name === "" ){
            inputError( name_input );
            error = true;
            return false;
        }else{
            inputClear( name_input );
        }

        //if adtype is selected
        if( adtype === "" ){
            error = true;
            inputError( adtype_input.querySelector( '.rs3__container' ) );
        }else{
            inputClear( adtype_input.querySelector( '.rs3__container' ) );
            adtype = adtype.replace( "adtype_", "" );
        }


        let data = new FormData();
        // let data = {
        //     campaign_id: this.state.selectedCampaign,
        //     name: name,
        //     adtype: adtype,
        //     site: "",
        //     placement: "",
        //     createdBy: GetData( "userId", "session" )
        // };


        data.append( 'campaign_id', this.state.selectedCampaign );
        data.append( 'name', name );
        data.append( 'adtype', adtype );

        if( site !== "" ){
            data.append( 'site', site.replace( "site_", "" ) );
            // data.site = site;
        }
        if( placement !== "" ){
            data.append( 'placement', placement.replace( "placement_", "" ) );
            // data.placement = placement;
        }
        if( ooid !== "" ){
            data.append( 'ooid', ooid );
            // data.placement = placement;
        }

        data.append( "createdBy", GetData( "userId", "session" ) );
        console.log(data);

        if(!error){
            fetch( `${ Global.apiEndpoint }/project/create`, {
                headers: {
                    'apiToken': GetData( 'apiKey', 'session' )
                },
                method: "POST",
                body: data
            } )
                .then( ( response ) => {
                    return response.text();
                } )
                .then( ( data ) => {
                    const dataObj = JSON.parse( data );

                    if( dataObj.status === "success" ){
                        button.classList.remove( "state-loading" );
                        button.classList.add( "state-success" );

                        setTimeout( function(){
                            button.classList.remove( "state-success" );
                        }, 2000 );

                        this.setState( {
                            newProjectId: dataObj.id,
                            newProjectName: dataObj.name
                        } );


                    }

                } );
        }

    }

    /*
        CLEAR FORM
    */
    clear( e ){
        console.log( 'reset the form' );
        e.preventDefault();
    }

    /*
        called when component first loads, not called for changes
    */
    componentDidMount() {

        //prevent users without permissions from being able to see create form
        const userGroup = GetData( 'userGroup', 'session' );
        if( userGroup !== "admin" && userGroup !== "editor" ){
            this.setState( {
                notAllowed: true
            } );
            return false;
        }

        let component = this;
        GetAdvertisers( function( data ){

            //add text property to match name for select3
            let advertisers = data.sort( SortAlphabetically( 'name' ) );
            for( let a of advertisers ){
                a.text = a.name;
            }

            //add create new to the bottom of advertiser list
            advertisers.push( {
                id: 0,
                text: "Create New Advertiser"
            } );

            component.setState( {
                advertisers: advertisers
            } );
        } );
    }



    render() {

        // console.log( this.state );

        if( this.state.notAllowed === true ){
            return (
                <Redirect to={ '/dashboard' }/>
            );
        }


        //advertiser dropdown
        let advertiserDropdown = null;

        //if advertiser hasn't been selected yet
        if( this.state.selectedAdvertiser === null ){
            advertiserDropdown = <Dropdown
                wrapClass="flex-grow margin-right-medium"
                inputClass="bold"
                name="advertiser"
                label="Advertiser"
                placeholder="Select Advertiser or Create New"
                instructions="Choose an Advertiser or create a new one"
                options={ this.state.advertisers }
                onChange={ this.selectAdvertiser }
            />;
        }

        //if advertiser selection has been made
        else{
            advertiserDropdown = <Dropdown
                wrapClass="flex-grow margin-right-medium"
                inputClass="bold"
                name="advertiser"
                label="Advertiser"
                options={ this.state.advertisers }
                placeholder={ this.state.selectedAdvertiserName }
                disabled={ true }
            />;

            //force advertiser dropdown to act selected
            setTimeout( function(){
                document.querySelector( '.form-item[data-content="advertiser"] .rs3__container' ).classList.add( "rs3--selected" );
            }, 200 );

        }




        //campaign dropdown
        //when no campaign data has been loaded yet
        let campaignDropdown = <Dropdown
            wrapClass="flex-grow"
            inputClass="bold"
            name="campaign"
            label="campaign"
            placeholder="Select or Create Advertiser First"
            instructions="Once you select an advertiser, campaigns will load here."
            disabled={ true }
        />;

        //once campaigns are retrieved
        if( this.state.campaigns !== null ){

            //if a specific campaign has been selected
            if( this.state.selectedCampaign !== null ){
                campaignDropdown = <Dropdown
                    wrapClass="flex-grow"
                    inputClass="bold"
                    name="campaign"
                    label="campaign"
                    options={ this.state.campaigns }
                    placeholder={ this.state.selectedCampaignName } //using name instead because dropdown wouldn't select correctly
                    //value={ this.state.selectedCampaign }
                    disabled={ true }
                />;

                //force campaign dropdown to be selected
                document.querySelector( '.form-item[data-content="campaign"] .rs3__container' ).classList.add( "rs3--selected" );
            }

            //if allowing choice of campaign from dropdown
            else{
                campaignDropdown = <Dropdown
                    wrapClass="flex-grow"
                    inputClass="bold"
                    name="campaign"
                    label="campaign"
                    placeholder="Select or Create Campaign"
                    instructions="Choose a Campaign or create a new one"
                    options={
                        this.state.campaigns
                    }
                    onChange={ this.selectCampaign }
                />;
            }
        }




        return (
            <section className="projectPage">

                <header className="headerSlantedEdge background-backgroundDark color-onDarkBackground padding-y-xlarge text-align-center">
                    <Icon src='pencil' />
                    <PageTitle classes="color-white">Create Project</PageTitle>
                </header>

                <div className="mainContent">
                    <div className="innerWrap medium ">
                        <div className="innerWrap-inner background-background padding-x-xlarge padding-y-xlarge">

                            <Form>
                                <div className="form-row flex pos-relative">
                                    <FormField
                                        wrapClass="flex-grow margin-right-large"
                                        inputClass="outlineInput moveLabelOnFocus forceLabelAbove bold color-blue"
                                        name="operative_id"
                                        type="text"
                                        label="Operative Id / IO Number"
                                        placeholder="Enter Operative Id"
                                        required="true"
                                        instructions="If you know the IO number for the campaign enter it here. Otherwise use the dropdowns to the right."
                                        //onFocus={ this.selectDate }
                                        onBlur={ this.findIO }
                                    />
                                    <Button
                                        classes="small pos-absolute background-background gray"
                                        text="Find"
                                        //onClick={ this.findIO }
                                    />
                                    <div className="separator separator-or margin-right-medium margin-top-small">
                                        <p>or</p>
                                    </div>

                                    { advertiserDropdown }

                                    { campaignDropdown }
                                </div>
                                <div className="form-row flex margin-top-xlarge">
                                    <FormField
                                        wrapClass="flex-grow margin-right-medium"
                                        inputClass="outlineInput moveLabelOnFocus forceLabelAbove bold color-blue"
                                        name="project_name"
                                        type="text"
                                        label="Project Name"
                                        placeholder="Enter Project Name"
                                        required="true"
                                        //instructions=""
                                        //onFocus={ this.selectDate }
                                    />
                                    <Dropdown
                                        wrapClass="sixth-width flex-grow"
                                        inputClass="bold"
                                        name="adtype"
                                        label="Adtype"
                                        placeholder="Select Adtype"
                                        required="true"
                                        options={
                                            Global.adTypes.sort( SortAlphabetically( 'text' ) )
                                        }
                                        //onChange={ this.selectCampaign }
                                    />
                                    <Dropdown
                                        wrapClass="sixth-width flex-grow"
                                        inputClass="bold hideSearch showReset"
                                        name="site"
                                        label="Site (optional)"
                                        placeholder="Select Site"
                                        options={
                                            Global.sites.sort( SortAlphabetically( 'text' ) )
                                        }
                                        showReset="true"
                                    />
                                    <Dropdown
                                        wrapClass="sixth-width flex-grow"
                                        inputClass="bold hideSearch showReset"
                                        name="placement"
                                        label="Placement (optional)"
                                        placeholder="Select Placement"
                                        options={
                                            Global.placements.sort( SortAlphabetically( 'text' ) )
                                        }
                                        showReset="true"
                                    />
                                    <FormField
                                        wrapClass="form-item sixth-width flex-grow"
                                        inputClass="outlineInput moveLabelOnFocus forceLabelAbove bold color-blue"
                                        name="project_ooid"
                                        type="text"
                                        label="OO placement ID"
                                        placeholder="Enter OO placement ID"
                                        required="false"
                                        //instructions=""
                                        //onFocus={ this.selectDate }
                                    />
                                </div>

                                <div className="form-actions flex justify-end margin-top-xlarge">
                                    { /*<Button
                                        classes="textOnly margin-right-medium color-dark"
                                        text="Start Over"
                                        onClick={ this.clear }
                                    /> */ }
                                    <Button
                                        loader="true"
                                        classes="primary"
                                        icon="pencil"
                                        text="Create Project"
                                        onClick={ this.create }
                                    />
                                </div>

                            </Form>

                            { this.state.newProjectId &&
                                <div className="form-actions flex justify-end margin-top-large">
                                    <Link to={ '/project/' + this.state.newProjectId }>
                                        <Button
                                            classes="secondary color-dark"
                                            text={ 'Go To ' + this.state.newProjectName }
                                        />
                                    </Link>
                                </div>
                            }

                        </div>
                    </div>
                </div>

                <Overlay>
                    <NewAdvertiser name="advertiser" oncomplete={ this.newAdvertiserComplete } onexists={ this.getCampaigns.bind( this ) } />
                    <NewCampaign name="campaign" advertiser={ this.state.selectedAdvertiser } oncomplete={ this.newCampaignComplete } />
                </Overlay>

            </section>
        );

    }
}

/*
    GET SPECIFIC PROJECT
*/
export class GetProject extends React.Component {
    constructor( props ){
        super( props );
        this.params = this.props.match.params; //gets the variables in the URL String and make a shorter way to access them

        this.state = {
            project: null
        };

        this.processData = this.processData.bind( this );
        this.updateDateRange = this.updateDateRange.bind( this );
        this.downloadData = this.downloadData.bind( this );
        this.editProject = this.editProject.bind( this );
        this.updateProject = this.updateProject.bind( this );
        this.pinProject = this.pinProject.bind( this );
    }

    /*
        custom function to fetch data
        setting component state triggers re-render
    */
    getData( id, startDate, endDate ){
        let dataURL = `${ Global.apiEndpoint }/project/${ id }`;
        if( typeof startDate !== "undefined" && typeof endDate !== "undefined" ){
            dataURL = `${ Global.apiEndpoint }/project/${ id }/${ startDate }/${ endDate }`;
        }

        let user = GetData( 'userId', 'session' );
        if( user !== null ){
            dataURL = dataURL + `?user=${ user }`;
        }
        fetch( dataURL, {
            headers: {
                'apiToken': GetData( 'apiKey', 'session' )
            }
        } )
            .then( ( response ) => {
                return response.text();
            } )
            .then( ( data ) => {
                // console.log( data );
                var dataObj = JSON.parse( data );
                //console.log( dataObj );
                if( typeof dataObj.metaData === 'undefined' ){
                    console.log( 'no project' );
                    alert( 'Project not found.' );
                }else{
                    this.processData( dataObj, startDate, endDate );
                }
            } );
    }

    /*
        Sort non-click tags into appropriate buckets
    */
    viewTagSort( tag, data ){

        let isViewTag = Global.viewTagOptions.find( function( option ){
            if( tag.name.includes( option ) ){
                return true;
            }
            else{
            }
        } );

        if( typeof isViewTag !== "undefined" ){
            //video tag
            if( tag.name.indexOf("video_") >= 0 || Global.videoTagOptions.some( function( option ){return tag.name.indexOf( option ) >= 0; } ) ){
                data.videoTags.push( tag );
            }
            //carousel tag
            else if(tag.name.indexOf("carousel_") >= 0 || Global.carouselTagOptions.some( function( option ){ return tag.name.indexOf( option ) >= 0; } ) ){
                data.carouselTags.push( tag );
            }
            //time tag
            else if( tag.name.includes( "time" ) ){
                data.timeTags.push( tag );
            }
            //other non-interaction view tag
            else{
                data.viewTags.push( tag );
            }
        }else{
            data.interactionTags.push( tag );
        }
    }

    /*
        Process data
    */
    processData( dataObj, startDate = null, lastDay = null ){


        //add project ID to data, for when passing to other components
        dataObj.data.projectId = dataObj.metaData.id;

        //do some data manipulation
        const tags = dataObj.data.tags;
        const data = dataObj.data;

        /*
            OVERALL IMPRESSIONS
        */
        const overallImpressions = GetOverallImpressions( tags, true );

        data.overallImpressionsByDay = overallImpressions.dataByDay;
        data.overallImpressions = overallImpressions.total > 0 ? overallImpressions.total : Global.overallImpressionPlaceholder;


        /*
            GET CLICK EVENTS
            Save tags
            Count up total click on each tag by summing dataByDay
            Count up total clicks for all tags
        */

        //ultimately use type, for now check if redirect is not empty
        /*const clickTags = tags.filter( ( tag ) => {
            return tag.type === "jump";
        } );*/

        const clickTags = tags.filter( ( tag ) => {
            return typeof tag.redirect !== "undefined";
        } );
        //data.clickTags = clickTags.sort( this.sortArray( 'name', 'asc' ) );
        data.clickTags = clickTags.sort( SortAlphabetically( 'name' ) );

        //for each tag, count up total clicks for all days
        for( var i = 0; i < clickTags.length; i++ ){
            clickTags[i].total = clickTags[i].dataByDay.reduce( ( total, day ) => {
                            return total += parseInt( day.total, Global.radix );
                        }, 0 );
        }

        //count up totals
        data.totalClicks = clickTags.reduce( ( total, tag ) => {
                                return total += tag.total;
                             }, 0 );

        if( data.totalClicks === 0 ){
            data.totalClicks = "--";
        }




        /*
            GET INTERACTION EVENTS
            Save tags
            Count up total events on each tag by summing dataByDay
            Count up total interactions for all tags
        */
        let interactionTags = tags.filter( ( tag ) => {
            return typeof tag.redirect === "undefined"; //temp until type works
        } );
        //interactionTags = interactionTags.sort( this.sortArray( 'name', 'asc' ) );
        interactionTags = interactionTags.sort( SortAlphabetically( 'name' ) );

        console.log(interactionTags);

        //temp until TYPE works
        data.interactionTags = [];
        data.viewTags = [];
        data.carouselTags = [];
        data.videoTags = [];
        data.timeTags = [];

        for( var j = 0; j < interactionTags.length; j++ ){

            let tag = interactionTags[j];

            //temp until TYPE works, split data
            /*if( Global.viewTagOptions.indexOf( tag.name ) > -1 ){
                data.viewTags.push( tag );
            }else{
                data.interactionTags.push( tag );
            }*/
            this.viewTagSort( tag, data );
            /*let isViewTag = Global.viewTagOptions.find( function( option ){
                if( tag.name.includes( option ) ){
                    return true;
                }
            } );
            if( typeof isViewTag !== "undefined" ){
                data.viewTags.push( tag );
            }else{
                data.interactionTags.push( tag );
            }*/


            //for each tag, count up total interactions for all days
            tag.total = tag.dataByDay.reduce( ( total, day ) => {
                return total += parseInt( day.total, Global.radix );
            }, 0 );

        }

        console.log(data.videoTags);

        //count up totals
        data.totalInteractions = data.interactionTags.reduce( ( total, tag ) => {
                                   return total += tag.total;
                               }, 0 );

        const engagedUsers = tags.find( x => x.name === 'engagedUser' );
        data.engageUsersByDay = 0;
        data.engagedUsersTotal = 0;

        //if engaged users data exists
        if( typeof engagedUsers !== "undefined" ){
            data.engagedUsersByDay = engagedUsers.dataByDay;
            data.engagedUsersTotal = data.engagedUsersByDay.reduce( ( total, day ) => {
                                       return total += parseInt( day.total, Global.radix );
                                   }, 0 );
        }





        /*
            SPECIFIC EVENTS
            Determine if there are any carousel impressions or video quartiles
            Create a flag to display those modules if they exist
        */


        /*
        When TYPE works use this
        const viewTags = tags.filter( ( tag ) => {
            return tag.type === "view";
        } );
        */

        //carousel
        data.carouselTags = data.viewTags.filter(  tag  => tag.name.includes( "carousel" ) );
        console.log(data.carouselTags);
        //create total for each carousel tag
        if( data.carouselTags.length > 0 ){
            for( var k = 0; k < data.carouselTags.length; k++ ){
                data.carouselTags[k].total = data.carouselTags[k].dataByDay.reduce( ( total, day ) => {
                    return total += parseInt( day.total, Global.radix );
                }, 0 );
            }
        }

        //video
        // data.videoTags = data.viewTags.filter( ( tag ) => {
        //     return tag.name.includes( "video" );
        // } );
        //create total for each video tag
        if( data.videoTags.length > 0 ){
            for( var l = 0; l < data.videoTags.length; l++ ){
                data.videoTags[l].total = data.videoTags[l].dataByDay.reduce( ( total, day ) => {
                    return total += parseInt( day.total, Global.radix );
                }, 0 );
            }
        }

        //time spent
        /*data.timeTags = data.viewTags.filter( ( tag ) => {
            return tag.name.includes( "time" );
        } );*/
        //create total for each video tag
        if( data.timeTags.length > 0 ){
            for( var m = 0; m < data.timeTags.length; m++ ){
                data.timeTags[m].total = data.timeTags[m].dataByDay.reduce( ( total, day ) => {
                    return total += parseInt( day.total, Global.radix );
                }, 0 );
            }
        }
        //sort in order time spent
        const sortOrderTimeSpent = [ 'time05', 'time10', 'time15', 'time20', 'time30', 'time1min', 'time2min', 'time3min', 'time4min', 'time5min' ];
        data.timeTags = data.timeTags.sort( function( a, b ) {
            return sortOrderTimeSpent.indexOf( a.name ) - sortOrderTimeSpent.indexOf( b.name );
        } );



        /*
            CALCULATIONS
            If overall impressions exist make calculations to determine ER IR CTR
        */

        //defaults in case data prevents calculations
        data.CTR = "--";
        data.ER = "--";
        data.IR = "--";

        console.log(data);


        //if overall impression tag exists
        if( data.overallImpressions !== Global.overallImpressionPlaceholder ){
            /*data.overallImpressionsByDay = overallTag.dataByDay;
            data.overallImpressions = data.overallImpressionsByDay.reduce( ( total, day ) => {
                                          return total += parseInt( day.total, Global.radix );
                                      }, 0 );*/

            //if less than threshold
            if( data.overallImpressions < Global.overallImpressionThreshold ){
                console.log( "overall impressions are less than", Global.overallImpressionThreshold );
                data.overallImpressions = Global.overallImpressionPlaceholder;
            }

            //greater than threshold so is Live Creative
            //if no clicks then don't make the calculations
            else if( data.totalClicks !== "--" ){
                /*
                    CLICK EVENTS
                    Calculate CTR
                */
                data.CTR = ( ( data.totalClicks / data.overallImpressions ) * 100 ).toFixed( 2 ) + "%";


                /*
                    INTERACTION EVENTS
                    Calculate ER and IR
                */
                data.ER = ( ( data.engagedUsersTotal / data.overallImpressions ) * 100 ).toFixed( 2 ) + "%";
                data.IR = ( ( data.totalInteractions / dataObj.data.overallImpressions ) * 100 ).toFixed( 2 ) + "%";

            }


            /*
              DATA START & END
              find last day of data set
              Use overall impressions to determine the last time the ad served
            */
            //if start date is from user selected date-range remove the erroneous previous day that shouldn't be part of dataset
            if( startDate !== null ){
                data.overallImpressionsByDay.shift();
            }
            data.firstDay = data.overallImpressionsByDay[ 0 ].date;
            data.lastDay = data.overallImpressionsByDay[ data.overallImpressionsByDay.length - 1 ].date;


            this.setState( {
                metaData: dataObj.metaData,
                data: dataObj.data
            } );

        }

        //some data but no impressions
        else if( data.clickTags.length > 0 || data.interactionTags > 0 ){
            console.log("No Click tag or Interactions tags");

            //loop through tags and find first and last
            data.firstDay = new Date( "2017-01-01" );
            data.lastDay = new Date( "2017-01-01" );

            if( data.clickTags.length > 0 ){
                //for each item in tag group check start and end days in dataByDay array against current days
                for( let t of data.clickTags ){
                    let firstDate = new Date( t.dataByDay[0].date );
                    if( firstDate > data.firstDay ){
                        data.firstDay = firstDate;
                    }
                    let lastDate = new Date( t.dataByDay[ t.dataByDay.length - 1 ].date );
                    if( lastDate > data.lastDay ){
                        data.lastDay = lastDate;
                    }
                }

            }

            this.setState( {
                metaData: dataObj.metaData,
                data: dataObj.data
            } );
        }

        //Just tags not overall impressions
        else if( data.tags.length > 0 ){
            console.log("Only Tags, no impressions");

            //loop through tags and find first and last
            data.firstDay = new Date( "2017-01-01" );
            data.lastDay = new Date( "2017-01-01" );

            if( data.clickTags.length > 0 ){
                //for each item in tag group check start and end days in dataByDay array against current days
                for( let t of data.clickTags ){
                    let firstDate = new Date( t.dataByDay[0].date );
                    if( firstDate > data.firstDay ){
                        data.firstDay = firstDate;
                    }
                    let lastDate = new Date( t.dataByDay[ t.dataByDay.length - 1 ].date );
                    if( lastDate > data.lastDay ){
                        data.lastDay = lastDate;
                    }
                }

            }

            this.setState( {
                metaData: dataObj.metaData,
                data: dataObj.data
            } );
        }


        //no data yet
        else{
            console.log("No Data Yet");
            this.setState( {
                metaData: dataObj.metaData,
                data: null
            } );

        }

        console.log(this.state.data)
    }


    updateDateRange( from, to ){
        console.log( 'update date range', from, to );

        //convert dates to YYYY-MM-DD
        from = moment( new Date( from ) ).format( 'YYYY-MM-DD' );
        to = moment( new Date( to ) ).format( 'YYYY-MM-DD' );

        this.getData( this.state.metaData.id, from, to );

        //close filter
        document.getElementsByClassName( 'filterWrap' )[0].classList.toggle( "show" );
    }


    /*
        Download all data for this project
    */
    downloadData(){
        console.log( 'download data', this.state.data );

        let tags = [];
        for( let t of this.state.data.tags ){
            let obj = {
                type: t.type,
                name: t.name,
                total: t.total,
                redirect: typeof t.redirect === "undefined" ? "" : t.redirect
            };

            //modify specific tags
            if( t.name === "overall" ){
                obj.type = 'Global Data';
                obj.total = this.state.data.overallImpressions;
            } else if( t.name === "engagedUser" ){
                obj.type = 'Global Data';
                obj.total = this.state.data.engagedUsersTotal;
            }

            //skip if total is undefined for this item and not overall or engagedUser
            else{
                if( typeof t.total === "undefined" ){
                    console.log( t );
                    continue;
                }
            }

            //push into array
            tags.push( obj );
        }

        //create new array with items in order by type and alphabetically sorted
        let finalData = [];
        console.log(finalData);


        //add title to data
        function addTitle( array, title ){
            //blank row above
            array.push( {
                name: "",
                total: "",
                redirect: ""
            } );
            //title row
            array.push( {
                name: "title_" + title,
                total: "",
                redirect: ""
            } );
        }

        //global data first
        addTitle( finalData, "Overall Data" );
        let globalData = tags.filter( ( t ) => {
            return t.type === "Global Data";
        } );
        for( let g of globalData ){
            finalData.push( {
                name: g.name,
                total: g.total,
                redirect: ""
            } );
        }

        //clicks
        addTitle( finalData, "Clicks" );
        /*let clickData = tags.filter( ( t ) => {
            return t.type === "jump";
        } ).sort( SortAlphabetically( 'name' ) )*/
        let clickData = this.state.data.clickTags.sort( SortAlphabetically( 'name' ) );
        for( let c of clickData ){
            finalData.push( {
                name: c.name,
                total: c.total,
                redirect: c.redirect
            } );
        }

        //engagements
        addTitle( finalData, "Interactions" );
        /*let engageData = tags.filter( ( t ) => {
            return t.type === "enga";
        } )
        .sort( SortAlphabetically( 'name' ) );*/
        let engageData = this.state.data.interactionTags.sort( SortAlphabetically( 'name' ) );
        for( let e of engageData ){
            finalData.push( {
                name: e.name,
                total: e.total,
                redirect: ""
            } );
        }

        //videoTags
        if(this.state.data.videoTags.length > 0){
            addTitle( finalData, "Video" );
            /*let engageData = tags.filter( ( t ) => {
                return t.type === "enga";
            } )
            .sort( SortAlphabetically( 'name' ) );*/
            let videoData = this.state.data.videoTags.sort( SortAlphabetically( 'name' ) );
            for( let e of videoData ){
                finalData.push( {
                    name: e.name,
                    total: e.total,
                    redirect: ""
                } );
            }
        }

        //carouselTags
        if(this.state.data.carouselTags.length > 0){
            addTitle( finalData, "Carousel" );
            /*let engageData = tags.filter( ( t ) => {
                return t.type === "enga";
            } )
            .sort( SortAlphabetically( 'name' ) );*/
            let carouselData = this.state.data.carouselTags.sort( SortAlphabetically( 'name' ) );
            for( let e of carouselData ){
                finalData.push( {
                    name: e.name,
                    total: e.total,
                    redirect: ""
                } );
            }
        }

        //other data
        addTitle( finalData, "Other Data" );
        /*let viewData = tags.filter( ( t ) => {
            return t.type === "view";
        } )
        .sort( SortAlphabetically( 'name' ) );*/
        let viewData = this.state.data.viewTags.sort( SortAlphabetically( 'name' ) );
        for( let v of viewData ){
            finalData.push( {
                name: v.name,
                total: v.total,
                redirect: ""
            } );
        }

        console.log( "new tags", finalData );

        const exportStatus = ExportData(
            finalData,
            [ 'name', 'total', 'redirect'  ],
            [
                { wch: 40 },
                { wch: 15 },
                { wch: 70 }
            ],
            `project_${ this.state.metaData.id }_all_data.xlsx`,
            function( data ){
                for( let d in data ){
                    let el = data[d];
                    if( typeof el.v === "string" ){
                        if( el.v.includes( 'title_' ) > 0 ){
                            el.v = el.v.replace( 'title_', '' );
                            el.s = {
                                font: {
                                    sz: 16,
                                    bold: true,
                                    color: {
                                        rgb: "FF5A6C8E"
                                    }
                                }
                            };
                        }
                    }

                }

                return data;
            }
        );
        console.log( exportStatus );
    }

    /*
        EDIT PROJECT
        Edit the name or other details for the project
    */
    editProject(){
        console.log( 'edit project' );
        this.editOverlay.show();
    }
    //find current meta data value against list
    getCurrentVal( type ){
        let val = "Select";
        let typeLowercase = type.toLowerCase();
        if( typeof this.state.metaData[ typeLowercase ] !== "undefined" ){
            for( let a of Global[ `${ type }s` ] ){
                if( a.id === `${ typeLowercase }_${ this.state.metaData[ typeLowercase ] }` ){
                    val = a.text;
                }
            }
        }
        //console.log( val );
        return val;
    }

    /*
        UPDATE PROJECT META DATA
        Called from within the edit project form
    */
    updateProject( e ){
        console.log( 'update' );
        e.preventDefault();

        const button = e.target;
        button.classList.remove( "state-error" );
        button.classList.add( "state-loading" );

        const name = document.querySelector( '.form-item[data-content="project-name"] input' ).value;
        const adtype = document.querySelector( '.form-item[data-content="adtype"] select' ).value;
        const site = document.querySelector( '.form-item[data-content="site"] select' ).value;
        const placement = document.querySelector( '.form-item[data-content="placement"] select' ).value;
        const ooid = document.querySelector( '.form-item[data-content="project_ooid"] input' ).value;

        let data = new FormData();

        if( name !== this.state.metaData.name ){
            data.append( 'name', name );
        }
        if( adtype !== "" ){
            data.append( 'adtype', adtype.replace( "adtype_", "" ) );
        }
        if( site !== "" ){
            data.append( 'site', site.replace( "site_", "" ) );
        }
        if( placement !== "" ){
            data.append( 'placement', placement.replace( "placement_", "" ) );
        }
        if( ooid !== "" ){
            data.append( 'ooid', ooid );
            // data.placement = placement;
        }

        fetch( `${ Global.apiEndpoint }/project/${ this.state.metaData.id }/update`, {
            headers: {
                'apiToken': GetData( 'apiKey', 'session' )
            },
            method: "POST",
            body: data
        } )
            .then( ( response ) => {
                return response.text();
            } )
            .then( ( data ) => {
                console.log( data );
                const dataObj = JSON.parse( data );

                if( dataObj.status === "success" ){
                    button.classList.remove( "state-loading" );
                    button.classList.add( "state-success" );

                    setTimeout( function(){
                        button.classList.remove( "state-success" );
                    }, 2000 );

                }

            } );
    }

    /*
        Save project to dashboard
    */
    pinProject( btn ){
        console.log( 'pin/unpin this project' );
        let icon = btn.target;
        icon.classList.toggle( "active" );

        //TO DO - save pin to DB
        fetch( `${ Global.apiEndpoint }/pin/${ this.state.metaData.id }/${ GetData( 'userId', 'session' ) }`, {
            headers: {
                'apiToken': GetData( 'apiKey', 'session' )
            },
            method: "POST"
        } )
            .then( ( response ) => {
                return response.text();
            } )
            .then( ( data ) => {
                console.log( data );
                const dataObj = JSON.parse( data );

                if( dataObj.status === "failed" ){
                    icon.classList.toggle( "active" );
                }

            } );
    }


    /*
        called when new params are passed to the component, like changing between projects
    */
    componentWillReceiveProps( nextProps ){
        console.log( 'receive props', nextProps.match.params.id );

        if( this.state.metaData.id !== nextProps.match.params.id ){
            this.getData( nextProps.match.params.id );
        }
    }


    /*
        called when component first loads, not called for changes
    */
    componentDidMount() {
        console.log( 'first' );
        this.getData( this.params.id );
    }

    render() {

        if( typeof this.state.metaData !== 'undefined' ){

            console.log( this.state );

            let content = "";

            console.log( this.state.metaData );

            //data exists
            if( this.state.data !== null){
                content = (
                    <section className="projectPage">
                        <div className="subnav related">
                            <div className="innerWrap medium flex">
                                <Parents type="project" id={ this.state.metaData.id } currentItem={ this.state.metaData.name } />
                                <Siblings type="project" id={ this.state.metaData.id } />
                            </div>
                        </div>

                        <header className="headerSlantedEdge background-backgroundDark color-onDarkBackground padding-y-xlarge">
                            <div className="innerWrap medium">
                                <MetaHeader
                                    name={ this.state.metaData.name }
                                    start={ this.state.data.firstDay }
                                    end={ this.state.data.lastDay }
                                    projectId={ this.state.metaData.id }
                                    operativeId={ this.state.metaData.campaign.operativeOrderId }
                                    handleDownload={ this.downloadData }
                                    handleEdit={ this.editProject }
                                    showActions="true"
                                    handlePin={ this.pinProject }
                                    pinStatus={ this.state.metaData.pinStatus }
                                    type="project"
                                />
                            </div>

                            <div className="introStats background-backgroundDark color-white margin-top-xlarge">
                                <div className="innerWrap medium flex">
                                    <div className='dataGroup percentage-width-thirty padding-large'>
                                        <div className="dataGroup-labels">
                                            <LargeDataLabel key="impressions" label='Impressions' value={ this.state.data.overallImpressions } textAlign="center" />
                                        </div>
                                    </div>
                                    <div className='dataGroup flex-grow padding-y-large padding-x-xlarge'>
                                        {/*<Graph project={ this.state.metaData.id } data='overall'  />*/}
                                        <div className="dataGroup-labels flex justify-between">
                                            <LargeDataLabel key="clicks" label='Clicks' value={ this.state.data.totalClicks } textAlign="center" />
                                            <LargeDataLabel key="ctr" label={[<span key='1'>CTR<sup>1</sup></span>]} value={ this.state.data.CTR } textAlign="center" />
                                            <LargeDataLabel key="ir" label={[<span key='2'>IR<sup>2</sup></span>]} value={ this.state.data.IR } textAlign="center" />
                                            <LargeDataLabel key="er" label={[<span key='3'>ER<sup>3</sup></span>]} value={ this.state.data.ER } textAlign="center" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </header>

                        <div className="mainContent">
                            <div className="innerWrap medium ">
                                <div className="innerWrap-inner background-background padding-x-xlarge">

                                    { this.state.data.clickTags.length > 0 &&
                                        <ClicksSection
                                            projectId={ this.state.metaData.id }
                                            tags={ this.state.data.clickTags }
                                            total={ this.state.data.totalClicks }
                                            CTR={ this.state.data.CTR }
                                            overallImpressions={ this.state.data.overallImpressions }
                                        />
                                    }
                                    { this.state.data.clickTags.length < 1 &&
                                        <div className="dataSection empty">
                                            <div className="dataHeader flex vertical-center">
                                                <h4 className="cardTitle">Clicks</h4>
                                                <div className="flex flex-align-right">
                                                    <h3 className="sectionTitle">
                                                        <FormatNumber num="0" />
                                                        <span className="xsmall margin-left-xsmall">Clicks</span>
                                                    </h3>
                                                    <h3 className="sectionTitle margin-left-medium">
                                                        0%
                                                        <span className="xsmall margin-left-xsmall">CTR</span>
                                                    </h3>
                                                </div>
                                            </div>
                                            <p className="margin-top-medium color-dark">
                                                No click data exists for this project
                                            </p>

                                        </div>
                                    }


                                    { this.state.data.interactionTags.length > 0 &&
                                        <InteractionsSection
                                            projectId={ this.state.metaData.id }
                                            interactions={ this.state.data.interactionTags }
                                            total={ this.state.data.totalInteractions }
                                            clicks={ this.state.data.clickTags }
                                            ER={ this.state.data.ER }
                                            IR={ this.state.data.IR }
                                            //engagedUsers={ this.state.data.engagedUsersTotal }
                                            engagedUsersByDay={ this.state.data.engagedUsersByDay }
                                            //overallImpressions={ this.state.data.overallImpressions }
                                        />
                                    }

                                    { this.state.data.interactionTags.length < 1 &&
                                        <div className="dataSection empty">
                                            <div className="dataHeader flex vertical-center">
                                                <h4 className="cardTitle">Interactions</h4>
                                                <div className="flex flex-align-right">
                                                    <h3 className="sectionTitle">
                                                        <FormatNumber num="0" />
                                                        <span className="xsmall margin-left-xsmall">Interactions</span>
                                                    </h3>
                                                    <h3 className="sectionTitle margin-left-medium">
                                                        0%
                                                        <span className="xsmall margin-left-xsmall">ER</span>
                                                    </h3>
                                                    <h3 className="sectionTitle margin-left-medium">
                                                        0%
                                                        <span className="xsmall margin-left-xsmall">IR</span>
                                                    </h3>
                                                </div>
                                            </div>
                                            <p className="margin-top-medium color-dark">
                                                No interaction data exists for this project
                                            </p>

                                        </div>
                                    }


                                    { this.state.data.carouselTags.length > 0 &&
                                        <CarouselSection
                                            projectId={ this.state.metaData.id }
                                            tags={ this.state.data.carouselTags }
                                        />
                                    }

                                    { this.state.data.videoTags.length > 0 &&
                                        <VideoSection
                                            projectId={ this.state.metaData.id }
                                            tags={ this.state.data.videoTags }
                                        />
                                    }


                                </div>
                            </div>
                        </div>

                        <Overlay name="datepicker">
                            <DatePicker start={ this.state.data.firstDay } end={ this.state.data.lastDay } updateData={ this.updateDateRange } />
                        </Overlay>
                        <Overlay name="editWrap" ref={ ( overlay ) => { this.editOverlay = overlay; }}>
                            <div className='editProjectWrap innerWrap xsmall-min'>
                                <CardTitle classes="margin-bottom-large">Edit Project</CardTitle>
                                <Form>
                                    <div className="form-row flex">
                                        {/* TO DO - add campaign option
                                        <Dropdown
                                            wrapClass="half-width flex-grow"
                                            inputClass="bold"
                                            name="campaign"
                                            label="Campaign"
                                            placeholder={ this.state.metaData.campaign.name }
                                            //use placeholder since value doesn't seem to work
                                            options={
                                                Global.adTypes.sort( SortAlphabetically( 'text' ) )
                                            }
                                            //onChange={ this.selectCampaign }
                                        />
                                        */}
                                        <FormField
                                            wrapClass="flex-grow"
                                            inputClass="outlineInput moveLabelOnFocus forceLabelAbove bold color-blue"
                                            name="project-name"
                                            type="text"
                                            label="Project Name"
                                            value={ this.state.metaData.name }
                                            required="true"
                                        />
                                    </div>
                                    <div className="form-row flex margin-top-large">
                                        <Dropdown
                                            wrapClass="sixth-width flex-grow"
                                            inputClass="bold"
                                            name="adtype"
                                            label="Adtype"
                                            placeholder={ this.getCurrentVal( 'adType' ) }
                                            //use placeholder since value doesn't seem to work
                                            options={
                                                Global.adTypes.sort( SortAlphabetically( 'text' ) )
                                            }
                                            //onChange={ this.selectCampaign }
                                        />
                                        <Dropdown
                                            wrapClass="sixth-width flex-grow"
                                            inputClass="bold hideSearch showReset"
                                            name="site"
                                            label="Site (optional)"
                                            placeholder={ this.getCurrentVal( 'site' ) }
                                            options={
                                                Global.sites.sort( SortAlphabetically( 'text' ) )
                                            }
                                            //showReset="true"
                                        />
                                        <Dropdown
                                            wrapClass="sixth-width flex-grow"
                                            inputClass="bold hideSearch showReset"
                                            name="placement"
                                            label="Placement (optional)"
                                            placeholder={ /*this.getCurrentVal( 'placement' )*/ 'TO DO' }
                                            options={
                                                Global.placements.sort( SortAlphabetically( 'text' ) )
                                            }
                                            //showReset="true"
                                        />
                                    </div>
                                    <div className="form-actions flex justify-end margin-top-xlarge">
                                        <Button
                                            loader="true"
                                            classes="primary"
                                            icon="pencil"
                                            text="Update Project"
                                            onClick={ this.updateProject }
                                        />
                                    </div>
                                </Form>
                            </div>
                        </Overlay>

                    </section>
                );
            }

            //meta data exists but no data yet
            else{
                content = (
                    <section className="projectPage">
                        <div className="subnav related">
                            <div className="innerWrap medium flex">
                                <Parents type="project" id={ this.state.metaData.id } currentItem={ this.state.metaData.name } />
                                <Siblings type="project" id={ this.state.metaData.id } />
                            </div>
                        </div>

                        <header className="headerSlantedEdge background-backgroundDark color-onDarkBackground padding-y-xlarge">
                            <div className="innerWrap medium">
                                <MetaHeader
                                    name={ this.state.metaData.name }
                                    projectId={ this.state.metaData.id }
                                    operativeId={ this.state.metaData.campaign.operativeOrderId }
                                    timestamp=""
                                    noData="true"
                                    handleEdit={ this.editProject }
                                    handlePin={ this.pinProject }
                                    pinStatus={ this.state.metaData.pinStatus }
                                />
                            </div>

                            <div className="introStats background-backgroundDark color-white margin-top-xlarge">
                                <div className="innerWrap medium flex">
                                    <div className='dataGroup percentage-width-thirty padding-large'>
                                        <div className="dataGroup-labels">
                                            <LargeDataLabel key="impressions" label='Impressions' value="---,---" textAlign="center" />
                                        </div>
                                    </div>
                                    <div className='dataGroup flex-grow padding-y-large padding-x-xlarge'>
                                        <div className="dataGroup-labels flex justify-between">
                                            <LargeDataLabel key="clicks" label='Clicks' value='---' textAlign="center" />
                                            <LargeDataLabel key="ctr" label={[<span key='1'>CTR<sup>1</sup></span>]} value="--%" textAlign="center" />
                                            <LargeDataLabel key="ir" label={[<span key='2'>IR<sup>2</sup></span>]} value="--%" textAlign="center" />
                                            <LargeDataLabel key="er" label={[<span key='3'>IR<sup>3</sup></span>]} value="--%" textAlign="center" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </header>

                        <div className="mainContent">
                            <div className="innerWrap medium ">
                                <div className="innerWrap-inner background-background padding-x-xlarge">


                                </div>
                            </div>
                        </div>

                        <Overlay name="editWrap" ref={ ( overlay ) => { this.editOverlay = overlay; }}>
                            <div className='editProjectWrap innerWrap xsmall-min'>
                                <CardTitle classes="margin-bottom-large">Edit Project</CardTitle>
                                <Form>
                                    <div className="form-row flex">
                                        {/* TO DO - add campaign option
                                        <Dropdown
                                            wrapClass="half-width flex-grow"
                                            inputClass="bold"
                                            name="campaign"
                                            label="Campaign"
                                            placeholder={ this.state.metaData.campaign.name }
                                            //use placeholder since value doesn't seem to work
                                            options={
                                                Global.adTypes.sort( SortAlphabetically( 'text' ) )
                                            }
                                            //onChange={ this.selectCampaign }
                                        />
                                        */}
                                        <FormField
                                            wrapClass="flex-grow"
                                            inputClass="outlineInput moveLabelOnFocus forceLabelAbove bold color-blue"
                                            name="project-name"
                                            type="text"
                                            label="Project Name"
                                            value={ this.state.metaData.name }
                                            required="true"
                                        />
                                    </div>
                                    <div className="form-row flex margin-top-large">
                                        <Dropdown
                                            wrapClass="sixth-width flex-grow"
                                            inputClass="bold"
                                            name="adtype"
                                            label="Adtype"
                                            placeholder={ this.getCurrentVal( 'adType' ) }
                                            //use placeholder since value doesn't seem to work
                                            options={
                                                Global.adTypes.sort( SortAlphabetically( 'text' ) )
                                            }
                                            //onChange={ this.selectCampaign }
                                        />
                                        <Dropdown
                                            wrapClass="sixth-width flex-grow"
                                            inputClass="bold hideSearch showReset"
                                            name="site"
                                            label="Site (optional)"
                                            placeholder={ this.getCurrentVal( 'site' ) }
                                            options={
                                                Global.sites.sort( SortAlphabetically( 'text' ) )
                                            }
                                            //showReset="true"
                                        />
                                        <Dropdown
                                            wrapClass="sixth-width flex-grow"
                                            inputClass="bold hideSearch showReset"
                                            name="placement"
                                            label="Placement (optional)"
                                            placeholder={ /*this.getCurrentVal( 'placement' )*/ 'TO DO' }
                                            options={
                                                Global.placements.sort( SortAlphabetically( 'text' ) )
                                            }
                                            //showReset="true"
                                        />
                                        <FormField
                                            wrapClass="sixth-width flex-grow"
                                            inputClass="outlineInput moveLabelOnFocus forceLabelAbove bold hideSearch"
                                            name="project_ooid"
                                            type="text"
                                            label="OO placement ID"
                                            placeholder="Enter OO placement ID"
                                            value={ this.state.metaData.ooid }
                                            required="false"
                                        />
                                    </div>
                                    <div className="form-actions flex justify-end margin-top-xlarge">
                                        <Button
                                            loader="true"
                                            classes="primary"
                                            icon="pencil"
                                            text="Update Project"
                                            onClick={ this.updateProject }
                                        />
                                    </div>
                                </Form>
                            </div>
                        </Overlay>

                    </section>
                );
            }

            return content;
        }

        //return no-project page
        else{
            return (
                <section className="projectPage">
                    <div className="subnav related">
                        <div className="innerWrap medium flex"></div>
                    </div>

                    <header className="headerSlantedEdge background-backgroundDark color-onDarkBackground padding-y-xlarge">

                        <div className="innerWrap medium">
                            <MetaHeader name="Loading Project" timestamp="" showActions="false" />
                        </div>

                        <div className="introStats background-backgroundDark color-white margin-top-xlarge">
                            <div className="innerWrap medium flex">
                                <div className='dataGroup percentage-width-thirty padding-large'>
                                    <div className="dataGroup-labels">
                                        <LargeDataLabel key="impressions" label='Impressions' value="---,---" textAlign="center" />
                                    </div>
                                </div>
                                <div className='dataGroup flex-grow padding-y-large padding-x-xlarge'>
                                    <div className="dataGroup-labels flex justify-between">
                                        <LargeDataLabel key="clicks" label='Clicks' value='---' textAlign="center" />
                                        <LargeDataLabel key="ctr" label={[<span key='1'>CTR<sup>1</sup></span>]} value="--%" textAlign="center" />
                                        <LargeDataLabel key="ir" label={[<span key='2'>IR<sup>2</sup></span>]} value="--%" textAlign="center" />
                                        <LargeDataLabel key="er" label={[<span key='3'>IR<sup>3</sup></span>]} value="--%" textAlign="center" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </header>

                    <div className="mainContent">
                        <div className="innerWrap medium ">
                            <div className="innerWrap-inner background-background padding-x-xlarge">



                            </div>
                        </div>
                    </div>

                </section>
            );
        }
    }
}
