import React from 'react';
import { Link } from 'react-router-dom';
import { Global } from '../json/global'; //global settings
import { Card } from './Layout';
import { SortAlphabetically, GetSiblings, GetData } from './Helpers'; //global settings
import { Button } from './Buttons';
import { Icon } from './Icons';
import { Form, FormField, Dropdown } from './Inputs';
import '../css/search.css';
import { PageTitle } from './Typography';

/*
    SEARCH
*/
export class Search extends React.Component {
    constructor( props ){
        super( props );

        this.state = {
            searchType: null,
            projectResults: null,
            campaignResults: null,
            keywordResults: null
        };

        //bind this to functions
        this.showProjectSearch = this.showProjectSearch.bind( this );
        this.showCampaignSearch = this.showCampaignSearch.bind( this );
        this.showKeywordSearch = this.showKeywordSearch.bind( this );
        this.startOver = this.startOver.bind( this );
        this.findProject = this.findProject.bind( this );
        this.findCampaign = this.findCampaign.bind( this );
        this.findKeyword = this.findKeyword.bind( this );
    }


    /*
        called when component first loads, not called for changes
    */
    componentDidMount() {

    }

    /*
        Start Over
        Go back to the search options
    */
    startOver(){
        console.log( "start over, back to search options" );
        this.setState( {
            searchType: null
        } );

        document.getElementById('searchOptions').querySelectorAll("button").forEach( function( btn ){
            if( btn.classList.contains( "active" ) ){
                btn.classList.remove( "active" );
            }
        } );
    }


    /*
        Find specific project
    */
    findProject( e ){
        console.log( "find a project" );
        e.preventDefault();

        const form = document.getElementById( "findProject" ).querySelector( "form" );
        const project_id = form.querySelector( "#creative_id" ).value;
        const project_name = form.querySelector( "#project_name" ).value;
        const ad_type = form.querySelector('.form-item[data-content="adType"] select').value;
        const ooid = form.querySelector('#ooid').value;



        /*
            Find by project id
            Check if project exists, if it does navigate to the project page.
            If it does not exist alert user.
        */
        if( project_id.length > 0 ){

            //query DB to see if project exists
            fetch( `${ Global.apiEndpoint }/project/${ project_id }/exists`, {
                headers: {
                    'apiToken': GetData( 'apiKey', 'session' )
                }
            })
                .then( ( response ) => {
                    return response.text();
                } )
                .then( ( data ) => {
                    const dataObj = JSON.parse( data );
                    console.log( dataObj );

                    if( dataObj.status === "exists" ){
                        console.log( 'project exists' );
                        window.location.href = `/project/${ project_id }`;
                    }else{
                        console.log( 'project does not exist' );
                        form.querySelector( "#creative_id" ).classList.add( 'error' );
                        form.querySelector( "[data-content='creative_id'] .inputMessage" ).innerHTML = "A project with this Project ID does not exist.<br/>Check the value you have entered or search by Project Name instead.";
                    }

                } );
        }

        else if(ooid !== ""){
            console.log("Search by ooid");
            fetch( `${ Global.apiEndpoint }/project/search/${ooid}`, {
                headers: {
                    'apiToken': GetData( 'apiKey', 'session' )
                }
            })
                .then( ( response ) => {
                    return response.text();
                } )
                .then( ( data ) => {
                    const dataObj = JSON.parse( data );
                    console.log( dataObj );

                    if( dataObj.status === "exists" ){
                        console.log( 'project exists' );
                        window.location.href = `/project/${ dataObj.id }`;
                    }else{
                        console.log( 'project does not exist' );
                        form.querySelector( "#creative_id" ).classList.add( 'error' );
                        form.querySelector( "[data-content='creative_id'] .inputMessage" ).innerHTML = "A project with this OOID does not exist.<br/>Check the value you have entered or search by Project Name instead.";
                    }

                });
        }

        /*
            Find by project name and or ad-type
            If project_name is not blank collect data, and also allow the ad-type field
            If project_name is blank alert user
        */
        else{
            if( project_name.length > 0 ){
                let data = new FormData();
                data.append( 'project_name', project_name );
                if( ad_type.length > 0 ){
                    data.append( 'ad_type', ad_type.replace( "adtype_", "" ) );
                }

                fetch( `${ Global.apiEndpoint }/project/search`, {
                    headers: {
                        'apiToken': GetData( 'apiKey', 'session' )
                    },
                    method: "POST",
                    body: data
                })
                    .then( ( response ) => {
                        return response.text();
                    } )
                    .then( ( data ) => {
                        const dataObj = JSON.parse( data );
                        console.log( dataObj );

                        this.setState( {
                            projectResults: dataObj
                        } );

                    } );

            }else{
                form.querySelector( "#project_name" ).classList.add( "error" );
                form.querySelector( "[data-content='project_name'] .inputMessage" ).innerHTML = "Please enter a Project Name.<br/>Or search by Project ID instead."
                return false;
            }
        }


    }
    showProjectSearch(){
        this.setState( {
            searchType: 'project',
            campaignResults: null,
            keywordResults: null
        } );
    }

    /*
        Find a campaign
    */
    findCampaign( e ){
        e.preventDefault();
        console.log( "find a campaign" );

        const form = document.getElementById( "findCampaign" ).querySelector( "form" );
        const operative_id = form.querySelector( "#operative_id" ).value;
        const campaign_name = form.querySelector( "#campaign_name" ).value;
        const advertiser_name = form.querySelector( "#advertiser_name" ).value;

        /*
            Find by operative id
            Check if campaign exists, if it does navigate to the campaign
            If it does not exist alert user.
        */
        if( operative_id.length > 0 ){

            //query DB to see if project exists
            fetch( `${ Global.apiEndpoint }/campaign/findByIO/${ operative_id }`, {
                headers: {
                    'apiToken': GetData( 'apiKey', 'session' )
                }
            })
                .then( ( response ) => {
                    return response.text();
                } )
                .then( ( data ) => {
                    const dataObj = JSON.parse( data );
                    console.log( dataObj );

                    if( dataObj.campaign !== null ){
                        window.location.href = `/campaign/${ dataObj.campaign.id }`;
                    }else{
                        console.log( 'campaign does not exist' );
                        form.querySelector( "#operative_id" ).classList.add( 'error' );
                        form.querySelector( "[data-content='operative_id'] .inputMessage" ).innerHTML = "A campaign with this Operative ID does not exist.<br/>Check the value you have entered or search by Campaign Name instead.";
                    }

                } );
        }

        /*
            Find by campaign name
        */
        else if( campaign_name.length > 0 ){

            let data = new FormData();
            data.append( 'campaign_name', campaign_name );

            fetch( `${ Global.apiEndpoint }/campaign/search`, {
                headers: {
                    'apiToken': GetData( 'apiKey', 'session' )
                },
                method: "POST",
                body: data
            })
                .then( ( response ) => {
                    return response.text();
                } )
                .then( ( data ) => {
                    const dataObj = JSON.parse( data );
                    console.log( dataObj );

                    this.setState( {
                        campaignResults: dataObj
                    } );

                } );
        }

        /*
            Find by advertiser name
        */
        else if( advertiser_name.length > 0 ){

            let data = new FormData();
            data.append( 'advertiser_name', advertiser_name );

            fetch( `${ Global.apiEndpoint }/advertisers/search`, {
                headers: {
                    'apiToken': GetData( 'apiKey', 'session' )
                },
                method: "POST",
                body: data
            })
                .then( ( response ) => {
                    return response.text();
                } )
                .then( ( data ) => {
                    const dataObj = JSON.parse( data );
                    console.log( dataObj );

                    this.setState( {
                        campaignResults: dataObj[0].campaigns
                    } );

                } );
        }


    }
    showCampaignSearch(){
        this.setState( {
            searchType: 'campaign',
            projectResults: null,
            keywordResults: null
        } );
    }

    /*
        Find everything by keyword
    */
    findKeyword( e ){
        e.preventDefault();
        console.log( "find by keyword" );

        const form = document.getElementById( "findKeyword" ).querySelector( "form" );
        const keyword = form.querySelector( "#keyword" ).value;
        const ad_type = form.querySelector('.form-item[data-content="adType"] select').value.replace( "adtype_", "" );


        let data = new FormData();
        data.append( 'keyword', keyword );
        data.append( 'ad_type', ad_type );

        fetch( `${ Global.apiEndpoint }/search`, {
            headers: {
                'apiToken': GetData( 'apiKey', 'session' )
            },
            method: "POST",
            body: data
        })
            .then( ( response ) => {
                return response.text();
            } )
            .then( ( data ) => {
                console.log( data );
                const dataObj = JSON.parse( data );
                console.log( dataObj );

                this.setState( {
                    keywordResults: dataObj
                } );

            } );

    }
    showKeywordSearch(){
        this.setState( {
            searchType: 'keyword',
            projectResults: null,
            campaignResults: null
        } );
    }

    render() {

        let results = [];


        if( this.state.searchType !== null ){

            //make button active
            const btn = document.getElementById( this.state.searchType );
            btn.classList.add( "active" );

            //make siblings buttons not active
            const siblings = GetSiblings( btn );
            for( let s of siblings ){
                if( s.classList.contains( "active" ) ){
                    s.classList.remove( "active" );
                }
            }

            //show search item
            const type = this.state.searchType.charAt(0).toUpperCase() + this.state.searchType.slice(1);
            setTimeout( function(){
                document.getElementById( "find" + type ).classList.add( "show" );
            }, 500 );

        }

        if( this.state.projectResults !== null ){

            for( let r of this.state.projectResults ){
                results.push(
                    <SearchResult
                        key={ r.id }
                        data={ r }
                        type='project'
                    />
                );
            }

            setTimeout( function(){

                const resultWrap = document.getElementById( "projectResults" );

                //show results
                resultWrap.classList.add( "show" );

                //scroll down to it
                resultWrap.scrollIntoView( {
                    behavior: 'smooth'
                } );

            }, 500 );
        }

        if( this.state.campaignResults !== null ){

            for( let r of this.state.campaignResults ){
                results.push(
                    <SearchResult
                        key={ r.id }
                        data={ r }
                        type='campaign'
                    />
                );
            }

            setTimeout( function(){

                const resultWrap = document.getElementById( "campaignResults" );

                //show results
                resultWrap.classList.add( "show" );

                //scroll down to it
                resultWrap.scrollIntoView( {
                    behavior: 'smooth'
                } );

            }, 500 );
        }

        if( this.state.keywordResults !== null ){

            for( let a of this.state.keywordResults.advertisers ){
                results.push(
                    <SearchResult
                        key={ a.id }
                        data={ a }
                        type='advertiser'
                    />
                );
            }
            for( let c of this.state.keywordResults.campaigns ){
                results.push(
                    <SearchResult
                        key={ c.id }
                        data={ c }
                        type='campaign'
                    />
                );
            }
            for( let p of this.state.keywordResults.projects ){
                results.push(
                    <SearchResult
                        key={ p.id }
                        data={ p }
                        type='project'
                    />
                );
            }

            setTimeout( function(){

                const resultWrap = document.getElementById( "keywordResults" );

                //show results
                resultWrap.classList.add( "show" );

                //scroll down to it
                resultWrap.scrollIntoView( {
                    behavior: 'smooth'
                } );

            }, 500 );
        }

        return (
            <section className="searchPage">

                <header className="headerSlantedEdge background-backgroundDark color-onDarkBackground padding-y-xlarge text-align-center">
                    <Icon src='search' />
                    <PageTitle classes="color-white">Search</PageTitle>
                </header>

                <div className="mainContent">
                    <div className="innerWrap large">
                        <div className="innerWrap-inner background-background padding-x-xlarge padding-y-xlarge">

                            <div className="sectionIntro">
                                <h3 className="sectionTitle">Search</h3>
                                <p>Use the options below to find a campaign or project.</p>
                            </div>

                            <Card
                                id="searchOptions"
                                classes="searchStep margin-top-large"
                                title="Step 1"
                            >
                                <h4 className="cardTitle">What are you looking for?</h4>
                                <div className="flex margin-top-medium">
                                    <Button
                                        id="project"
                                        classes="primary"
                                        text="Find a Project"
                                        onClick={ this.showProjectSearch }
                                    />
                                    <Button
                                        id="campaign"
                                        classes="primary"
                                        text="Find a Campaign"
                                        onClick={ this.showCampaignSearch }
                                    />
                                    <Button
                                        id="keyword"
                                        classes="primary"
                                        text="Search by Keyword"
                                        onClick={ this.showKeywordSearch }
                                    />
                                </div>
                            </Card>


                            { this.state.searchType === "project" &&
                                <SearchStep
                                    id="findProject"
                                    num="2"
                                    cardTitle="Step 2"
                                    title="Find a Project"
                                    desc="Choose from the options below to find a project."
                                    cta="Find Project"
                                    clickHandler={ this.findProject }
                                    startOverHandler={ this.startOver }
                                >
                                    <FormField
                                        wrapClass="flex-grow margin-right-medium"
                                        inputClass="outlineInput onLightest moveLabelOnFocus bold"
                                        name="creative_id"
                                        type="number"
                                        label="Project ID"
                                        //placeholder="Enter Project ID Number"
                                        instructions='Enter the Project ID Number. If you enter a number here leave the other fields empty'
                                        required="true"
                                    />
                                    <div className="separator separator-or margin-right-medium">
                                        <p>or</p>
                                    </div>
                                    <FormField
                                        wrapClass="flex-grow margin-right-medium"
                                        inputClass="outlineInput onLightest moveLabelOnFocus bold"
                                        name="ooid"
                                        type="text"
                                        label="OOID"
                                        //placeholder="Enter Project ID Number"
                                        instructions='Enter the OOID. If you enter a number here leave the other fields empty'
                                        required="true"
                                    />
                                    <div className="separator separator-or margin-right-medium">
                                        <p>or</p>
                                    </div>
                                    <FormField
                                        wrapClass="flex-grow margin-right-medium"
                                        inputClass="outlineInput onLightest moveLabelOnFocus bold"
                                        name="project_name"
                                        type="text"
                                        label="Project Name"
                                        //placeholder="Enter Project Name"
                                        required="true"
                                        //instructions=""
                                        //onFocus={ this.selectDate }
                                    />
                                    <Dropdown
                                        wrapClass="flex-grow"
                                        inputClass="bold onLightest"
                                        name="adType"
                                        label="Ad Type (optional)"
                                        placeholder="Select Ad Type"
                                        //multiple="false"
                                        instructions="If you search by Project Name you can apply an ad-type filter to find projects matching both name and ad-type"
                                        //disabled={ true }
                                        //api={ Global.apiEndpoint + '/advertisers' }
                                        //onChange={ this.onChange }
                                        options={
                                            Global.adTypes.sort( SortAlphabetically( 'text' ) )
                                        }
                                    />
                                </SearchStep>
                            }
                            { this.state.projectResults !== null &&
                                <SearchStep
                                    id="projectResults"
                                    num="3"
                                    cardTitle="Step 3"
                                    title="Project Search Results"
                                    desc="Here are the results of your search. Click on a project to view details."
                                    cta={ null }
                                    clickHandler={ null }
                                    startOverHandler={ null }
                                >
                                    { results }
                                </SearchStep>
                            }


                            { this.state.searchType === "campaign" &&
                                <SearchStep
                                    id="findCampaign"
                                    num="2"
                                    cardTitle="Step 2"
                                    title="Find a Campaign"
                                    desc="Choose from the options below to find a campaign and the associated projects."
                                    cta="Find Campaigns"
                                    clickHandler={ this.findCampaign }
                                    startOverHandler={ this.startOver }
                                >
                                    <FormField
                                        wrapClass="flex-grow margin-right-medium"
                                        inputClass="outlineInput onLightest moveLabelOnFocus bold"
                                        name="operative_id"
                                        type="number"
                                        label="Operative ID"
                                        //placeholder="Enter Operative ID Number"
                                        instructions='Enter Operative ID Number to find the campaign it is associated with and the corresponding projects.'
                                        required="true"
                                    />
                                    <FormField
                                        wrapClass="flex-grow margin-right-medium"
                                        inputClass="outlineInput onLightest moveLabelOnFocus bold"
                                        name="campaign_name"
                                        type="text"
                                        label="Campaign Name"
                                        //placeholder="Enter Campaign Name"
                                        required="true"
                                        //instructions=""
                                        //onFocus={ this.selectDate }
                                    />
                                    <FormField
                                        wrapClass="flex-grow"
                                        inputClass="outlineInput onLightest moveLabelOnFocus bold"
                                        name="advertiser_name"
                                        type="text"
                                        label="Advertiser Name"
                                        //placeholder="Enter Campaign Name"
                                        required="true"
                                        instructions="Enter Advertiser Name to find all campaigns associated with the advertiser."
                                        //onFocus={ this.selectDate }
                                    />
                                </SearchStep>
                            }
                            { this.state.campaignResults !== null &&
                                <SearchStep
                                    id="campaignResults"
                                    num="3"
                                    cardTitle="Step 3"
                                    title="Campaign Search Results"
                                    desc="Here are the results of your search. Click on a campaign to view details."
                                    cta={ null }
                                    clickHandler={ null }
                                    startOverHandler={ null }
                                >
                                    { results }
                                </SearchStep>
                            }


                            { this.state.searchType === "keyword" &&
                                <SearchStep
                                    id="findKeyword"
                                    num="2"
                                    cardTitle="Step 2"
                                    title="Search by Keyword"
                                    desc="Search for projects, campaigns, and advertisers by keyword."
                                    cta="Search Now"
                                    clickHandler={ this.findKeyword }
                                    startOverHandler={ this.startOver }
                                >
                                    <FormField
                                        wrapClass="flex-grow twoThird-width"
                                        inputClass="outlineInput onLightest moveLabelOnFocus bold"
                                        name="keyword"
                                        type="text"
                                        label="Keyword(s)"
                                        //placeholder="Enter Operative ID Number"
                                        instructions='Enter the words you want to search by.'
                                        required="true"
                                    />
                                    <Dropdown
                                        wrapClass="third-width"
                                        inputClass="bold onLightest"
                                        name="adType"
                                        label="Ad Type (optional)"
                                        placeholder="Select Ad Type"
                                        //multiple="false"
                                        instructions="Add a ad-type filter to your keyword search to view only the projects matching the keyword and ad-type"
                                        //disabled={ true }
                                        //api={ Global.apiEndpoint + '/advertisers' }
                                        //onChange={ this.onChange }
                                        options={
                                            Global.adTypes.sort( SortAlphabetically( 'text' ) )
                                        }
                                    />
                                </SearchStep>
                            }

                            { this.state.keywordResults !== null &&
                                <SearchStep
                                    id="keywordResults"
                                    num="3"
                                    cardTitle="Step 3"
                                    title="Keyword Search Results"
                                    desc="Here are the results of your search. Click an item to view details."
                                    cta={ null }
                                    clickHandler={ null }
                                    startOverHandler={ null }
                                >
                                    { results }
                                </SearchStep>
                            }


                        </div>
                    </div>
                </div>

            </section>
        );
    }
}


/*
    Search Step
    Builds the content for a specific method of search
*/
class SearchStep extends React.Component {

    render() {
        return (
            <Card
                id={ this.props.id }
                classes={ `searchStep step${ this.props.num } margin-top-large` }
                title={ this.props.cardTitle }
            >
                <h4 className="cardTitle">{ this.props.title }</h4>
                <p>{ this.props.desc }</p>
                <div className="flex margin-top-medium">
                    { this.props.num < 3 &&
                        <Form classes="flex-grow">
                            <div className="form-row flex margin-top-large">
                                { this.props.children }
                            </div>
                            <div className="form-actions flex justify-end margin-top-xlarge">
                                { this.props.startOverHandler !== null &&
                                    <Button
                                        classes="textOnly textOnly-small margin-right-medium color-dark"
                                        icon="trash"
                                        text="Start Over"
                                        onClick={ this.props.startOverHandler }
                                        type="reset"
                                    />
                                }
                                { this.props.cta !== null &&
                                    <Button
                                        loader="true"
                                        classes="primary submit"
                                        icon="search"
                                        text={ this.props.cta }
                                        onClick={ this.props.clickHandler }
                                        type="submit"
                                    />
                                }
                            </div>

                        </Form>
                    }
                    { this.props.num === "3" &&
                        <div className="form-row flex margin-top-large">
                            { this.props.children }
                        </div>
                    }
                </div>
            </Card>
        );
    }
}


/*
    Search Result
    Builds an individual search result
*/
class SearchResult extends React.Component {

    render() {

        let data = this.props.data;

        let createdAt = null;
        if( data.created_at !== null ){
            createdAt = new Date( data.created_at );
            createdAt = new Date( createdAt.setHours( createdAt.getHours() + 4 ) ).toDateString();
        }

        return (
            <div className='advertiser-group searchResult' data-id={ data.id }>
                <p className="advertiser-group-letter">{ data.id }</p>
                <Link to={ `/${ this.props.type }/${ data.id }` } target="_blank">
                    <div className="result-name">{ data.name }</div>
                    <div className="campaign-metaData margin-top-small">
                        <p>
                            { data.advertiserName }
                            { typeof data.campaignName !== "undefined" && <span> - </span> }
                            { data.campaignName }
                        </p>
                        <p>Created: { createdAt }</p>
                    </div>
                    <Button
                        classes="secondary gray margin-top-large"
                        text={ `Go To ${ this.props.type }` }
                    />
                </Link>
            </div>
        );
    }
}
