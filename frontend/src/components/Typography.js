import React from 'react';

export class PageTitle extends React.Component {
    render(){
        return (
            <h2 className={ `sectionTitleLarge ${ this.props.classes }` }>{ this.props.children }</h2>
        );
    }
}

export class SectionTitle extends React.Component {
    render(){
        return (
            <h3 className={ `sectionTitle ${ this.props.classes }` }>{ this.props.children }</h3>
        );
    }
}

export class CardTitle extends React.Component {
    render(){
        return (
            <h4 className={ `cardTitle ${ this.props.classes }` }>{ this.props.children }</h4>
        );
    }
}
