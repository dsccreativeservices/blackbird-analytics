import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { useOktaAuth } from '@okta/okta-react';
import { Global } from '../json/global'; // global settings
import { GetData, FormatDate, StoreData } from './Helpers';
import { Button } from './Buttons';
import { PageTitle, SectionTitle, CardTitle } from './Typography';
import { Icon } from './Icons';
import { LargeDataLabel } from './DataLabels';
import '../css/home.css';
import logo from '../img/logo.svg';

/* the App class is the main wrapper for the app */
const Home = () => (
  <section className="homePage">

    <div className="mainContent">
      <div className="innerWrap small padding-x-xlarge padding-y-xxlarge">

        <img
          className="logo"
          src={logo}
          alt="Blackbird Ad Tracking Analytics"
        />

        <div className="margin-top-xlarge">
          <p className="color-onDarkBackground margin-bottom-small">Please login to continue</p>
          <Link to="/dashboard">
            <Button
              classes="primary"
              icon="lock"
              text="Log In"
            />
          </Link>
        </div>

      </div>
    </div>

  </section>

);

const Dashboard = () => {
  const { oktaAuth, authState } = useOktaAuth();
  const [userInfo, setUserInfo] = useState(null);
  const [pinsLoaded, setPinsLoaded] = useState(false);
  const [pinnedProjects, setPinnedProjects] = useState(null);

  // useEffect(() => {
  //   console.log(authService);
  //   console.log(authState);
  //   if (typeof authService === 'undefined') return;
  //   if (!authState.isAuthenticated) {
  //     // When user isn't authenticated, forget any user info
  //     setUserInfo(null);
  //   } else {
  //     authService.getUser().then((info) => {
  //       setUserInfo(info);
  //     });
  //   }
  // }, [authState, authService]); // Update if authState changes

  useEffect(() => {
    console.log(oktaAuth);
    console.log(authState);
  }, [oktaAuth, authState]);

  /*
        Find all pins for user
    */
  const findPins = () => {
    const userId = GetData('userId', 'session');
    if (userId !== null) {
      console.log('find pins');
      fetch(`${Global.apiEndpoint}/user/${userId}/pins`, {
        headers: {
          apiToken: GetData('apiKey', 'session'),
        },
      })
        .then((response) => response.text())
        .then((data) => {
          // console.log( data );
          const dataObj = JSON.parse(data);
          console.log(dataObj);
          if (dataObj.status === 'success') {
            setPinsLoaded(true);
            setPinnedProjects(dataObj.pins);
          }
          if (dataObj.status === 'no pins') {
            setPinsLoaded(true);
            setPinnedProjects('none');
          }
        });
    }
  };

  /*
        Unpin item
    */
  const removePin = (id) => {
    console.log('remove pin with id', id);

    // TO DO - remove pin from DB and visually
  };

  useEffect(() => {
    findPins();
    fetch(`${Global.apiEndpoint}/advertisers`, {
      headers: {
        apiToken: GetData('apiKey', 'session'),
      },
    })
      .then((response) => response.text())
      .then((data) => {
        const updatedData = JSON.parse(data);
        StoreData('advertiserList', updatedData, 'session');
        console.log('Refreshed the data from the advertisers');
      });
  }, []);

  let pins = null;
  if (pinnedProjects !== null && pinnedProjects !== 'none') {
    pins = pinnedProjects.map((pin) => (
      <Pin
        key={pin.id}
        data={pin}
        removePin={removePin}
      />
    ));
  }

  return (
    <section className="dashboardPage">

      <header className="color-onDarkBackground padding-y-xlarge text-align-center">
        <Icon src="clipboard" />
        <PageTitle classes="color-white">Dashboard</PageTitle>
      </header>

      <div className="mainContent">
        <div className="innerWrap padding-x-xlarge">

          { pinnedProjects !== null
            && (
            <div className="pinnedProjects">
              <SectionTitle classes="color-white">
                <Icon src="heart" />
                Your Saved Projects
              </SectionTitle>
              <div className="grid threeColumn spacing-xlarge margin-top-large">
                { pins }
              </div>

            </div>
            )}
          { pinnedProjects === 'none'
            && <p className="color-onDarkBackground">You can save items here by clicking the heart icon in the top right of a project page</p>}
          { pinsLoaded === false
            && <p className="color-onDarkBackground">Loading Saved Items</p>}

        </div>
      </div>

    </section>

  );
};

class Pin extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      actionsOpen: false,
    };

    this.toggleActions = this.toggleActions.bind(this);
  }

  getAdtype(id) {
    let val = null;
    for (const a of Global.adTypes) {
      if (a.id === `adtype_${id}`) {
        val = a.text;
      }
    }
    return val;
  }

  toggleActions(state) {
    this.setState({
      actionsOpen: state,
    });
  }

  render() {
    const { project } = this.props.data;
    let impressions = parseInt(project.impressions.count, Global.radix);
    let CTR = '--';
    if (isNaN(impressions)) {
      impressions = '--';
    } else {
      CTR = ((parseInt(project.clicks.count, Global.radix) / impressions) * 100).toFixed(2);
    }
    return (
      <div className="pinWrap color-onDarkBackground padding-large">
        <div className="pin-actions">
          <Button
            classes="iconOnly"
            icon="actionDots"
            onClick={this.toggleActions.bind(this, true)}
          />
          { this.state.actionsOpen === true
                        && (
                        <div className="pin-actions-open background-background padding-large">
                          <Button
                            classes="textOnly button-close"
                            text="+"
                            onClick={this.toggleActions.bind(this, false)}
                          />
                          <Link to={`/project/${project.id}`}>
                            <Button
                              classes="secondary gray project-button"
                              text="Go To Project"
                            />
                          </Link>
                          <Button
                            classes="secondary gray remove-button margin-top-small"
                            text="Remove"
                            onClick={this.props.removePin.bind(this, this.props.data.id)}
                          />
                        </div>
                        )}
        </div>
        <div className="pin-parents">
          <p />
        </div>
        <CardTitle classes="color-white margin-top-small text-align-center dash-below dash-center dash-onDark">
          { project.name }
        </CardTitle>
        <div className="pin-metrics color-blue flex justify-center margin-top-large">
          <LargeDataLabel
            key="impressions"
            label="Impressions"
            value={impressions}
            textAlign="center"
          />
          <div className="color-pink margin-left-large">
            <LargeDataLabel
              key="ctr"
              label={[<span key="1">CTR</span>]}
              value={`${CTR}%`}
              textAlign="center"
            />
          </div>
        </div>
        <div className="pin-data flex justify-center margin-top-large">
          <p className="pin-data-item">
            Created:
            {' '}
            { project.created_at }
          </p>
          <p className="pin-data-item">
            Adtype:
            {' '}
            { this.getAdtype(project.adtype) }
          </p>
        </div>

      </div>
    );
  }
}

export { Home, Dashboard };
