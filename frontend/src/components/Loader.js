import React from 'react';

/*
    CREATE NEW PROJECT
*/
export class Loader extends React.Component {
    render() {
        return (
            <div className="loaderWrap">
                <div className="loader">
                    <div className="shadow"></div>
                    <div className="box"></div>
                </div>
            </div>
        );
    }
}
