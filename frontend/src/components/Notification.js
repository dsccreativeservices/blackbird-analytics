import React from 'react';
import { Global } from '../json/global'; //global settings

export function showNotification(title, message, type){
    let notification = document.querySelector(".notification");
    notification.classList.add("active", type);
    notification.querySelector(".notificationTitle").innerHTML = title;
    notification.querySelector(".notificationMessage").innerHTML = message;
    setTimeout(function () {
        notification.classList.remove("active", type);
        notification.querySelector(".notificationTitle").innerHTML = "";
        notification.querySelector(".notificationMessage").innerHTML = "";
    }, 5000)
}

export class Notification extends React.Component{
    constructor( props ){
        super( props );
        console.log("Inside Notification");
        console.log(props);
    }

    render() {
        return(
            <div className="notification">
                <div className="notificationTitle">TITle</div>
                <div className="notificationMessage">MESSAGE</div>
            </div>


        )
    }
}