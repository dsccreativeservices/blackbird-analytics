import React from 'react';
import { SubmenuItem } from './Submenu';
import { GetData } from './Helpers';
import { Global } from '../json/global'; //global settings

/*
    PROJECT PARENTS
    Return advertiser / campaign / project structure
*/
export class Parents extends React.Component {
    constructor( props ){
        super( props );

        this.state = {
            parents: [],
            loading: true
        };
    }

    componentDidMount() {
        fetch( `${ Global.apiEndpoint }/${this.props.type}/${this.props.id}/parents`, {
            headers: {
                'apiToken': GetData( 'apiKey', 'session' )
            }
        })
            .then( ( response ) => {
                return response.text();
            } )
            .then( ( data ) => {
                console.log(data);
                if( data === "project does not exist" ){
                    this.setState( { parents: data } );
                }else{
                    this.setState( { parents: JSON.parse( data ) } );
                }
            } );
    }

    render() {

        //parents returns array, unless project is not found.
        //Don't render submenu is project is not found
        let submenu = "";
        if( this.state.parents.constructor === Array ){
            submenu = this.state.parents.map( ( item ) =>
                <SubmenuItem key={ item.type } className="parents-item" type={ item.type } name={ item.name } url={ '/' + item.type + '/' + item.id } />
            );
        }

        return (
            <div className="parents">
                <ul className="parents-list flex">
                    { submenu }
                    { /* current item */ }
                    <li className="parents-item" data-type={ this.props.type }>
                        { this.props.currentItem }
                    </li>
                </ul>
            </div>
        );
    }
}
