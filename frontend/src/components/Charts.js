import React from 'react';
import * as d3 from 'd3';
import { Global } from '../json/global'; //global settings



export class BarChart extends React.Component {
    constructor( props ){
        super( props );
        this.createChart = this.createChart.bind( this );
    }

    componentDidMount() {
       this.createChart();
    }
    componentDidUpdate() {
       this.createChart();
    }

    createChart() {
        const node = this.node;
        //const numItems = this.props.data.length;
        const barSpacing = 10;
        const barWidth = ( this.props.size[0] / this.props.data.length ) - barSpacing;
        const dataMax = d3.max( this.props.data );
        const yScale = d3.scaleLinear()
                        .domain( [ 0, dataMax ] )
                        .range( [ 0, this.props.size[1] ] );

        d3.select( node )
            .selectAll( 'rect' )
            .data( this.props.data )
            .enter()
            .append( 'rect' );

        d3.select( node )
            .selectAll( 'rect' )
            .data( this.props.data )
            .exit()
            .remove();

        d3.select( node )
            .selectAll( 'rect' )
            .data( this.props.data )
            .style( 'fill', '#fe9922' )
            .attr( 'x', ( d, i ) => ( ( i * barWidth ) + ( i * barSpacing ) ) )
            .attr( 'y', d => this.props.size[1] - yScale( d ) )
            .attr( 'height', d => yScale( d ) )
            .attr( 'width', barWidth );


    }

    render() {

        //show header prop allows hiding of the date filtering option
        return (
            <div className="chart barChart">
                <svg
                    ref={ node => this.node = node }
                    width={ this.props.size[0] } height={ this.props.size[1] }>
                </svg>
            </div>
        );
    }
}




export class LineChart extends React.Component {
    constructor( props ){
        super( props );
        this.createChart = this.createChart.bind( this );
        this.height = this.props.size[1] - 60;
        this.width = this.props.size[0];
    }

    componentDidMount(){
        this.createChart();
    }
    componentDidUpdate(){
        this.createChart();
    }

    /*
        Return the smallest and largest values
    */
    getRange( data ){
        console.log("in getRange");

        //get lowest value
        const lowestValues = data.map( ( obj, index ) => {
            return parseInt( Object.values( obj ).sort( ( prev, next ) => prev - next )[0], Global.radix  );
        } );

        //if we want to use non-0 value as lowest value
        const minY = lowestValues.splice(
            lowestValues.indexOf( Math.min.apply( null, lowestValues ) ),
            1
        );

        //get highest value
        const highestValues = data.map( ( obj, index ) => {
            const valuesInOrder = Object.values( obj ).sort( ( prev, next ) => prev - next );
            return parseInt( valuesInOrder[ valuesInOrder.length - 2 ], Global.radix  );
        } );
        const maxY = highestValues.splice(
            highestValues.indexOf( Math.max.apply( null, highestValues ) ),
            1
        );

        console.log(data);

        return {
            minX: data[0].date,
            maxX: data[ data.length - 1 ].date,
            //minY: minY,
            minY: minY,
            maxY: maxY
        };
    }

    /*
        Build the Chart
    */
    createChart(){

        //console.log( this.props.data );
        console.log(this.props.data);

        const data = this.props.data,
            node = this.node,
            //node = this.refs[ this.props.name ],
            margin = {
                top: 20,
                right: 0,
                bottom: 20,
                left: 33
            },
            width = this.width - margin.left - margin.right,
            height = this.height - margin.top - margin.bottom;

        //for legend
        //let offset = 0;
        //const spacing = 8;
        const legendRightSpacing = 20;

        //convert date into d3 time
        let parseDate = d3.timeParse( "%Y-%m-%d" );

        //create scale
        let x = d3.scaleTime().range( [0, width ] ),
            y = d3.scaleLinear().range( [ height, 0] ),
            color = d3.scaleOrdinal( Global.colorArray );

        //build area if fill is needed
        const area = d3.area()
            .curve( d3.curveBasis )
            .x( function( d ){
                return x( d.date );
            } )
            .y0( height )
            .y1( function( d ){
                return y( d.total );
            } );


        //build a line
        const line = d3.line()
            .curve( d3.curveBasis )
            .x( function( d ){
                return x( d.date );
            } )
            .y( function( d ){
                return y( d.total );
            } );

        // gridlines in x axis function
        /*function make_x_gridlines(){
            return d3.axisBottom( x )
                .ticks( 5 );
        }

        // gridlines in y axis function
        function make_y_gridlines(){
            return d3.axisLeft( y )
                .ticks( 5 );
        }*/

        //determine colors
        color.domain(
            d3.keys( data[0] ).filter( function( key ) {
                return key !== "date";
            } )
        );

        //parse dates
        data.forEach( function( d ){
            d.date = parseDate( d.date );
        } );

        let items = color.domain().map( function( name ){
            return {
                name: name,
                values: data.map( function( d ){
                    return {
                        date: d.date,
                        total: +d[name]
                    };
                } )
            };
        } );

        console.log(data);
        const range = this.getRange( data );

        x.domain( [ range.minX, range.maxX ] );
        y.domain( [ range.minY, range.maxY ] );


        //kill current chart, if any
        d3.select( node ).selectAll( "*" ).remove();

        //create outer group
        d3.select( node ).append( "g" );
        const g = d3.select( node ).select( 'g' );

        //move graph group
        g.attr( "transform", "translate(" + margin.left + "," + margin.top + ")" );

        //append x axis
        g.append( "g" )
            .attr( "class", "axis axis-x" )
            .attr( "transform", "translate(0," + height + ")" )
            .call( d3.axisBottom( x ).ticks( 10 ).tickSize( -height ).tickPadding( 10 ) );

        //append y axis and label
        g.append( "g" )
            .attr( "class", "axis axis-y" )
            .call( d3.axisLeft( y ).ticks( 10 ).tickSize( -width ).tickPadding( 6 ) );

            /*
            axis label text
            .append( "text" )
            .attr( "transform", "rotate(-90)" )
            .attr( "y", 6 )
            .attr( "dy", "0.71em" )
            .attr( "fill", "#000" )
            .text( "value, ºF" );*/


        let item = g.selectAll( ".item" )
            .data( items )
            .enter().append( "g" )
            .attr( "class", "lineWrap" );

        //if type is filledLine
        if( this.props.graphStyle === 'filledLines' ){
            item.append( "path" )
               .attr( "class", "area" )
               .style( "fill", function( d ){
                   return color( d.name );
               } )
               .attr( "d", function( d ){
                   return area( d.values );
               } );
        }

        if( this.props.graphStyle === "lines" ){
            item.append( "path" )
                .attr( "class", "line" )
                //apply styles here so they'll be part of JPG export
                .attr( "fill", "none" )
                .attr( "stroke-width", '3px' )
                .style( "stroke", function( d ){
                    return color( d.name );
                } )
                //end styles
                .attr( "d", function( d ){
                    return line( d.values );
                } )
                .attr( "data-tagname", function( d ){ return d.name; } ); //just for debugging
        }



        /* if you want to add text and the end of a line */
        /*item.append( "text" )
            .datum( function( d ) {
                return {
                    name: d.name,
                    value: d.values[d.values.length - 1]
                };
            } )
            .attr( "transform", function( d ) {
                return "translate(" + x( d.value.date ) + "," + y( d.value.total ) + ")";
            } )
            .attr( "x", 3 )
            .attr( "dy", ".35em" )
            .text( function( d ) {
                return d.name;
            } );*/






        //build legend
        let legend = g.append( "g" )
            .attr( "class", "legend" )
            .attr( "text-anchor", "end" ); //needed if vertically stacked legend

        //add items to legend
        legend.selectAll( ".item" )
            .data( items )
            .enter().append( "g" )
            .attr( "class", "legendItem" )
            .each( function( key, i ){
                let item = d3.select( this );

                let box = item.append( 'rect' );

                let text = item.append( 'text' )
                    .text( function( d ) {
                        return d.name;
                    } )
                    //apply styles here so they'll be part of JPG export
                    .style( "font-size", "0.65em" )
                    .style( "font-weight", "bold" )
                    .style( "fill", function( d ){
                        return color( d.name );
                    } );
                    //END styles

                let textW = text.node().getBBox().width,
                    textH = text.node().getBBox().height;

                //update box sizing and position based on text
                box
                    .attr( "width", textW + 20 )
                    .attr( "height", textH + 5 )
                    .attr( "transform", "translate(-" + ( textW + 10 ) + ", -" + textH + " )" )
                    .style( "fill", "white" );


                //horizontal legend
                // Translate the group based on the running width
                /*item.attr( 'transform', function(){
                    return 'translate(' + offset + ', 0)';
                } );*/

                // Update the offset - need for horizontal legend
                //offset += textW + spacing;

                //vertical legend
                item.attr( 'transform', 'translate( 0, ' + i * 15 + ' )' );

            } );

        //transform legend now that we know how large it will be
        //if horizontal legend
        //legend.attr( "transform", "translate(" + ( width - legend.node().getBBox().width - legendRightSpacing ) + ", -10)" );

        //if vertical
        legend.attr( "transform", "translate(" + ( width - legendRightSpacing ) + ", -10)" );





    }

    render() {
        return (
            <div className="chart lineChart">
                <svg xmlns="http://www.w3.org/2000/svg"
                    id={ this.props.name }
                    ref={ node => this.node = node }
                    width={ this.props.size[0] } height={ this.height }>
                </svg>
            </div>
        );
    }
}
