import React from 'react';

/*
    CREATE NEW PROJECT
*/
export class Icon extends React.Component {
    render() {
        return (
            <svg className="icon">
                <use xlinkHref={"/img/icons/master.svg#" + this.props.src } />
            </svg>
        );
    }
}
