import React from 'react';
import { Global } from '../json/global'; //global settings
import { Button } from  './Buttons';
import { /*BarChart,*/ LineChart } from './Charts';

/*
    GRAPH BUILDER
    Gets data and returns a graph
*/
export class Graph extends React.Component {
    constructor( props ){
        super( props );
        this.data = this.props.data;
        this.state = {
            currentDataSet: 'all'
        };
        this.handleDataSetChange = this.handleDataSetChange.bind( this ); //so we can access "this" inside function
        this.handleSaveJPG = this.handleSaveJPG.bind( this );
    }

    handleSaveJPG() {
        console.log( 'save JPG' );

        //const svgNode = document.getElementById( this.props.id );
        const svgNode = document.querySelectorAll( '#' + this.props.id + " svg" )[0];

        //convert SVG into string
        let source = ( new XMLSerializer() ).serializeToString( svgNode );
        const canvas = this.refs.canvas;
        const ctx = canvas.getContext( '2d' );
        const DOMURL = window.URL || window.webkitURL || window;



        //clear canvas
        ctx.clearRect( 0, 0, canvas.width, canvas.height );

        const img = new Image();
        const svg = new Blob( [ source ], { type: 'image/svg+xml' } );
        const url = DOMURL.createObjectURL( svg );
        const link = this.refs.downloadBtn;

        img.onload = function() {
            ctx.drawImage( img, 0, 0 );
            DOMURL.revokeObjectURL( url );
            //build link and download
            link.href = canvas.toDataURL();
            link.click();
        };
        img.src = url;



    }

    handleDataSetChange( dataRange ){
        console.log( 'update data to show: ', dataRange, 'days' );
        this.setState( {
            currentDataSet: dataRange
        } );
    }

    render() {

        //show header prop allows hiding of the date filtering option
        if( typeof this.props.data !== 'undefined' && this.props.data.length > 2 ){
            return (
                <div className="graphWrap">
                    { this.props.showHeader &&
                    <GraphHeader clickListener={ this.handleDataSetChange } dataSet={ this.state.currentDataSet } save={ this.handleSaveJPG } />
                    }
                    <GraphBody id={ this.props.id } data={ this.props.data } dataSet={ this.state.currentDataSet } graphStyle={ this.props.graphStyle } />
                    <div className="canvasForSavingImage">
                        <canvas ref="canvas" width={1046} height={448}></canvas>
                        <a href='' ref='downloadBtn' download="download">Download</a>
                    </div>
                </div>
            );
        }else{
            return (
                <div className="emptyTable tableWrap">
                    <div>No Data</div>
                </div>
            );
        }
    }
}
class GraphHeader extends React.Component {

    /* if data set will change, then pass change event up to parent */
    onClick( val ){
        if( this.props.dataSet !== val ){
            this.props.clickListener( val );
        }
    }

    render() {

        return (
            <div className="graphHeader flex">
                <Button
                    classes={"small gray " + ( this.props.dataSet === "first7" ? 'active' : '' )}
                    text="First 7 Days"
                    onClick={ this.onClick.bind( this, 'first7' ) }
                />
                <Button
                    classes={"small gray " + ( this.props.dataSet === "last7" ? 'active' : '' )}
                    text="Last 7 Days"
                    onClick={ this.onClick.bind( this, 'last7' ) }
                />
                <Button
                    classes={"small gray margin-left-medium " + ( this.props.dataSet === "first30" ? 'active' : '' )}
                    text="First 30 Days"
                    onClick={ this.onClick.bind( this, 'first30' ) }
                />
                <Button
                    classes={"small gray margin-left-medium " + ( this.props.dataSet === "last30" ? 'active' : '' )}
                    text="Last 30 Days"
                    onClick={ this.onClick.bind( this, 'last30' ) }
                />
                <Button
                    classes={"small gray margin-left-medium " + ( this.props.dataSet === "all" ? 'active' : '' )}
                    text="All"
                    onClick={ this.onClick.bind( this, 'all' ) }
                />
                <Button
                    classes="small gray margin-left-medium arrowRight saveJPG"
                    text="Save JPG"
                    onClick={ this.props.save }
                />
            </div>
        );
    }
}

class GraphBody extends React.Component {
    constructor( props ){
        super( props );

        this.state = {
            data: null
        };
        this.modifyData = this.modifyData.bind( this );
    }


    componentWillReceiveProps( nextProps ){
        this.modifyData( nextProps.data, nextProps.dataSet );
        //this.setState( { data: this.modifyData( nextProps.data, nextProps.dataSet ) } );
    }


    componentWillMount(){
        this.modifyData( this.props.data, this.props.dataSet );
        //this.setState( { data: this.modifyData( this.props.data, this.props.dataSet ) } );
    }

    //modify data to fit d3 line graph structure
    //dataSet indicates chunk of data to include (7 days, 30days, all)
    modifyData( data, dataSet ){

        //get only the data from the tags data
        let masterData = [],
            names = []; //array of tags

        if( data )

        for( var i = 0; i < data.length; i++ ){
            if( typeof data[i].dataByDay !== "undefined" ){
                let dataGroup = data[i].dataByDay,
                    dataName = data[i].name;

                names.push( dataName );

                for ( var j = 0; j < dataGroup.length; j++ ) {
                    let day = dataGroup[j];

                    //first see if data exists, if not create
                    if( typeof masterData.find( item => item.date === day.date ) === "undefined" ){
                        let dataObj = {
                            date: day.date
                        };
                        dataObj[ dataName ] = day.total.toString();
                        masterData.push( dataObj );
                    }

                    //if it does exist add to it
                    else{
                        var item = masterData.find( item => item.date === day.date );
                        item[ dataName ] = day.total;
                    }
                }
            }
        }

        //sort data by date ( masterData might not have been created in order )
        function sortByDate( a, b ) {
            let comparison = 0;
            if( a.date > b.date ) {
                comparison = 1;
            } else if( a.date < b.date ){
                comparison = -1;
            }
            return comparison;
        }
        masterData.sort( sortByDate );


        //loop back through data and add in any items where they don't have a record for the day, as a 0 value
        for( var k = 0; k < masterData.length; k++ ){
            let day = masterData[k];
            for( let n of names ){
                if( typeof day[n] === "undefined" ){
                    day[n] = '0';
                }
            }
        }

        //limit data to data set
        // limit data to last days in the data
        if( dataSet !== "all" ){

            const numDays = parseInt( dataSet.replace( "first", "" ).replace( "last", "" ), Global.radix );

            //first n days
            if( dataSet.includes( 'first' ) ){
                masterData = masterData.slice( 0, numDays );
            }

            //last n days
            else{
                masterData = masterData.slice( Math.max( masterData.length - numDays, 1 ) );
            }
        }

        //console.log( masterData );

        this.setState( {
            data: masterData
        } );

    }

    render() {

        let graphSize = [ 1046, 508 ];
        if( this.props.id === "carouselGraph" || this.props.id === "videoGraph" ){
            graphSize = [ 675, 400 ];
        }

        if( this.state.data !== null ){
            console.log(this.state.data);
            return (
                <div id={ this.props.id } className="graphBody d3">
                    <LineChart name={ this.props.id } data={ this.state.data } size={ graphSize } graphStyle={ this.props.graphStyle } />
                </div>
            );
        }else{
            return (
                <div>test</div>
            );
        }
    }
}







/*
    DATA TABLE BUILDER
    Gets data and returns a graph
*/
export class Table extends React.Component {
    /*constructor( props ){
        super( props );
    }*/

    render() {
        if( typeof this.props.data !== 'undefined' && this.props.data.length > 0 ){
            return (
                <div id={ this.props.id }  className="tableWrap">
                    <TableHeader type={ this.props.type } />
                    <TableRows data={ this.props.data } dataType={ this.props.type } />
                </div>
            );
        }else{
            return (
                <div className="emptyTable tableWrap">
                    <div>No Data { this.props.id }</div>
                </div>
            );
        }
    }
}
class TableHeader extends React.Component {
    render() {

        //include redirect in table
        let includeRedirect = false;
        if( this.props.type === "clicks" ){
            includeRedirect = true;
        }

        return (
            <div className="tableHeader flex">
                <div className="tableColumn name">
                    <label>Name</label>
                </div>
                { includeRedirect &&
                    <div className="tableColumn redirect">
                        <label>Redirect</label>
                    </div>
                }
                <div className="tableColumn count">
                    <label>Count</label>
                </div>
            </div>
        );
    }
}

class TableRows extends React.Component {
    render() {

        //get highest number
        const max = this.props.data.reduce( ( max, p ) => p.total > max ? p.total : max, this.props.data[0].total );

        //build rows for each item
        const rows = this.props.data.map( ( item, i ) =>
            (
                <TableRow key={ `item_${ i }` } data={ item } dataType={ this.props.dataType } max={ max } />
            )
        );

        return (
            <div className="tableRows">
                { rows }
            </div>
        );
    }
}


//build data for each row
class TableRow extends React.Component {

    render() {

        const scale = ( ( this.props.data.total / this.props.max ).toFixed( 3 ) * 100 );
        let code;

        //if clicks
        if( this.props.dataType === "clicks" ){
            code = (
                <div className='tableRow'>
                    <div className="barWrap">
                        <div className="bar" style={{ width: scale + "%" }}></div>
                    </div>
                    <div className="tableRow-inner flex">
                        <div className="tableColumn name">
                            <p>{ this.props.data.name }</p>
                        </div>
                        <div className="tableColumn redirect">
                            <p>{ this.props.data.redirect }</p>
                        </div>
                        <div className="tableColumn count">
                            <p>{ this.props.data.total }</p>
                        </div>
                    </div>
                </div>
            );
        }else{
            code = (
                <div className='tableRow' >
                    <div className="barWrap">
                        <div className="bar" style={{ width: scale + "%" }}></div>
                    </div>
                    <div className="tableRow-inner flex">
                        <div className="tableColumn name">
                            <p>{ this.props.data.name }</p>
                        </div>
                        <div className="tableColumn count">
                            <p>{ this.props.data.total }</p>
                        </div>
                    </div>
                </div>
            );
        }

        return code;
    }
}
