import React from 'react';
import { Global } from '../json/global'; //global settings
import { Button } from  './Buttons';
import { FormField, Dropdown } from './Inputs';
import { CardTitle } from './Typography';
import { ExportData, GetData } from './Helpers';
import {showNotification} from "./Notification";

/*
    TAG GENERATOR
    Used to build and save tracking tags
*/
export class TagGenerator extends React.Component {
    constructor( props ){
        super( props );

        this.state = {
            tags: [],
            tagCount: 0,
            renderedClicks: [],
            renderedImps: []
        };

        //bind this to functions
        this.getExistingTags = this.getExistingTags.bind( this );
        this.newTag = this.newTag.bind( this );
        this.buildTag = this.buildTag.bind( this );
        this.saveNewTags = this.saveNewTags.bind( this );
        this.saveTag = this.saveTag.bind( this );
        this.deleteTag = this.deleteTag.bind( this );
        this.deleteSavedTag = this.deleteSavedTag.bind( this );
        this.downloadTags = this.downloadTags.bind( this );
    }

    /*
    show() {
        document.getElementsByClassName( 'tagGeneratorWrap' )[0].classList.toggle( "show" );
        document.getElementsByClassName( 'headerSlantedEdge' )[0].classList.add( "filterOpen" );
    }
    */

    /*
        Hide the tag generator lightbox
    */
    hide(){
        document.getElementsByClassName( 'tagGeneratorWrap' )[0].classList.toggle( "show" );
        setTimeout( function(){
            document.getElementsByClassName( 'headerSlantedEdge' )[0].classList.remove( "filterOpen" );
        }, 600 );
    }


    /*
        Get Existing Tags from the database
        These are tags that users have previously saved
    */
    getExistingTags( projectId ){
        fetch( `${ Global.apiEndpoint }/project/${ projectId }/tags`, {
            headers: {
                'apiToken': GetData( 'apiKey', 'session' )
            }
        } )
            .then( ( response ) => {
                return response.text();
            } )
            .then( ( data ) => {
                var tags = JSON.parse( data );
                console.log( "existing tags:", tags );

                //show tags in list
                let count = 0;
                for( let t of tags ){
                    count++;
                    this.buildTag( count, t.type, t.desc, t.redirect, true );
                }
            } );
    }


    /*
        Create New Tag
        Update the tags array in the state to append new tag item
    */
    newTag(){
        let tagList = this.state.tags;
        let count = this.state.tagCount + 1;
        tagList.push(
            <NewTag
                key={ count }
                num={ count }
                onUpdate={ this.buildTag }
                onDelete={ this.deleteTag }
            />
        );

        this.setState( {
            tags: tagList,
            tagCount: count
        } );
    }


    /*
        Build Tag String
        Called when the tag inputs change
        Tag list is the group the tag gets visually displayed in
    */
    buildTag( id, type, desc, redirect = null, existingTag = false ) {

        if( type === "overall" ){
            type = "imp";
        }

        let baseTag = Global.baseTags[ type ];
        let tagList = this.state.renderedImps;

        if( existingTag === true ){
            id = "Saved Tag " + id;
        }
        if( type === "click" ){
            tagList = this.state.renderedClicks;
        }

        //replace spaces in the description/name property with underscores
        if( desc !== null ){
            desc = desc.replace( / /g, "_" );
        }

        //if redirect exists URL encode a few things
        //the redirect is a query within the overall tag URL so it can't contain any it's own queries (?) or parameters {&/#} or other invalid strings
        if( redirect !== null ){
            redirect = redirect.replace( "?", "%3F" );
            redirect = redirect.replace( "&", "%26" );
            redirect = redirect.replace( "#", "%23" );
            redirect = redirect.replace( ";", "%3B" );
        }

        //update the properties in the baseTag to build the tag
        const map = {
            "{{ creativeId }}" : this.props.projectId,
            "{{ operativeId }}" : this.props.operativeId,
            "{{ tagName }}" : desc,
            "{{ redirect }}" : redirect
        };
        let regex = new RegExp( Object.keys( map ).join( "|" ), "gi" );
        let tag = baseTag.replace( regex, function( matched ) {
            return map[matched];
        } );

        //add cachebuster for clicktags
        if( type !== "click" ){
            tag = tag + "&ord=%%CACHEBUSTER%%";
        }

        //see if tag exists in data already
        let tagExists = false;
        for( let t of tagList ){
            if( t.id === id ){
                t.tag = tag;
                t.name = desc;
                t.redirect = redirect;
                tagExists = true;
            }
        }

        //does not exist, make new
        if( tagExists === false ){
            tagList.push( {
                id: id,
                tag: tag,
                name: desc,
                redirect: redirect
            } );
        }

        //set state to push tag into list
        if( type === "click" ){
            this.setState( {
                renderedClicks: tagList
            } );
        }else{
            this.setState( {
                renderedImps: tagList
            } );
        }
    }


    /*
        Save all tags to database
        Loops through the current list of tags and calls the saveTag method to push it into the DB
    */
    saveNewTags( tag, type, desc, redirect ){
        const impTags = this.state.renderedImps;
        const clickTags = this.state.renderedClicks;

        for( let i of impTags ){
            if( typeof i.id === 'number' ){
                this.saveTag( i.tag, 'imp', i.name, null );
            }
        }
        for( let c of clickTags ){
            if( typeof c.id === 'number' ){
                this.saveTag( c.tag, 'click', c.name, c.redirect );
            }
        }
    }



    /*
        Save individual tag to database
        If tag exists it simply updates the data for it
    */
    saveTag( tag, type, desc, redirect ){
        let data = new FormData();
        data.append( 'fullTag', tag );
        data.append( 'type', type );
        data.append( 'desc', desc );
        data.append( 'redirect', redirect );

        fetch( `${ Global.apiEndpoint }/project/${ this.props.projectId }/tag/create`, {
            headers: {
                'apiToken': GetData( 'apiKey', 'session' )
            },
            method: "POST",
            body: data
        } )
            .then( ( response ) => {
                return response.text();
            } )
            .then( ( data ) => {
                console.log( data );
            } );
    }


    /*
        Delete Tag
        Removes it from the visual tag list on the page
        This does not affect the database
    */
    deleteTag( id ){
        let tagList = this.state.tags;

        //remove from tags
        for( let t of tagList ){
            if( parseInt( t.key, Global.radix ) === id ){
                this.state.tags.splice( tagList.indexOf( t ), 1 ); //remove
            }
        }

        //remove from rendered tags
        const impTags = this.state.renderedImps;
        const clickTags = this.state.renderedClicks;
        let foundTag = false;

        for( let i of impTags ){
            if( i.id === id ){
                impTags.splice( impTags.indexOf( i ), 1 ); //remove
                foundTag = true;
            }
        }
        if( foundTag === false ){
            for( let c of clickTags ){
                if( c.id === id ){
                    clickTags.splice( clickTags.indexOf( c ), 1 ); //remove
                    foundTag = true;
                }
            }
        }

        this.setState( {
            tags: tagList,
            renderedImps: impTags,
            renderedClicks: clickTags
        } );
    }

    /*
        Delete saved tag from database
    */
    deleteSavedTag( tagId, name ){

        let data = new FormData();
        data.append( 'desc', name );

        fetch( `${ Global.apiEndpoint }/project/${ this.props.projectId }/tag/delete`, {
            headers: {
                'apiToken': GetData( 'apiKey', 'session' )
            },
            method: "POST",
            body: data
        } )
            .then( ( response ) => {
                return response.text();
            } )
            .then( ( data ) => {
                console.log( data );
            } );

        //remove from rendered tags
        this.deleteTag( tagId );
    }


    /*
        Download Tags as excel doc
    */
    downloadTags(){
        const data = [];
        for( let c of this.state.renderedClicks ){
            data.push( {
                type: 'click',
                id: c.id,
                name: c.name,
                tag: c.tag
            } );
        }
        for( let i of this.state.renderedImps ){
            data.push( {
                type: 'impression',
                id: i.id,
                name: i.name,
                tag: i.tag
            } );
        }

        const exportStatus = ExportData(
            data,
            [ 'type', 'id', 'name', 'tag' ],
            [
                { wch:20 },
                { wch:10 },
                { wch: 30 }
            ],
            `project_${ this.props.projectId }_tags.xlsx`
        );
        console.log( exportStatus );
    }



    /*
        called when component first loads, not called for changes
    */
    componentDidMount() {
        //get existing tags
        this.getExistingTags( this.props.projectId );
    }


    render() {

        let renderedClicks = this.state.renderedClicks.map( ( item, i ) => (
            <RenderedTag key={ item.id } data={ item } deleteSaved={ this.deleteSavedTag } />
        ) );
        let renderedImps = this.state.renderedImps.map( ( item, i ) => (
            <RenderedTag key={ item.id } data={ item } deleteSaved={ this.deleteSavedTag } />
        ) );

        return (
            <div className="metaHeader-expandable tagGeneratorWrap">

                <Button
                    classes="createTagsBtn secondary onDark hasIcon active"
                    icon="pencil"
                    text="Create Tags"
                    onClick={ this.hide }
                />

                <div className="flex contentWrapper">
                    <div className="intro">
                        { this.state.tags.length === 0 &&
                            <p>Click New Tag to start building a new tag.</p>
                        }
                        <div id="tagsBuilder">
                            { this.state.tags }
                        </div>

                        <div className="flex">
                            <Button
                                classes="newTag small hasIcon margin-top-medium"
                                icon="pencil"
                                text="New Tag"
                                onClick={ this.newTag }
                            />
                            { this.state.tags.length > 0 &&
                                <Button
                                    classes="downloadTags small hasIcon margin-top-medium"
                                    icon="download"
                                    text="Download Tags"
                                    onClick={ this.downloadTags }
                                />
                            }
                            { this.state.tags.length > 0 &&
                                <Button
                                    classes="saveTags small hasIcon margin-top-medium"
                                    icon="folder"
                                    text="Save New Tags"
                                    onClick={ this.saveNewTags }
                                />
                            }
                        </div>
                    </div>

                    <div id="generatedTags" className="margin-left-large">
                        <div id="clickTags">
                            <h4 className="margin-bottom-small">Click Tags</h4>
                            { this.state.renderedClicks.length > 0 &&
                                renderedClicks
                            }
                        </div>
                        <div id="impTags" className="margin-top-large">
                            <h4 className="margin-bottom-small">Impression Tags</h4>
                            { this.state.renderedImps.length > 0 &&
                                renderedImps
                            }
                        </div>
                    </div>
                </div>


            </div>
        );
    }
}


/*
    New Tag Dialog
    Build the form fields and handles the inputs for creating a specific new tag
    New instance is started everytime "new tag" button is clicked
*/
class NewTag extends React.Component {
    constructor( props ){
        super( props );

        this.state = {
            type: 'imp',
            name: null,
            redirect: null
        };

        //bind this to functions
        this.changeType = this.changeType.bind( this );
        this.changeName = this.changeName.bind( this );
        this.changeRedirect = this.changeRedirect.bind( this );
        this.deleteTag = this.deleteTag.bind( this );
    }


    /*
        Set the type of the tag
        Options are "overall", "click", "impression"
    */
    changeType( data ){
        const selected = data.target.option;

        //if overall impression go ahead and build tag
        if( selected.id === "overall" ){
            this.setState( {
                type: 'overall',
                result: document.getElementById( "impTags" ),
                name: Global.overallImpressionTagName
            } );
        }

        //if anything else
        else{
            this.setState( {
                type: selected.id,
                result: document.getElementById( selected.id + "Tags" )
            } );
        }
    }

    //update the tag name when the user inputs data
    changeName( data ){
        this.setState( {
            name: data.target.value
        } );
    }

    //update the redirect when the user inputs data
    changeRedirect( data ){
        let redirect = data.target.value;

        //make URL protocol relative
        //protocol relative URLs inherit the protocol of the tag, which is https - so that assumes all redirects are https, which is not true
        //redirect = redirect.replace( "https:", "" );
        //redirect = redirect.replace( "http:", "" );

        this.setState( {
            redirect: redirect
        } );
    }

    /*
        delete tag
        calls the parent component delete method so it's deleted from both the form side and the visual tag list side
    */
    deleteTag(){
        this.props.onDelete(
            this.props.num
        );
    }

    componentDidUpdate( data ){
        //call update method to generate tag string
        if( this.state.name !== null ){
            this.props.onUpdate(
                this.props.num,
                this.state.type,
                this.state.name,
                this.state.redirect
            );
        }
    }


    render() {

        return (
            <div className="tagWrap" data-id={ 'tag' + this.props.num }>
                <CardTitle classes='tagCount margin-bottom-medium'>{ 'Tag ' + this.props.num }</CardTitle>
                <Button
                    classes="deleteTag iconOnly small"
                    icon="trash"
                    onClick={ this.deleteTag }
                />
                <div className="flex">
                    <Dropdown
                        wrapClass="half-width flex-grow"
                        inputClass="bold onLightest"
                        name="type"
                        label="type"
                        placeholder="Choose Tag Type"
                        instructions="Select or create new"
                        options={ [
                            {
                                id: 'click',
                                text: 'Clicktag'
                            },
                            {
                                id: 'imp',
                                text: 'Interaction'
                            },
                            {
                                id: 'overall',
                                text: 'Overall Impression'
                            }
                        ] }
                        onChange={ this.changeType }
                    />
                    { this.state.type !== "overall" &&
                        <FormField
                            wrapClass="half-width flex-grow"
                            inputClass="outlineInput onLightest moveLabelOnFocus forceLabelAbove bold color-blue"
                            name="description"
                            type="text"
                            label="Name / Description"
                            placeholder="Enter Tag Description"
                            required="true"
                            onBlur={ this.changeName }
                        />
                    }
                </div>
                { this.state.type === "click" &&
                    <div id="redirectField" className="form-row margin-top-medium">
                        <FormField
                            wrapClass="flex-grow margin-right-medium"
                            inputClass="outlineInput onLightest moveLabelOnFocus forceLabelAbove bold color-blue"
                            name="redirect"
                            type="text"
                            label="Redirect"
                            placeholder="Enter Clicktag Redirect URL"
                            required="true"
                            onBlur={ this.changeRedirect }
                        />
                    </div>
                }
            </div>
        );
    }
}

/*
    Create a visual string representation of the tag and append it into the right column of tags
*/
class RenderedTag extends React.Component {


    constructor( props ) {
        super( props );

        this.state ={
            displayNotification: null,
            notification: {
                title: "Test Title",
                message: "Test Message",
            }
        };

        this.deleteSaved = this.props.deleteSaved.bind( this, this.props.data.id, this.props.data.name );
        this.copyToClipboard = this.copyToClipboard.bind(this, this.props.data.tag);

        if(typeof navigator.clipboard !== "undefined"){
            console.log("Can access the clipboard API")
        }
        else{
            console.log("Cannot access the clipboard API")
        }

    }

    copyToClipboard(tag){
        console.log("Copying to clipboard");

        if (!navigator.clipboard) {
            this.fallbackCopyTextToClipboard(tag);
            return;
        }
        else{
            navigator.clipboard.writeText(tag)
                .then(() => {
                    console.log('Text copied to clipboard');
                    console.log("textCopy: " + tag);
                    console.log("Showing Notificationn");
                    showNotification("Tag Copied", "", "success");
                    // this.setState({
                    //     displayNotification: "active",
                    //     notification: {
                    //         title: "Text Copied",
                    //         message: tag
                    //     }
                    // });
                    // console.log(this.state);
                    // setTimeout(() => {this.setState({displayNotification: null,}); console.log("Hiding Notification")}, 3000)


                })
                .catch(err => {
                    // This can happen if the user denies clipboard permissions:
                    console.error('Could not copy text: ', err);
                    showNotification("Could not copy tag", err, "error");
                });
        }
    }

    fallbackCopyTextToClipboard(text) {
        var textArea = document.createElement("textarea");
        textArea.value = text;
        document.body.appendChild(textArea);
        textArea.focus();
        textArea.select();

        try {
            var successful = document.execCommand('copy');
            var msg = successful ? 'successful' : 'unsuccessful';
            console.log('Fallback: Copying text command was ' + msg);
            showNotification("Tag Copied", "", "success");
            successful ? showNotification("Tag Copied", "", "success") : showNotification("Could not copy tag", "", "error");
        } catch (err) {
            console.error('Fallback: Oops, unable to copy', err);
            showNotification("Could not copy tag", err, "error");
        }

        document.body.removeChild(textArea);
    }

    render(){

        return (
            <div id={ this.props.data.id } className="renderedTag">
                {/*<Notification notification={this.state.notification} active={this.state.displayNotification}/>*/}
                { typeof this.props.data.id === "string" &&
                    <div className='flex vertical-center justify-between'>
                        <span className="tag-name">{ this.props.data.id } { this.props.data.name }</span>
                        <Button
                            classes="deleteTag tiny hasIcon gray flex-align-right"
                            icon="clipboard"
                            text="Copy To Clipboard Beta"
                            onClick={this.copyToClipboard}
                        />
                        <Button
                            classes="deleteTag tiny hasIcon gray"
                            icon="trash"
                            text="Delete Saved Tag"
                            onClick={ this.deleteSaved }
                        />
                    </div>
                }
                { typeof this.props.data.id === "number" &&
                    <span className="tag-name">New Tag { this.props.data.id } - { this.props.data.name }</span>
                }
                <p className="tag-value">{ this.props.data.tag }</p>
            </div>
        );
    }
}
