import React from 'react';
import { Global } from '../json/global'; //global settings

/*
    FORMAT NUMBER
*/
export class FormatNumber extends React.Component {
    /*constructor( props ){
        super( props );
    }*/

    render() {

        let formattedNum = this.props.num;

        //if number, format with commas
        //if not number ignore
        if( Number.isFinite( formattedNum ) === true ){
            formattedNum = parseInt( formattedNum, Global.radix ).toLocaleString( 'en-US' );
        }

        return (
            <span>{ formattedNum }</span>
        );

    };
}
