import React from 'react';
import { Icon } from './Icons';
import { Link } from 'react-router-dom';
import { Button, ButtonState } from './Buttons';
import { Global } from '../json/global';
//import { AdvertiserList } from '../../json/advertisers'; //advertiser list
import { SortAlphabetically, GroupByLetter, StoreData, GetData } from './Helpers'; //global settings
import {Dropdown, Form, FormField} from './Inputs';
import {CardTitle, SectionTitle} from './Typography';
import {Overlay} from "./Overlay";


/*
    GET ADVERTISERS
    Retrieve the advertiser list either from DB or from session storage
*/
export function GetAdvertisers( callback ){

    //if saved to local storage
    const storedAdvertiserData = GetData( 'advertiserList', 'session' );
    if( storedAdvertiserData !== null ){
        if( typeof callback === "function" ){
            callback( storedAdvertiserData );
        }
        return storedAdvertiserData;
    }

    //not saved, get from server
    else{

        fetch( `${ Global.apiEndpoint }/advertisers`, {
            headers: {
                'apiToken': GetData( 'apiKey', 'session' )
            }
        } )
            .then( ( response ) => {
                return response.text();
            } )
            .then( ( data ) => {
                data = JSON.parse( data );
                StoreData( 'advertiserList', data, 'session' );
                if( typeof callback === "function" ){
                    callback( data );
                }
            } );
    }

}




/*
    GET ALL ADVERTISERS
*/
export class AllAdvertisers extends React.Component {
    constructor( props ){
        super( props );

        this.state = {
            advertiserList: null
        };

        this.jumpToLetter = this.jumpToLetter.bind( this );
    }

    jumpToLetter( letter ){
        document.getElementById( `letter_${ letter }` ).scrollIntoView( {
          behavior: 'smooth'
        } );
    }


    /*
        called when new params are passed to the component, like changing between projects
    */
    componentWillReceiveProps( nextProps ){
        let component = this;
        GetAdvertisers( function( data ){
            console.log( 'retrieved advertisers' );
            component.setState( {
                advertiserList: data
            } );
        } );
    }

    /*
        called when component first loads, not called for changes
    */
    componentDidMount() {
        let component = this;
        GetAdvertisers( function( data ){
            console.log( 'retrieved advertisers' );
            component.setState( {
                advertiserList: data
            } );
        } );
    }


    render() {

        let advertisers = [];
        let activeLetters = [];

        if( this.state.advertiserList !== null ){
            let advertiserByLetter = this.state.advertiserList.sort( SortAlphabetically( 'name' ) );
            advertiserByLetter = GroupByLetter( advertiserByLetter );

            for( let [k, v] of Object.entries( advertiserByLetter ) ){
                advertisers.push( <AdvertiserGroup key={ k } letter={ k } data={ v } /> );
                activeLetters.push( k );
            }
        }




        return (
            <section className="advertiserPage">

                <header className="headerSlantedEdge background-backgroundDark color-onDarkBackground padding-y-xlarge text-align-center">
                    <Icon src='layers' />
                    <h2 className="color-white">All Advertisers</h2>
                </header>

                <div className="mainContent">
                    <div className="innerWrap large">

                        <div className="innerWrap-inner background-background padding-x-xlarge padding-y-xlarge">

                            <AlphabetSelector data={ activeLetters } clickHandler={ this.jumpToLetter } />

                            <div className="grid fourColumn spacing-xlarge margin-top-xlarge">
                                { advertisers }
                            </div>
                        </div>
                    </div>
                </div>

            </section>
        );
    }
}

/*
    Alphabet Selector to quickly jump to letter user is interested in
*/
class AlphabetSelector extends React.Component {

    render() {

        const alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".split( "" ); //alphabet array
        let alphabetNodes = [];

        for( let letter of alphabet ){
            if( this.props.data.includes( letter ) ){
                alphabetNodes.push(
                    <Letter
                        key={ letter }
                        active="true"
                        letter={ letter }
                        clickHandler={ this.props.clickHandler.bind( this, letter ) } //bind the active letter to the click handler
                    />
                );
            }else{
                alphabetNodes.push( <Letter key={ letter } active="false" letter={ letter } /> );
            }
        }

        return (
                <div className="alphabetSelector flex justify-center">
                    { alphabetNodes }
                </div>
        );
    }
}

class Letter extends React.Component {
    render() {
        return (
            <div className="alphabetSelector-letter" data-active={ this.props.active } onClick={ this.props.clickHandler }>{ this.props.letter }</div>
        );
    }
}

/*
    Advertiser Group
*/
class AdvertiserGroup extends React.Component {

    render() {

        //build advertisers
        const advertisers = this.props.data.map( ( item ) =>
            (
                <Advertiser key={ item.id } id={ item.id } name={ item.name } />
            )
        );

        //determine number of items and make grid layout based on it
        const numColumns = Math.ceil( this.props.data.length / 10 );

        return (
            <div className={ `advertiser-group grid span${ numColumns } matchColumns spacing-x-xlarge spacing-y-medium` } id={ 'letter_' + this.props.letter }>
                <p className="advertiser-group-letter">{ this.props.letter }</p>
                { advertisers }
            </div>
        );
    }
}

/*
    Advertiser item
*/
class Advertiser extends React.Component {

    render() {
        return (
            <div className='advertiser' data-id={ this.props.id }>
                <Link to={ `/advertiser/${ this.props.id }` }>{ this.props.name }</Link>
            </div>
        );
    }
}






/*
    custom function to fetch data
*/
function getCampaigns( id, callback ){
    console.log( 'getting data for advertiser', id );

    fetch( `${ Global.apiEndpoint }/advertisers/${ id }`, {
        headers: {
            'apiToken': GetData( 'apiKey', 'session' )
        }
    } )
        .then( ( response ) => {
            return response.text();
        } )
        .then( ( data ) => {
            //console.log( data );
            var dataObj = JSON.parse( data );

            if( typeof callback === "function" ){
                callback( dataObj );
            }

        } );
}





/*
    GET SPECIFIC ADVERTISER
*/
export class GetAdvertiser extends React.Component {
    constructor( props ){
        super( props );
        this.params = this.props.match.params; //gets the variables in the URL String and make a shorter way to access them

        this.state = {
            metaData: null,
            campaigns: null
        };
        this.editAdvertiser = this.editAdvertiser.bind(this);
        this.updateProject = this.updateProject.bind(this);
    }


    /*
        called when new params are passed to the component, like changing between projects
    */
    componentWillReceiveProps( nextProps ){
        console.log( 'receive props', nextProps.match.params.id );

        if( this.state.metaData.id !== nextProps.match.params.id ){
            const component = this;
            getCampaigns( nextProps.match.params.id, function( data ){
                component.setState( {
                    metaData: data.advertiser,
                    campaigns: data.campaigns
                } );
            } );
        }
    }


    /*
        called when component first loads, not called for changes
    */
    componentDidMount() {
        const component = this;
        getCampaigns( parseInt( this.params.id, Global.radix  ), function( data ){
            component.setState( {
                metaData: data.advertiser,
                campaigns: data.campaigns
            } );
        } );
    }

    editAdvertiser(){
        this.editOverlay.show();
    }

    /*
        UPDATE Advertiser META DATA
        Called from within the edit project form
    */
    updateProject( e ){
        console.log( 'update' );
        e.preventDefault();

        const button = e.target;
        button.classList.remove( "state-error" );
        button.classList.add( "state-loading" );

        const name = document.querySelector( '.form-item[data-content="advertiser-name"] input' ).value;


        let data = new FormData();

        if( name !== this.state.metaData.name ){
            data.append( 'name', name );
        }


        fetch( `${ Global.apiEndpoint }/advertisers/${ this.state.metaData.id }/update`, {
            headers: {
                'apiToken': GetData( 'apiKey', 'session' )
            },
            method: "POST",
            body: data
        } )
            .then( ( response ) => {
                return response.text();
            } )
            .then( ( data ) => {
                console.log( data );
                const dataObj = JSON.parse( data );

                if( dataObj.status === "success" ){
                    button.classList.remove( "state-loading" );
                    button.classList.add( "state-success" );

                    setTimeout( function(){
                        button.classList.remove( "state-success" );
                    }, 2000 );

                }

            } );
    }

    //advertisers/{id}/update

    render() {

        let advertiserName = "Loading Advertiser";

        if( this.state.metaData !== null ){
            advertiserName = this.state.metaData.name;
        }



        //day advertiser was created, with timezone adjustment
        let createdAt = "";
        if( this.state.metaData !== null ){
            if( this.state.metaData.created_at !== null ){
                createdAt = new Date( this.state.metaData.created_at );
                createdAt = new Date( createdAt.setHours( createdAt.getHours() + 4 ) ).toDateString();
            }
        }


        //campaign data
        let campaigns = "loading";
        if( this.state.campaigns !== null ){
            //console.log( this.state.campaigns );
            campaigns = this.state.campaigns.map( function( c ){
                if( c.projectCount !== 0 ){
                    return <Campaign key={ c.id } data={ c } />;
                }else{
                    return false;
                }
            } );
        }




        return (
            <section className="advertiserPage">

                <header className="headerSlantedEdge background-backgroundDark color-onDarkBackground padding-y-xlarge text-align-center">
                    <Icon src='layers' />
                    <h2 className="color-white">
                        { advertiserName }
                        { GetData( 'userGroup', 'session' ) !== "viewer" &&
                        <Button
                            classes="onDark iconOnly margin-left-small"
                            icon="pencil"
                            onClick={ this.editAdvertiser }
                        />
                        }
                    </h2>
                    <div className="dates">
                        <p className="tiny">Created: { createdAt }</p>
                    </div>

                    <div className="backToAll">
                        <Link to={ `/advertiser/all` }>
                            <Button
                                classes="secondary onDark"
                                icon="arrowBack"
                                text='All Advertisers'
                            />
                        </Link>
                    </div>
                </header>

                <div className="mainContent advertiserDetailPage">
                    <div className="innerWrap large">

                        <div className="innerWrap-inner background-background padding-x-xlarge padding-y-xlarge">

                            <div className="grid fourColumn spacing-xlarge margin-top-large">
                                { campaigns }
                            </div>

                        </div>
                    </div>
                </div>

                { this.state.metaData !== null &&

                    <Overlay name="editWrap" ref={(overlay) => {
                    this.editOverlay = overlay;
                }}>
                    <div className='editProjectWrap innerWrap xsmall-min'>
                    <CardTitle classes="margin-bottom-large">Edit Project</CardTitle>
                    <Form>
                    <div className="form-row flex">
                    <FormField
                    wrapClass="flex-grow"
                    inputClass="outlineInput moveLabelOnFocus forceLabelAbove bold color-blue"
                    name="advertiser-name"
                    type="text"
                    label="Advertiser Name"
                    value={advertiserName}
                    required="true"
                    />
                    </div>
                    <div className="form-actions flex justify-end margin-top-xlarge">
                    <Button
                    loader="true"
                    classes="primary"
                    icon="pencil"
                    text="Update Project"
                    onClick={this.updateProject}
                    />
                    </div>
                    </Form>
                    </div>
                    </Overlay>

                }

            </section>
        );
    }
}



/*
    NEW ADVERTISER
    Called from create project form
*/
export class NewAdvertiser extends React.Component {

    constructor( props ){
        super( props );
        this.createNew = this.createNew.bind( this );
    }

    createNew( e ){
        console.log( 'create new advertiser' );

        e.preventDefault();

        const button = e.target;
        ButtonState( button, 'loading' );

        //verify name
        const name = document.getElementById( 'newAdvertiserName' ).value;
        if( name === "" ){
            ButtonState( button, 'error' );
            alert( "Advertiser Name is required." );
            return false;
        }

        let data = new FormData();
        data.append( 'name', name );

        fetch( `${ Global.apiEndpoint }/advertisers/create`, {
            headers: {
                'apiToken': GetData( 'apiKey', 'session' )
            },
            method: "POST",
            body: data
        } )
            .then( ( response ) => {
                return response.text();
            } )
            .then( ( data ) => {
                console.log( data );
                const dataObj = JSON.parse( data );
                if( dataObj.status === "success" ){
                    this.props.onClose();
                    this.props.oncomplete( dataObj.newAdvertiser );
                    ButtonState( button, 'success' );
                }else{

                    //if advertiser exists select it, then call getCampaigns function to populate campaign dropdown
                    if( dataObj.error === "exists" ){
                        this.props.onClose();
                        this.props.onexists( dataObj.advertiser.id );
                        ButtonState( button, 'success' );
                    }

                    //if doesn't exist then another error occured
                    else{
                        ButtonState( button, 'error' );
                    }
                }

            } );
    }

    render() {
        return (
            <div className="overlayItem newAdvertiser">
                <SectionTitle>Create new advertiser</SectionTitle>
                <Form>
                    <div className="form-row flex margin-top-large viewport-min-width-thirty">
                        <FormField
                            wrapClass="flex-grow"
                            inputClass="outlineInput moveLabelOnFocus bold color-blue"
                            name="newAdvertiserName"
                            type="text"
                            label="Advertiser Name"
                            //placeholder="Enter Advertiser Name"
                            required="true"
                            //instructions="Enter a name for the new advertiser"
                        />
                    </div>
                    <div className="form-actions flex justify-end margin-top-xlarge">
                        <Button
                            loader="true"
                            classes="primary"
                            icon="pencil"
                            text="Create Advertiser"
                            onClick={ this.createNew }
                        />
                    </div>
                </Form>
            </div>
        );
    }

}





/*
    Build Campaign nodes
*/
class Campaign extends React.Component {

    render() {

        const data = this.props.data;

        //day advertiser was created, with timezone adjustment
        let createdAt = null;
        if( data.created_at !== null ){
            createdAt = new Date( data.created_at );
            createdAt = new Date( createdAt.setHours( createdAt.getHours() + 4 ) ).toDateString();
        }

        let projectText = `${ data.projectCount } project`;
        if( data.projectCount > 1 ){
            projectText = projectText + "s";
        }

        return (
            <div className='advertiser-group campaign' data-id={ data.id }>
                <p className="advertiser-group-letter"></p>
                <Link to={ `/campaign/${ data.id }` }>
                    <div className="campaign-name">{ data.name }</div>
                    <div className="campaign-metaData margin-top-small">
                        <p>Operative Id: { data.operativeOrderId }</p>
                        <p>Created: { createdAt }</p>
                    </div>
                    <Button
                        classes="secondary gray margin-top-large"
                        text={ projectText }
                    />
                </Link>
            </div>
        );
    }
}
