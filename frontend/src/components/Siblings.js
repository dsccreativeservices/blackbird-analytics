import React from 'react';
import { SubmenuItem } from './Submenu';
import { Link } from 'react-router-dom';
import { Global } from '../json/global'; //global settings
import { Button } from './Buttons';
import { GetData } from './Helpers';

/*
    PROJECT SIBLINGS
    Return siblings in campaign
*/
export class Siblings extends React.Component {
    constructor( props ){
        super( props );

        this.state = {
            siblings: null
        };
    }


    /*
        custom function to fetch data
        setting component state triggers re-render
    */
    getData( id ){
        fetch( `${ Global.apiEndpoint }/${this.props.type}/${id}/siblings`, {
            headers: {
                'apiToken': GetData( 'apiKey', 'session' )
            }
        })
            .then( ( response ) => {
                return response.text();
            } )
            .then( ( data ) => {
                //console.log( data );
                if( data === "project does not exist" || data === "project has no siblings" ){
                    this.setState( { siblings: null } );
                }else{
                    const dataObj = JSON.parse( data );
                    this.setState( {
                        siblings: dataObj.siblings,
                        parent: dataObj.parent
                    } );
                }
            } );
    }


    /*
        called when new params are passed to the component, like changing between projects
    */
    componentWillReceiveProps( nextProps ){
        if( this.state.siblings !== nextProps.id ){
            this.getData( nextProps.id );
        }
    }


    /*
        called when component first loads, not called for changes
    */
    componentDidMount() {
        this.getData( this.props.id );
    }


    render() {

        //siblings returns array, unless project is not found.
        //Don't render submenu is project is not found
        let submenu = "";
        if( this.state.siblings !== null && typeof this.state.siblings !== "undefined" ){
            submenu = this.state.siblings.map( ( item ) =>
                <SubmenuItem key={ item.id } className="siblings-item" name={ item.name } url={ '/' + this.props.type + '/' + item.id } />
            );

            let parent = "advertiser";
            if( this.props.type === "project" ){
                parent = "campaign"
            }

            return (
                <div className="siblings flex vertical-center flex-align-right">
                    <p className="siblings-intro uppercase add-spacing">Related:</p>
                    <ul className="siblings-list flex">
                        { submenu }
                    </ul>
                    <Link className="" to={ `/${ parent }/${ this.state.parent }` }>
                        <Button
                            classes="button outline small arrowRight"
                            text="See All"
                        />
                    </Link>
                </div>
            );
        }

        //return nothing
        else{
            return false;
        }

    }
}
