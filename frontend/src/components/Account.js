import React from 'react';
import { Global } from '../json/global';
import { PageTitle } from './Typography';
import { Icon } from './Icons';
import {genRandID, GetData} from './Helpers';
import {Dropdown, Form, FormField} from './Inputs';
import { Button } from './Buttons';

import '../css/account.css';

/*
    Account Page
*/
export class Account extends React.Component {
    constructor( props ){
        super( props );

        this.state = {
            data: null,
            fullUserList: [],
            users: [],
            permissions: [{text:"admin", id:"admin"}, {text:"viewer",id:"viewer"},{text:"editor",id:"editor"}],
            userGroup: "viewer",
            issues: [],
        };

        this.getData = this.getData.bind( this );
        this.filterUsers = this.filterUsers.bind(this);
    }

    /*
        fetch user data
    */
    getData(){

        const userId = GetData( 'userId', 'session' );
        const userGroup = GetData ('userGroup', 'session');

        console.log(userGroup);

        if(userGroup === "admin"){
            fetch( `${ Global.apiEndpoint }/users`, {
                headers: {
                    'apiToken': GetData( 'apiKey', 'session' )
                }
            })
                .then( ( response ) => {
                    console.log(response);
                    return response.text();
                } )
                .then( ( data ) => {
                    // console.log( data );
                    var dataObj = JSON.parse( data );
                    // console.log( dataObj );
                    let currentUser = dataObj.filter(user => user.id === userId)[0];
                    let issues = JSON.parse(currentUser.issues);
                    let trelloAPI = "f85c5e434d8709e1f11d146fb8ffd315&token=322d6ffea0ffbe61758aa0e0908776c84269dc7960582fd2aa968fa2442595c4";
                    let trelloToken = "322d6ffea0ffbe61758aa0e0908776c84269dc7960582fd2aa968fa2442595c4";
                    let trelloURLS = "";


                    //Limit to 10
                    if(issues > 10){

                    }
                    else{
                        for (let i = 0; i < issues.length ; i++) {
                            if(i === issues.length-1){
                                trelloURLS += `/cards/${issues[i]}`
                                // trelloURLS += `/cards/${issues[i]}/`
                            }
                            else{
                                trelloURLS += `/cards/${issues[i]},`
                                // trelloURLS += `/cards/${issues[i]}/,`
                            }
                        }
                    }
                    let trelloEndpoint = `https://api.trello.com/1/batch/?urls=${trelloURLS}&key=${trelloAPI}&token=${trelloToken}`;

                    console.log(trelloEndpoint);

                    fetch( trelloEndpoint)
                        .then( ( response ) => {
                            console.log(response);
                            return response.text();
                        } ).then( ( data ) => {
                        var issueObj = JSON.parse(data);

                        this.setState( {
                            data: currentUser,
                            users: dataObj,
                            fullUserList: dataObj,
                            userGroup: userGroup,
                            issues: issueObj,
                        } );
                        console.log(this.state);

                    });
                } );
        }
        else{
            fetch( `${ Global.apiEndpoint }/user/${ userId }`, {
                headers: {
                    'apiToken': GetData( 'apiKey', 'session' )
                }
            })
                .then( ( response ) => {
                    return response.text();
                } )
                .then( ( data ) => {
                    console.log( data );
                    var dataObj = JSON.parse( data );
                    console.log( dataObj );

                    let currentUser = dataObj;
                    let issues = JSON.parse(currentUser.issues);

                    this.setState( {
                        data: currentUser,
                        users: null,
                        fullUserList: null,
                        userGroup: userGroup,
                        issues: issues,
                    } );
                    console.log(this.state);
                } );
        }
    }

    update(e){
        e.preventDefault();
        console.log( 'update password' );

        const button = e.target;

        function inputError( input ){
            input.classList.add( "error" );
            button.classList.remove( "state-loading" );
            button.classList.add( "state-error" );
        }
        function inputClear( input ){
            input.classList.remove( "error" );
            button.classList.remove( "state-error" );
        }

        const password1 = document.getElementById( 'password1' ).value;
        const password2 = document.getElementById( 'password2' ).value;

        if( password1 === "" ){
            inputError( document.getElementById( 'password1' ) );
            return false;
        }else{
            inputClear( document.getElementById( 'password1' ) );
        }

        if( password2 === "" ){
            inputError( document.getElementById( 'password2' ) );
            return false;
        }else{
            inputClear( document.getElementById( 'password2' ) );
        }

        if( password1 !== password2 ){
            inputError( document.getElementById( 'password1' ) );
            inputError( document.getElementById( 'password2' ) );
            document.querySelector( '.form-item[data-content="password2"] .inputMessage' ).innerHTML = 'Passwords do not match';
            return false;
        }
        else{
            inputClear( document.getElementById( 'password1' ) );
            inputClear( document.getElementById( 'password2' ) );
            document.querySelector( '.form-item[data-content="password2"] .inputMessage' ).innerHTML = '';
        }

        let data = new FormData();
        data.append( 'password', password1 );

        fetch( `${ Global.apiEndpoint }/user/${ GetData( 'userId', 'session' ) }/passwordUpdate`, {
            headers: {
                'apiToken': GetData( 'apiKey', 'session' )
            },
            method: "POST",
            body: data
        })
            .then( ( response ) => {
                return response.text();
            } )
            .then( ( data ) => {
                console.log( data );
                /*const dataObj = JSON.parse( data );

                if( dataObj.status === "success" ){
                    button.classList.remove( "state-loading" );
                    button.classList.add( "state-success" );

                    setTimeout( function(){
                        button.classList.remove( "state-success" );
                    }, 2000 );

                }*/

            } );
    }

    updateSelf(e){
        e.preventDefault();
        console.log(this.state.data);

        let data = new FormData();

        data.append( 'name', document.getElementById("currentName").value);
        data.append( 'email', document.getElementById("currentEmail").value );

        function inputError( input ){
            input.classList.add( "error" );
            // button.classList.remove( "state-loading" );
            // button.classList.add( "state-error" );
        }
        function inputClear( input ){
            input.classList.remove( "error" );
            // button.classList.remove( "state-error" );
        }

        if(document.getElementById("passwordNew1").value !== ""){
            const password1 = document.getElementById( 'passwordNew1' ).value;
            const password2 = document.getElementById( 'passwordNew2' ).value;

            if( password1 === "" ){
                inputError( document.getElementById( 'passwordNew1' ) );
                return false;
            }else{
                inputClear( document.getElementById( 'passwordNew1' ) );
            }

            if( password2 === "" ){
                inputError( document.getElementById( 'passwordNew2' ) );
                return false;
            }else{
                inputClear( document.getElementById( 'passwordNew2' ) );
            }

            if( password1 !== password2 ){
                inputError( document.getElementById( 'passwordNew1' ) );
                inputError( document.getElementById( 'passwordNew2' ) );
                document.querySelector( '.form-item[data-content="passwordNew2"] .inputMessage' ).innerHTML = 'Passwords do not match';
                return false;
            }
            else{
                inputClear( document.getElementById( 'passwordNew1' ) );
                inputClear( document.getElementById( 'passwordNew2' ) );
                document.querySelector( '.form-item[data-content="passwordNew2"] .inputMessage' ).innerHTML = '';
                data.append( 'password', password2 );
            }
        }

        fetch( `${ Global.apiEndpoint }/user/${this.state.data.id}/update`, {
            headers: {
                'apiToken': GetData( 'apiKey', 'session' )
            },
            method: "POST",
            body: data
        })
            .then( ( response ) => {
                return response.text();
            } )
            .then( ( data ) => {
                console.log( data );
                // const dataObj = JSON.parse( data );
                //
                // if( dataObj.status === "success" ){
                //     // button.classList.remove( "state-loading" );
                //     // button.classList.add( "state-success" );
                //
                //     // setTimeout( function(){
                //     //     button.classList.remove( "state-success" );
                //     // }, 2000 );
                //
                // }

            } );

    }

    selectPermission(data){
        const selected = data.target.option;
    }

    updateUser(e, userID){
        e.preventDefault();
        let data = new FormData();

        data.append( 'name', document.getElementById("name" + userID).value );
        data.append( 'email', document.getElementById("email" + userID).value );
        data.append( 'userGroup', document.getElementById("permissions" + userID).value );

        if(document.getElementById("password" + userID).value !== ""){
            console.log("Changing password");
            data.append( 'password', document.getElementById("password" + userID).value);
        }

        fetch( `${ Global.apiEndpoint }/user/${userID }/update`, {
            headers: {
                'apiToken': GetData( 'apiKey', 'session' )
            },
            method: "POST",
            body: data
        })
            .then( ( response ) => {
                return response.text();
            } )
            .then( ( data ) => {
                console.log( data );
                /*const dataObj = JSON.parse( data );

                if( dataObj.status === "success" ){
                    button.classList.remove( "state-loading" );
                    button.classList.add( "state-success" );

                    setTimeout( function(){
                        button.classList.remove( "state-success" );
                    }, 2000 );

                }*/

            } );


    }

    createUser(e){
        e.preventDefault();

        let data = new FormData();

        data.append( 'name', document.getElementById("nameNew").value );
        data.append( 'email', document.getElementById("emailNew").value );
        data.append( 'userGroup', document.getElementById("permissionsNew").value );
        data.append( 'password', document.getElementById("passwordNew").value);
        data.append( 'api_token', genRandID(60) );

        fetch( `${ Global.apiEndpoint }/user/new`, {
            headers: {
                'apiToken': GetData( 'apiKey', 'session' )
            },
            method: "POST",
            body: data
        })
            .then( ( response ) => {
                return response.text();
            } )
            .then( ( data ) => {
                const dataObj = JSON.parse( data );
                console.log(dataObj);

                if( dataObj.status === "success" ){
                    let userArray = this.state.users;
                    userArray.push(dataObj.user);

                    document.getElementById("nameNew").value = "";
                    document.getElementById("emailNew").value = "";
                    document.getElementById("passwordNew").value = "";
                    document.getElementById("permissionsNew").value = "";

                    this.setState({
                        users: userArray
                    })
                }

            } );

    }

    deleteUser(e, userID){
        e.preventDefault();

        fetch( `${ Global.apiEndpoint }/user/${userID }/delete`, {
            headers: {
                'apiToken': GetData( 'apiKey', 'session' )
            },
            method: "GET",
        })
            .then( ( response ) => {
                return response.text();
            } )
            .then( ( data ) => {

                const dataObj = JSON.parse( data );
                console.log(dataObj);

                if( dataObj.status === "success" ){
                    this.state.users.splice(this.state.users.findIndex(user => user.id === userID), 1);
                    let newUserArr = this.state.users;
                    this.setState({
                        users: newUserArr
                    })
                }

            } );
    }

    filterUsers(e){
        e.preventDefault();
        let filteredUsers = this.state.fullUserList;
        filteredUsers = filteredUsers.filter((user) => {
            let userName = user.name.toLowerCase() + user.email.toLowerCase();
            let value = document.getElementById("search").value.toLowerCase();
            return userName.indexOf(value) !== -1
        });
        this.setState({
           users: filteredUsers
        });
    }

    /*
        called when component first loads, not called for changes
    */
    componentDidMount() {
        this.getData();
    }

    render() {
        return (
            <section className="loginPage">

                <header className="headerSlantedEdge background-backgroundDark color-onDarkBackground padding-y-xlarge text-align-center">
                    <Icon src='home' />
                    <PageTitle classes="color-white">My Account</PageTitle>
                </header>

                <div className="mainContent">
                    <div className="innerWrap medium">
                        <div className="innerWrap-inner background-background padding-x-xlarge padding-y-xlarge">

                            <div className="sectionIntro">

                                { this.state.data !== null &&
                                    <div>
                                        <style>
                                            {"\
                                                .third-width{\
                                                    margin-top: 0 !important;\
                                                }\
                                                .submit{\
                                                    min-width: 9em;\
                                                }\
                                                .submit .buttonInner{\
                                                    padding-top: 0.925em !important;\
                                                }\
                                                .form-item + .form-item{\
                                                    margin-top: 0;\
                                                }\
                                            "}
                                        </style>
                                        <h4 className="cardTitle text-align-center margin-bottom-medium">My Account</h4>
                                        <div className="flex">
                                            <FormField
                                                wrapClass="quarter-width"
                                                inputClass="outlineInput moveLabelOnFocus forceLabelAbove bold color-blue"
                                                name="currentName"
                                                type="text"
                                                label="Name"
                                                value={ this.state.data.name }
                                            />
                                            <FormField
                                                wrapClass="quarter-width"
                                                inputClass="outlineInput moveLabelOnFocus forceLabelAbove bold color-blue"
                                                name="currentEmail"
                                                type="email"
                                                label="Email Address"
                                                value={ this.state.data.email }
                                            />
                                            <FormField
                                                wrapClass="quarter-width"
                                                inputClass="outlineInput moveLabelOnFocus forceLabelAbove bold color-blue"
                                                name="passwordNew1"
                                                type="password"
                                                label="Update your password"
                                                required="true"
                                            />
                                            <FormField
                                                wrapClass="quarter-width"
                                                inputClass="outlineInput moveLabelOnFocus forceLabelAbove bold color-blue"
                                                name="passwordNew2"
                                                type="password"
                                                label="Confirm password"
                                                required="true"
                                            />
                                            <div className={"flex quarter-width"}>
                                                <Button
                                                    loader="true"
                                                    classes="primary submit sixth-width"
                                                    text="Update"
                                                    onClick={ (evt) => this.updateSelf(evt) }
                                                />
                                            </div>

                                        </div>
                                        {
                                            this.state.issues.length > 0 &&
                                            <div className="margin-top-xlarge">
                                                <h4 className="cardTitle text-align-center margin-bottom-medium">Your Issues</h4>
                                                {this.state.issues.map((issue) => (
                                                        <div className="flex" key={issue[200].id}>{issue[200].name}</div>
                                                    )
                                                )}
                                            </div>
                                        }
                                    </div>
                                }

                            </div>
                            {
                                this.state.userGroup === "admin" &&
                                <div className={"adminArea"}>
                                    <h4 className="cardTitle text-align-center margin-bottom-medium">Admin Area</h4>

                                    <div className="flex">
                                        <Form classes={"flex"}>

                                            <FormField
                                                wrapClass="quarter-width"
                                                inputClass="outlineInput moveLabelOnFocus forceLabelAbove bold color-blue"
                                                name={"nameNew"}
                                                type="text"
                                                label="Name"
                                                autoComplete="off"
                                            />
                                            <FormField
                                                wrapClass="quarter-width"
                                                inputClass="outlineInput moveLabelOnFocus forceLabelAbove bold color-blue"
                                                name={"emailNew" }
                                                type="email"
                                                label="Email Address"
                                                autoComplete="off"
                                            />
                                            <FormField
                                                wrapClass="quarter-width"
                                                inputClass="outlineInput moveLabelOnFocus forceLabelAbove bold color-blue"
                                                name={"permissionsNew" }
                                                type="text"
                                                label="Permissions"
                                                autoComplete="off"
                                            />
                                            <FormField
                                                wrapClass="quarter-width"
                                                inputClass="outlineInput moveLabelOnFocus forceLabelAbove bold color-blue"
                                                name={"passwordNew" }
                                                type="password"
                                                label="Password"
                                                autoComplete="off"
                                            />
                                            <div className={"flex quarter-width"}>
                                                <Button
                                                    loader="true"
                                                    classes="primary submit sixth-width color-green"
                                                    text="Add"
                                                    onClick={ (evt) => this.createUser(evt) }
                                                />
                                            </div>

                                        </Form>
                                    </div>
                                    <div>
                                        <h4 className="cardTitle text-align-center margin-bottom-medium">Search</h4>
                                    </div>
                                    <div className="flex">
                                        <Form classes={"flex"}>
                                            <FormField
                                                wrapClass="full-width"
                                                inputClass="outlineInput moveLabelOnFocus forceLabelAbove bold color-blue"
                                                name={"search"}
                                                type="search"
                                                label="Search For User"
                                                autoComplete="off"
                                                onChange={(evt) => this.filterUsers(evt)}
                                            />

                                            <div className={"flex quarter-width"}>
                                                <Button
                                                    loader="true"
                                                    classes="primary submit sixth-width gray"
                                                    text="Search"
                                                    onClick={ (evt) => this.filterUsers(evt) }
                                                />
                                            </div>
                                        </Form>
                                    </div>

                                    {this.state.users.map((user) => (
                                    <div className="flex">
                                        <Form classes={"flex"}>

                                        <FormField
                                            wrapClass="quarter-width"
                                            inputClass="outlineInput moveLabelOnFocus forceLabelAbove bold color-blue"
                                            name={"name" + user.id }
                                            type="text"
                                            label="Name"
                                            value={ user.name }
                                            autoComplete="off"
                                        />
                                        <FormField
                                            wrapClass="quarter-width"
                                            inputClass="outlineInput moveLabelOnFocus forceLabelAbove bold color-blue"
                                            name={"email" + user.id }
                                            type="email"
                                            label="Email Address"
                                            value={ user.email }
                                            autoComplete="off"
                                        />
                                        <FormField
                                            wrapClass="quarter-width"
                                            inputClass="outlineInput moveLabelOnFocus forceLabelAbove bold color-blue"
                                            name={"permissions" + user.id }
                                            type="text"
                                            label="Permissions"
                                            value={ user.userGroup }
                                            autoComplete="off"
                                        />

                                            {/*<Dropdown*/}
                                                {/*wrapClass="quarter-width"*/}
                                                {/*inputClass="moveLabelOnFocus forceLabelAbove bold color-blue"*/}
                                                {/*name={"permissions" + user.id }*/}
                                                {/*label="Permissions"*/}
                                                {/*options={ this.state.permissions }*/}
                                                {/*onChange={ this.selectPermission }*/}
                                                {/*disabled={ false }*/}
                                            {/*/>*/}
                                            <FormField
                                            wrapClass="quarter-width"
                                            inputClass="outlineInput moveLabelOnFocus forceLabelAbove bold color-blue"
                                            name={"password" + user.id }
                                            type="password"
                                            label="Password"
                                            value={ user.password }
                                            autoComplete="off"
                                        />
                                            <div className={"flex quarter-width"}>
                                                <Button
                                                    loader="true"
                                                    classes="primary submit sixth-width color-red"
                                                    text="Delete"
                                                    onClick={ (evt) => this.deleteUser(evt, user.id) }
                                                />
                                                <Button
                                                    loader="true"
                                                    classes="primary submit sixth-width"
                                                    text="Update"
                                                    onClick={ (evt) => this.updateUser(evt, user.id) }
                                                />
                                            </div>
                                        </Form>
                                    </div>
                                        )
                                    )}
                                </div>
                            }

                        </div>
                    </div>
                </div>

            </section>
        );
    }
}
