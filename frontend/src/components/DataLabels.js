import React from 'react';
import { FormatNumber } from './Formatting';

/*
    PAGE HEADER
    Returns meta data and header info for page
*/
export class LargeDataLabel extends React.Component {
    /*constructor( props ){
        super( props );
    }*/

    render() {

        return (
            <div className={`largeDataLabel text-align-${ this.props.textAlign }`}>
                <h2 className="sectionTitleLarge value">
                    <FormatNumber num={ this.props.value } />
                </h2>
                <label className="largeLabel">{ this.props.label }</label>
            </div>
        );
    }
}
