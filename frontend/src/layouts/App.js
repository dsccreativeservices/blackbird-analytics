import React from 'react';
import {
  Switch, Route, Link, useHistory,
} from 'react-router-dom';
import { Security, SecureRoute, LoginCallback } from '@okta/okta-react';
import { OktaAuth, toRelativeUrl } from '@okta/okta-auth-js';
import { GlobalNav, FooterNav } from '../components/Nav';
import { Year } from '../components/Calendar';
import { Home, Dashboard } from '../components/Home';
import { AllAdvertisers, GetAdvertiser } from '../components/Advertiser';
import { GetCampaign } from '../components/Campaign';
import { CreateProject, GetProject } from '../components/Project';
import { Search } from '../components/Search';
import { Benchmark } from '../components/Benchmark';
import { Account } from '../components/Account';
import { PrivateRoute, Login, Logout } from '../components/Auth';
import { FindParent } from '../components/Helpers';
import '../css/master.css';
import logo from '../img/logo.svg';
import { Notification } from '../components/Notification';
import { IssueTracker } from '../components/IssueTracker';
import config from '../config';
import Profile from '../components/Profile';

const oktaAuth = new OktaAuth({ ...config.oidc });

/* the App class is the main wrapper for the app */
const App = () => {
  const history = useHistory();
  // form submit with enter button
  document.body.addEventListener('keyup', (e) => {
    if (e.which === 13) {
      e.preventDefault();
      if (document.activeElement !== null && document.activeElement.tagName !== 'BODY') {
        console.log('submit form');
        // get parent
        const parent = FindParent(document.activeElement, 'formWrapper');
        // get submit button and trigger click
        if (parent !== null) {
          if (parent.querySelector('.button.submit') !== null) {
            parent.querySelector('.button.submit').click();
          }
        }
      }
    }
  });

  const restoreOriginalUri = async (_oktaAuth, originalUri) => {
    history.replace(toRelativeUrl(originalUri, window.location.origin));
  };

  return (
    <div className="flex flex-column">
      <Security
        oktaAuth={oktaAuth}
        restoreOriginalUri={restoreOriginalUri}
      >
        <Notification />
        <IssueTracker />
        <header className="globalHeader flex vertical-center">
          <div className="header-branding">
            <Link to="/dashboard">
              <p className="header-title uppercase add-spacing bold">
                <img
                  className="header-logo"
                  src={logo}
                  alt="Blackbird Ad Tracking Analytics"
                />
              </p>
            </Link>
          </div>
          <GlobalNav />
        </header>
        <div className="flex-grow">
          <main>
            {/* switch loads different components depending on URL pathname */}
            <Switch>
              <Route
                path="/login/callback"
                component={LoginCallback}
              />
              <Route
                path="/logout"
                component={Logout}
              />
              <Route
                exact
                path="/"
                component={Home}
              />
              <SecureRoute
                path="/dashboard"
                component={Dashboard}
              />
              <SecureRoute
                path="/advertiser/all"
                component={AllAdvertisers}
              />
              <SecureRoute
                path="/advertiser/:id"
                component={GetAdvertiser}
              />
              <SecureRoute
                path="/campaign/:id"
                component={GetCampaign}
              />
              <SecureRoute
                path="/project/create"
                component={CreateProject}
              />
              <SecureRoute
                path="/project/:id"
                component={GetProject}
              />
              <SecureRoute
                path="/search"
                component={Search}
              />
              <SecureRoute
                path="/benchmarks"
                component={Benchmark}
              />
              <SecureRoute
                path="/account"
                component={Account}
              />
              <SecureRoute
                path="/profile"
                component={Profile}
              />
            </Switch>
          </main>

        </div>
        <footer className="globalFooter margin-top-xxlarge padding-x-large padding-y-large">
          <div className="footer-branding text-align-center">
            <p className="footer-title uppercase add-spacing bold color-blue">Blackbird</p>
          </div>
          <FooterNav />
          <p className="copywrite margin-top-large text-align-center tiny color-dark">
            ©
            <Year />
            {' '}
            Scripps Networks Interactive
          </p>
        </footer>
      </Security>
    </div>
  );
};

// export App
export default App;

/*
<Switch>
    <Route exact path='/' component={Home}/>
    {/* both /roster and /roster/:number begin with /roster }
    <Route path='/roster' component={Roster}/>
    <Route path='/schedule' component={Schedule}/>
</Switch>

const extraProps = { color: 'red' }
<Route path='/page' render={(props) => (
    <Page {...props} data={extraProps} />
)}/>
*/
