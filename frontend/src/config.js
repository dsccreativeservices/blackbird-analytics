const yourOktaDomain = 'dev-74367200.okta.com';
const clientId = '0oabufido2EzGeRvU5d6';
const CLIENT_ID = process.env.CLIENT_ID || `${clientId}`;
const ISSUER = process.env.ISSUER || `https://${yourOktaDomain}/oauth2/default`;
const OKTA_TESTING_DISABLEHTTPSCHECK = process.env.OKTA_TESTING_DISABLEHTTPSCHECK || false;

export default {
  oidc: {
    clientId: CLIENT_ID,
    issuer: ISSUER,
    redirectUri: 'http://localhost:8080/login/callback',
    scopes: ['openid', 'profile', 'email'],
    pkce: true,
    disableHttpsCheck: OKTA_TESTING_DISABLEHTTPSCHECK,
    transformAuthState: async (oktaAuth, authState) => {
      if (!authState.isAuthenticated) {
        return authState;
      }
      // extra requirement: user must have valid Okta SSO session
      const user = await oktaAuth.token.getUserInfo();
      authState.isAuthenticated = !!user; // convert to boolean
      authState.users = user; // also store user object on authState
      return authState;
    },
  },
  resourceServer: {
    messagesUrl: 'http://localhost:8080/api/messages',
  },
};

export { CLIENT_ID, ISSUER };
