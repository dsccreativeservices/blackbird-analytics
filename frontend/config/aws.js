const fs = require('fs');
const AWS = require('aws-sdk');
const mime = require('mime');

const s3 = new AWS.S3({
    accessKeyId: process.env.AWS_ACCESS_KEY,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY
});

const uploadFile = (fileName, path) => {
    console.log(fileName);

    fs.readFile(fileName, 'utf8', (err, data) => {
        if (err) throw err;

        const params = {
            Bucket: 'sni-blackbird.com', // pass your bucket name
            Key: path,
            Body: data,
            ContentType: mime.getType(path)
        };

        // console.log(params.Bucket +  params.Key);
        // console.log("File Type: " + params.ContentType);

        s3.upload(params, function(s3Err, data) {
            if (s3Err) throw s3Err;
            console.log(`File uploaded successfully at ${data.Location}`)
        });
    });
};

const testFolder = "./build";

showFiles();

function showFiles() {

    console.log(s3);

    let files = getFiles(testFolder);

    for (let i = 0; i < files.length ; i++) {
        console.log("Original File: " + files[i]);
        let path = files[i].substring('./build/'.length);
        // console.log("Path: " + path);
        files[i] = files[i].substring('./'.length);
        console.log("New File: " + files[i]);
        uploadFile(files[i], path);

    }
}



function getFiles (dir, files_){
    files_ = files_ || [];
    var files = fs.readdirSync(dir);
    for (var i in files){
        var name = dir + '/' + files[i];
        console.log(files[i])
        if (fs.statSync(name).isDirectory()){
            getFiles(name, files_);
        } else {
            files_.push(name);
        }
    }
    return files_;
}